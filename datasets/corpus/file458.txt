The incredible ways governments use the Internet of Things
Leading cities and countries are transforming with IoT and AI
Pawel Nolbert / Unsplash
Government and innovation? Yes: two recent surveys highlight how governments are embracing technology to tackle issues from public safety to social services.
The 2017 Digital Cities Survey ranks cities on how they leverage technology to better serve their citizens.[1] An important takeaway: city size and budget aren’t as important as visionary leadership who recognizes technology’s potential to transform communities and attract new citizens.
“Techies, startups and recent college graduates seek to live in places that create the best citizen services for them — cities that reduce their traffic, save them time finding parking, allow them to pay taxes online, and are on the leading edge of innovation,” said Marquis Cabrera, IBM’s Global Leader of Digital Government Transformation. “Cities are planting fertilizer for greener economic pastures; they’re setting the conditions for new jobs, new business creation, and new buzz about the city.”
So what technology excites the leading digital cities?
81% are actively considering the potential of the Internet of Things.[2] Imagine if your city’s infrastructure, roads, traffic lights, buildings had a tiny sensor capturing data. Connecting and learning from that influx of new information would help governments more quickly respond to challenges such as reducing energy consumption or helping first responders safely assess dangerous situations.
At the country level, top-ranked governments optimize public services delivery to be more effective, accessible, and responsive to people’s needs. The UK, Australia, Korea, Singapore, and Finland — the top five in the UN E-Government Survey — have doubled-down on digital technology.[3]

The stakes are high: government digitization could generate over $1 trillion annually worldwide.[4]
The Angle:
The future is closer than we think: 20.4 billion IoT devices are forecast by 2020.[5]
Practical uses for IoT in government are everywhere:
Management of traffic and reduction of automobile pollution with data coming from sensors in roads, weather information, public transportation, and car GPS devices
Detection of flooding and earthquakes, giving citizens earlier warnings
Tracking first responders’ location, movement, respiration, and exposure to heat and hazardous materials
When combined with AI, can help monitor the movements of the elderly or transform accessibility for the 650 million people with disabilities worldwide [6]
Fully embracing this new technology may seem like a radical shift for some governments, but the rewards will far outweigh the headaches.
“My recommendation to cities is to take a risk and try to digitally transform citizen services using technology, so more people are inclined to learn about your city, move to your city, and contribute to your city,” Cabrera said.
Learn more about how IoT can help improve the lives of your citizens.
[1] http://www.govtech.com/dc/digital-cities/Digital-Cities-Survey-2017-Winners-Announced.html
[2] http://www.govtech.com/dc/digital-cities/Digital-Cities-Survey-2017-Winners-Announced.html
[3] https://publicadministration.un.org/egovkb/en-us/Reports/UN-E-Government-Survey-2016
[4] https://www.mckinsey.com/business-functions/digital-mckinsey/our-insights/public-sector-digitization-the-trillion-dollar-challenge
[5] http://www.zdnet.com/article/iot-devices-will-outnumber-the-worlds-population-this-year-for-the-first-time/
[6] https://www.ibm.com/watson/advantage-reports/ai-social-good-social-services.html
