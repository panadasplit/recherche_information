AI en 3 minutos: ¿Para qué Machine Learning?
AI Learners
En un post anterior hablamos sobre los 3 tipos de Machine Learning. En este post cubriremos el para qué nos sirve.
Vinyals & Le, Google (2015)
Machine Learning es un campo de la Inteligencia Artificial en el que un programa puede aprender sin ser explícitamente programado (Arthur Samuel). En los últimos años, hemos visto todo tipo de aplicaciones interesantes de ML: Desde agentes que pueden tener conversaciones filosóficas sobre la moral hasta agentes que pueden detectar si alguien tiene cáncer a partir de la foto de una lesión de la piel con mayor precisión que doctores.
En los últimos años, tareas que considerábamos irremplazables por computadoras, tareas de áreas de servicios, han podido ser realizadas de una mejor manera por computadoras. Abogados, economistas e incluso programadores han tenido que adaptarse en un mercado donde técnicas de Machine Learning pueden desempeñarse mejor que humanos. Y no sólo en servicios, ML también ha empezado a desempeñarse muy bien en áreas creativas o en las que pensarías que sólo un humano se puede desempeñar.
Una red neuronal que subtitula imágenes describiendo qué es lo que pasa
Andrew Ng separa las áreas de aplicación de ML en tres:
Database Mining: Teniendo acceso a tanta información, algo que ha ocurrido gracias al mundo web, ML se vuelve más potente. Aunque ML es un campo que lleva desde los 70s siendo investigado, recién lleva unos 10 años siendo exitoso por la cantidad de datos y el poder de procesamiento de las computadoras actuales. Gracias a que gobiernos y compañías crean bases de datos con todo tipo de información (info de sensores, historial médico, dónde hacen click los usuarios), ML se potencia de todo eso para crear soluciones muy interesantes.
Aplicaciones que no podemos programar a mano: Imagina que tuvieras qué programar con técnicas convencionales un programa que detecta si una imagen se encuentra en una foto. Otro ejemplo: imagina que tuvieras que escribir un programa que dijera al coche cómo conducir para cada caso. Hay cientos de miles de posibilidades, y hacer un programa para esto no sólo puede ser tedioso, sino imposible. ML se puede aplicar para este tipo de campos pues aprende por su cuenta: Visión por Computadora, Procesamiento de Lenguaje Natural y Vehículos Autónomos son 3 áreas que entrarían en esta aplicación.
Programas que se adaptan a si mismos: Cuando Spotify te sugiere una canción, Netflix una película o Amazon un producto, se está adaptando específicamente a ti.
Machine Learning está en todos lados. Es por eso que es importante entender cómo funciona y el impacto tanto positivo como negativo que esta gran rama de la Inteligencia Artificial puede tener.
Algunos tips para aprender:
No te vayas con el hype de una herramienta. Aprende las bases primero.
Entiende las matemáticas detrás de los modelos. Por otro lado, no te dejes intimidar por ellas.
Ten buenos fundamentos: entiende cómo enfrentarte a un problema de Machine Learning y qué técnicas se pueden utilizar.
Aprende las aplicaciones más comunes de ML en la industria. Compañías como Google, Facebook y Netflix tienen blogs donde suben sus resultados.
Hay cursos gratuitos. Revisa Coursera, Udacity y EdX para ver cursos gratuitos de todo tipos de temas.
¡Aprende con otros!
