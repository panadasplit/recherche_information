THE IMPORTANCE OF STANDING OUT ON-SHELF

We sat down in April 2018 with Spoon Cereals, a challenger brand within the breakfast category. Spoon are on a mission to make breakfast better, and their flavoursome and healthy (but certainly not boring) granolas and mueslis have been developed over the past few years including a successful appearance on Dragon’s Den. Their products now sit in Waitrose and Wholefoods, amongst other retailers not beginning with “W”.
Their biggest issue now:
“We’ve got a great product that people love when they try it… how can we get more people picking up our brand?”

Sampling solves some of this issue but it’s too localised… you can’t commit to sampling in every store to reach every potential shopper as it’s too labour intensive (read: expensive). Then, despite being localised to a store, it’s completely untargeted. You will end up with people trying the product who had no intention of ever buying from your category, let alone your product (though maybe this could be offset by the one in a thousand chance of converting a new-to-category shopper).
Spoon wanted to see if changing packaging would help. It’s not enough just to ask customers if they like a pack design. How does behaviour change when shoppers are faced with a purchasing decision at shelf with different packaging? If behaviour does change and make a positive impact for the brand, is it worth changing the packaging? What’s the ROI? Even if it’s worth us doing it, will it be launched by our retailers if we can’t prove that it will benefit the category overall?
We could access these kind of results through a traditional in-store trial, assuming we can get agreement from a retailer, enough stores to participate and be compliant, enough stores to act as control stores, enough people to implement it, a spare 3 months to monitor the trial and a cool £250,000 to conduct all of the above.
Or we could run a Virtual Store Trial.
We built a virtual environment of the Granola category as an example of a Waitrose store with three different scenarios.
Control — How the Spoon brand currently looks
Scenario 1 — A change in Spoon branding
Scenario 2 — An alternative change in Spoon branding
In under a week, we ran a Virtual Store Trial with 1,500 shoppers (500 per scenario) to measure the impact of changing the packaging compared to what currently exists in store.
The best of the new packaging options resulted in:
51% increase in sales for Spoon
2% increase in sales to the category
Spoon’s on-share neighbours lose value share as shoppers are attracted by Spoon’s new packaging
Unsurprisingly, Spoon Cereals are changing their packaging to this option, which we’re looking forward to seeing hit stores in August. They said:
“The results from VST more than justified the investment delivering an outstanding ROI, creating an excellent category argument and an uplift in sales.”
For more information and to understand how we can test changes to your brand or category, please contact Nick Theodore at nick@storetrials.com or visit www.storetrials.com.

