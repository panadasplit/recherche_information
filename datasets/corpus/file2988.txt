Designing a Chatbot — UX process

(Image Source: Christos)
In today’s era companies transitioning from app based or web based interfaces to ‘Conversation based platforms’ (Chatbots). Chatbots are a computer program designed to simulate conversation with human users, especially over the Internet. Chatbots are not new invention, history of chatbots can be found during 1950–60s when Alan Turing & Joseph Weizenbaum contemplated the concept of computers communicating like humans do with experiments like the Turing Test and the invention of the first chatterbot program, Eliza.
Today Chatbots are disrupting the overall world. Many platforms like Facebook, Twitter, Telegram etc. has given their platforms available & open for chatbots integration which has given major shift in customer’s interacting with the Companies and their Products.
Today I wanted to share Chatbot design process which I have synthesized based upon my experience on designing a Chatbot. My main goal from this article is to spread awareness about ‘How to design a Chatbot’, ‘What steps to be taken in designing a Chatbot’ from a UX Designer’s point of view. I am open for recommendations & suggestions to improve my process.
Chatbot Design Process

Lets see each step in more detail,
1. Scope & Requirements
Why Chatbot! Understand context of a chatbot
List features or offerings your chatbot going to offer
Be specific and limit your offerings
Platform to launch chatbot
2. Identify Inputs
Every goal in a Chatbot interaction can be divided in to logical steps (conversations).
Primary ways a Chatbot gets input,
From user (queries, info from users — text or voice)
From device (device sensors like geo-location etc.)
From intelligence systems (Insights from user’s historic data)
3. Understand Chatbot UI elements
There are multiple platforms like Facebook, twitter, telegram, WeChat etc. who support chatbots. https://developers.facebook.com/docs/messenger-platform/send-messages/templates

Continue Reading >>
About Author
Abhishek Jain
User Experience Designer & Researcher
Twitter | LinkedIn | Email
