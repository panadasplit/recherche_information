Traffic Sign Recognition via Neural Network
Build a Traffic Sign Recognition Project
The goals / steps of this project are the following:
Load the data set (see below for links to the project data set)
Explore, summarize and visualize the data set
Design, train and test a model architecture
Use the model to make predictions on new images
Analyze the softmax probabilities of the new images
Summarize the results with a written report
Usage of my code
Pull a docker container with tensorflow gpu and python3
CPU: use udacity-carnd.
For the use of GPU, AWS EC2 GPU g2.x2large instance can be used. Install docker-ce, nvidia-docker2 on the instance. Plus, I need to build a docker image locally to support cv2, etc.
Launch this workspace
CPU only
GPU
Rubric Points
Here I will consider the rubric points individually and describe how I addressed each point in my implementation.
Writeup / README
1. Provide a Writeup / README that includes all the rubric points and how you addressed each one. You can submit your writeup as markdown or pdf. You can use this template as a guide for writing the report. The submission includes the project code.
Here is a link to my project code
Data Set Summary & Exploration
1. Provide a basic summary of the data set. In the code, the analysis should be done using python, numpy and/or pandas methods rather than hardcoding results manually.
I used the pandas library to calculate summary statistics of the traffic signs data set:
The size of the training set is 34799.
The size of the validation set is 4410.
The size of the test set is 12630.
The shape of a traffic sign image is (32, 32, 3).
The number of unique classes/labels in the data set is 43.
2. Include an exploratory visualization of the dataset.
Here is an exploratory visualization of the data set. It is a bar chart showing the data distributions, where x-axis is the indices of labels and y-axis represents the size of samples for one category/label.
Train Set Distribution

Test Set Distribution

Validation Set Distribution

Design and Test a Model Architecture
1. Describe how you preprocessed the image data. What techniques were chosen and why did you choose these techniques? Consider including images showing the output of each preprocessing technique. Pre-processing refers to techniques such as converting to grayscale, normalization, etc. (OPTIONAL: As described in the “Stand Out Suggestions” part of the rubric, if you generated additional data for training, describe why you decided to generate additional data, how you generated the data, and provide example images of the additional data. Then describe the characteristics of the augmented training set like number of images in the set, number of images for each class, etc.)
As a first step, I decided to convert the images to grayscale because the color does not help the sign recognition based on my experiment. Here the corresponding examples of traffic sign images before and after grayscaling.
The examples of RGB images are,

The corresponding grayscale images are shown as follows,

As a last step, I normalized the image data because zero-mean data will provide better-conditioned distribution for numerical optimization during the training. The equation is
The normalized images are shown as follows

I decided to generate additional data because I found one misprediction among the 5 new signs from the website is due to the scaling issue. Due to the time constraint of this project, I used only the scaling and cropping method to generate extra data samples to help recognize this kind of images. Here the examples of original images and the augmented images:




Augmented train set has the sample distribution as

Compared to the original distribution of train set.

2. Describe what your final model architecture looks like including model type, layers, layer sizes, connectivity, etc.) Consider including a diagram and/or table describing the final model.
My final model consisted of the following layers:

3. Describe how you trained your model. The discussion can include the type of optimizer, the batch size, number of epochs and any hyperparameters such as learning rate.
To train the model, I used an Adam optimizer discussed in the lecture.
The batch size is 128.
The number of epochs is 51.
The learning rate is 0.0008.
The keep probability of dropout is 50.0%.
4. Describe the approach taken for finding a solution and getting the validation set accuracy to be at least 0.93. Include in the discussion the results on the training, validation and test sets and where in the code these were calculated.
My final model results were:
training set accuracy of 99.9%.
validation set accuracy of 95.7%.
test set accuracy of 94.0%.
new signs accuracy of 100.00% (80% without augmented train sets)
Iterative approach was chosen
What was the first architecture that was tried and why was it chosen?
Answer: I started with LeNet since the lecture mentioned it has pretty good performance in this kind of task.

What were some problems with the initial architecture?
Answer: The accuracy is not high enough, only around 89% for the test set.
How was the architecture adjusted and why was it adjusted?
Answer:
I increased the filter depth to capture more pattern information from the inputs.
I added dropout for the fully-connected layers to avoid the overfitting.
Which parameters were tuned? How were they adjusted and why?
Answer: I tuned the Dropout’s keep probability. I set 0.7 then decreased it to 0.5. Check the Configuration and Performance Table added below.
Configuration and Performance Table

The training performance figure is attached.

Test a Model on New Images
1. Choose five German traffic signs found on the web and provide them in the report. For each image, discuss what quality or qualities might be difficult to classify.
Here are five German traffic signs that I found on the web:

For example, the following image has the wrong prediction from the neural network trained without the augmented data set. This is actually a priority road sign, which is scaled and cropped. It fooled the neural network.

After adding the generated data with scaling images based on the original train set, the prediction is right then.

2. Discuss the model’s predictions on these new traffic signs and compare the results to predicting on the test set. At a minimum, discuss what the predictions were, the accuracy on these new predictions, and compare the accuracy to the accuracy on the test set
Here are the results of the prediction with augmented data sets:

The result of those new signs is better than the accuracy of test set.
Note that without augmented data, the model was able to correctly guess 4 of the 5 traffic signs, which gives an accuracy of 80%.

This is the reason why I added scaled images as augmented data samples to help the deep neural network to get trained.
3. Describe how certain the model is when predicting on each of the five new images by looking at the softmax probabilities for each prediction. Provide the top 5 softmax probabilities for each image along with the sign type of each probability.
Following figures should the top 5 softmax probabilities.





The confidence of each prediction is pretty high.
Summary
Based on the test set.
The Precision of the model is 91%.
The Recall Score of the model 94%.
The confusion matrix of the model

Further Steps:
Add more diverse samples to the train set. Due to the time constraint, I only added the data with different scaling factors. Actually, we can rotate the images, use different blur versions, and so on.
Balance the sample data distributions in the train set.
Train the IJCNN’11 paper mentioned.
Visualization of the neural network’s state.
Following paragraphs are from the comments based on my project submission by Udacity Reviewers. Thank you for the code review!
Meets Specifications
Brilliant Learner,
Thank you for the prompt resubmission. By carefully going through the project, it shows a lot of effort, diligence and above all, understanding of the underlying concepts. Well done!!.

You have successfully passed all the rubrics of this project excellently on the very first submission. This is very uncommon and I congratulate you for this. Do not forget, there is more to SDC and you are just getting started. Keep up the hard work and determination. It was my pleasure reviewing this wonderfully implemented project

Files Submitted
The project submission includes all required files.
Well done . The project submission has successfully included the necessary files which include:
Ipython notebook with code
HTML output of the code and a
A markdown write-up report
Dataset Exploration
The submission includes a basic summary of the data set.
Good job performing basic data summary !!! You rightly used python libraries such as pandas and numpy and some methods to perform operations such as extracting the shape of the images, the number of examples in the training set, the number of examples in the testing set and the number of unique classes in the dataset.
The submission includes an exploratory visualization on the dataset.
Excellent job done in the visualization of the data set. The notebook shows images with class titles and also bar charts to further explore the data set.

Design and Test a Model Architecture
The submission describes the preprocessing techniques used and why these techniques were chosen.
The report explicitly describes the preprocessing techniques used for example grayscale and data normalization. It goes further to explain why these techniques where chosen. Brilliant work done.
Suggestions.
In the report, you mentioned grayscale and normalization as techniques. However, I also invite you to read the following topics:
How can I convert an RGB image into grayscale in Python?
Thresholding of a grayscale Image in a range;
Image Processing with Python — RGB to Grayscale Conversion;
Normalizing images in OpenCV;
Normalization in Image processing;
sklearn.preprocessing.normalize.
The submission provides details of the characteristics and qualities of the architecture, including the type of model used, the number of layers, and the size of each layer. Visualizations emphasizing particular qualities of the architecture are encouraged.
Great work. The report shows details of the characteristics and qualities of the architecture used and states the number of convolution and dropout layers used.
Suggestions.
We see that your final model is based on the LeNet Architecture. More reading about this topic is provided here:
LeNet — Convolutional Neural Network in Python;
Convolutional Neural Networks LeNet;
Convolutional Neural Networks CNNs/ConvNets.
The submission describes how the model was trained by discussing what optimizer was used, batch size, number of epochs and values for hyperparameters.

Good work using the Adam Optimizer for this project. The following hyperparameters were used in the project with their corresponding values.

Suggestions.
Some documents and topics about Adam optimizer:
Tensorflow: Using Adam optimizer;
tf.train.AdamOptimizer ;
Adam: A Method for Stochastic Optimization.
The submission describes the approach to finding a solution. Accuracy on the validation set is 0.93 or greater.
Nice work achieving a maximum validation set accuracy of 0.95 as shown in the image below.
Suggestions
Please, check on Early stopping to avoid overfitting.
Test a Model on New Images
The submission includes five new German Traffic signs found on the web, and the images are visualized. Discussion is made as to particular qualities of the images or traffic signs in the images that are of interest, such as whether they would be difficult for the model to classify.
The submission includes five new German Traffic signs found on the web, and provide a good discussion on the quality of the images.

The submission documents the performance of the model when tested on the captured images. The performance on the new images is compared to the accuracy results of the test set.
The submission correctly documents the performance of the model, and the performance on the new images is compared to the accuracy results of the test set.
The top five softmax probabilities of the predictions on the captured images are outputted. The submission discusses how certain or uncertain the model is of its predictions.
The top five softmax probabilities of the predictions on the captured images are perfectly outputted. Impressive work!
