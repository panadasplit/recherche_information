The Data Science Execution Approach
The Traditional Approach
All those who have worked in a typical Software Engineering Process find it a way to mayhem to understand and adopt the Data Science Process. This is due to the inability to clarify the Data Science related ambiguities in various aspects. In this article, I will explain how Traditional systems will blend with the new Data Science systems.
In the typical Software Development Life Cycle, that follow waterfall or agile development approach consists of the one or more iteration of the following steps:
Typical SDLC Cycle
In this traditional approach, the data is fed into the system and based on the logic of computation, the results are generated. This can be roughly depicted as below:
Traditional Approach
The Data Science Approach
Now this is what happens in the Data Science approach. The results of the traditional systems becomes an input and the computational output of this approach is actually the model or the program which can then be applied to any other data received from the traditional systems.
Data Science Approach
Synonymous to the traditional approach, in order to make Data Science successful in any business, the following steps are recommended:
Data Science Execution Process
1. Sense the need– The business drivers should understand and realize the importance of data science as a separate practice. Ask if data science is really required? Create business scenarios where data science can bring value.
2. Strong Data Science Process — The processes and policies around a data science project should be set to make execution quick to develop and deploy. Governance of data should be well defined. Sensitive or personal information should be handled in a manner that it is not tampered with.
3. Clear Objectives and KPIs — The outcomes of a particular project should be clearly defined in terms of numbers, figures, percentages, dates, timelines, etc. The more clarity and crispness in the outcome, the easier it is to develop and measure KPIs against results.
4. Create a data science team — An effective data science team has experts like Solution Architects, Big Data Architects, Big Data Engineers, Back-end Developers, Front-end Developers, Data Scientists, Machine Learning Engineers, Business Intelligent (BI) Experts, Domain Experts and Statisticians. Based on the gravity of the project all or few of these experts should be brought together.
5. Make data available — Once the need is created, data should be made available to the data team. Ensure any creepiness towards data sharing is avoided. This is the vital step in creating a integration touch point with the traditional systems. Data from one or more traditional systems might flow into the data science system. At some instances, the outputs or predictions of machine learning might have to be integrated back with the traditional systems. A smooth, unhindered flow of data is a must.
6. Robust Architecture Design — The architecture design of the data science project should be carefully thought of. It could vary from choosing to use Big Data Spark or Hadoop, remote or cloud or local implementation, etc.
7. Applying suitable Algorithm — Usage of the most newest or fanciest algorithm might not necessarily be the solution to your problem. Start from the oldest and the most trusted basic algorithms, get a baseline metrics and then gradually build on with ensembles or the more complicated ones.
8. Clear Visualization and Insights — The outcomes of modelling the data science project needs to be very effectively conveyed back to the business stakeholders. The results, only when visualized as an impressive story will bring an impact to the business decision making process.
Conclusion
Once the above processes are well-defined and business stakeholders are in consensus to work towards achieving the data science objective, the traditional teams will work in close knit with the data science team in attaining the goals. And create yet another AI- Artful Intelligent system.
References:
TDWI Checklist Report — 7 steps for executing a successful Data Science Strategy
