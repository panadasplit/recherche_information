Improving Image Classification
Summary of Fast AI’s second lesson
Recap — learning rate, learning rate finder, annealing (stepwise, cosine), differential learning rate annealing
Finder — learning rate increases over time/iteration
Annealing — learning rate decrease over time/iteration
Data Augmentation (DA)—using different variations of a picture to train a model. 
In a pre-trained CNN model, layer activations are pre-computed.
To take advantage of DA in a dataset, precompute should be set to false, and run. Precompute usually should, usually, be run only once with a dataset.
A frozen layer is a layer which is untrained. Unfreeze to relearn activations.
Stochastic Gradient Descent w/ Restarts (SGDR) — using cosine annealing to restart the learning rate every epoch (if cycle_len = 1) or every 2 epochs (if cycle_len = 2)
SGDR helps with the learning to jump out of a local minima
More about SGDR here (blog) and here (fast wiki discussing sgdr h’prms)
Test Time Augmentation (TTA) — makes predictions on original + 4 augmented images and takes the average of them for a final prediction
Tiny detour into the FastAI library — built on top of PyTorch — and its origins
REVIEW — Easy Steps to train a world-class image classifier
* Enable data augmentation, and precompute=true
* Use lr_find() to find the highest learning rate where loss is still clearly improving
* Train last layer from precomputed activations for 1–2 epochs
* Train last layer with data augmentation (precompute=false) for 2–3 epochs with cycle_len=1
* Unfreeze all layers
* Set earlier layers to 3x-10x lower learning rate than next higher layer
* Use lr_find() again
* Train full network with cycle_mult=2 until over-fitting
