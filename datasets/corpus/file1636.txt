Let’s talk about trust, media, and democracy
Ben Boyd, danah boyd, Ari Fleischer, and Claire Wardle at the first official meeting of the Knight Foundation Commission on Trust, Media and Democracy on October 12, 2017 in New York City.
Welcome to the new online home for public discussion of the work of the Knight Commission on Trust, Media and Democracy. As the 26-member commission meets over the next year in cities across the country, members will hear from experts, take comments, and deliberate on how to think big and broadly about misinformation in public life. Meanwhile, recipients of the Knight Foundation’s new funding for related projects to increase trust in the media are already at work, and we’ll report on their progress here.
We hope this space will become a place to learn about what the commissioners are hearing, what they are thinking, and what is being discussed in the public sphere on this weighty subject central to our collective future.
Most importantly, we want you to be part of the conversation. We’ll pose questions based on what we’re seeing and hearing, and ask for your thoughts. We’ll report these back to the commission.
To start, here are five questions growing out of the discussions at the first meeting of the commission, which took place at the New York Public Library on Thursday, October 12th, 2017. Here is a full recap of public discussion and presentations at the meeting.
What would make you trust more of what you see and read in the media?
This chart shows how trust in the media is at an all-time low not just in the U.S., but all over the globe. If you don’t trust the media, why? What would make you trust the media more?

Source: www.edelman.com/trust2017
2. What are the potential trade-offs for subjecting social media platforms to more scrutiny and regulation?
Traditional newspapers printed on paper are an endangered species. Meanwhile, Facebook, Google, Twitter and other social media platforms drive traffic to real news stories. They allow everyone to be their own publisher, but have also been conduits for false stories. Some say these platforms should be subject to more scrutiny and regulation. What do you think?
3. What do we need to teach our children — and learn ourselves — when consuming media?
People often share things they already agree with, and ignore things they don’t. What questions should we all be asking when we see a news story posted online? What tools would help us become more discerning consumers of news and information? How do we avoid increasing ideological self-segregation on platforms and media?
4. How do we pay for good journalism?
As local newspapers and journalists lose jobs, there is less reporting on the state houses and legislatures, the water commissions and state agencies. Assuming accountability is important but profitable local media models are lacking, how should society pay for this utility?
5. How can we make the robots work for us?
We now know that misinformation can spread rapidly in the era of Artificial Intelligence, or AI. But how can we harness AI to work for the common good?
Post your answers, comments, disagreements below, and we’ll begin the conversation.
