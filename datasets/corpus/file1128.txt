Residual Plots Part 3— Scale-Location Plot
We’ve already discussed residual vs. fitted plots and normal QQ plots. Today we’ll move on to the next residual plot, the Scale-Location or Spread-Location plot.
The Scale-Location plot shows whether our residuals are spread equally along the predictor range, i.e. homoscedastic. We want the line on this plot to be horizontal with randomly spread points on the plot.
Let’s return to our code from the previous posts and generate another simple example in R to demonstrate:
# linear model — distance as a function of speed from base R cars dataset
model <- glm(dist ~ speed, data = cars, family = gaussian)
# setup plot grid
par(mfrow=c(2,2))
# we’re going to use the generic R plotting function which has a built-in scale-#location plot
plot(model)

You’ll notice our line starts off horizontal at the beginning of our predictor range, slopes up around 25, and then flattens again around 45. The line goes up because the residuals for those predictor values are more spread out. Our data generally has uniform variance at the ends of our predictor range and is somewhat heteroscedastic (i.e. non-uniform variance) in the middle of the range.
Next up is the residuals vs. leverage plot.
Additional resources if you’d like to explore further
http://data.library.virginia.edu/diagnostic-plots/ — more detailed overview
