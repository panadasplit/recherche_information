Hadron. The in Browser Mining and Blockchain A.I. That Could Change the World
“Blockchain Artificial Intelligence, Mined in Your Web Browser” — The White Paper
Why am I investing energy in Hadron versus the thousands of other startups and altcoins out there? I chose Hadron due to it’s vast difference from every other altcoin on the market, Artificial Intelligence. Imagen a world that could arise from thousands of idle hardware turned into energy to compute through a neural network that helps cure cancer. This isn’t just a currency, it’s an A.I. neural network.
As stated in their White Paper:
“95% of business leaders expect their enterprises to utilize AI, yet 91% of business also report significant barriers to AI implementation, with a lack of IT Infrastructure being the top problem (Teradata, 2017). Hadron bridges these shortfalls by pooling unused resources to sell on an automated free-market exchange.”
AI is an incredibly fast growing market as thousands of companies rely on AI to scan, screen, tag, and organize.
Hadron uses A.I. computations to help in the real world as you mine, so rather than sucking power out of your wall going through hours of the same hashing algorithms like Bitcoin and the hundreds of minable altcoins out there, you will be processing in an A.I. neural network to assist in real world situations. Hadron at the moment is in an Alpha phase, if you are reading this now, then you are still early. As of the date of writing this the project emerged from hiding about two weeks ago. Rather than sitting here and saying what Hadron is, or how you can get involved I will link you here to their bitcointalk post that explains the gist of what they do and how you can join far better than I ever could.
The community has grown by the thousands. The Telegram group is over 7,600 people, the reddit group (created by the community) over 1,000. All in less than two weeks.
A lot of information is not yet available to the public. Things like tokenomics, a wallet (currently the coins are stored on your hadron dashboard). This is because they are not yet ready to discuss these things as the current focus is to have a smooth and solid mining platform (their main product). The miner works on most browsers, on phones and computers alike. Google Chrome, Safari, Firefox, Chromium, and more. Not only this but it barely uses any resources and is scalable! For example, 1 mining tab uses about 3% GPU usage on a GTX 1060 6gb and you are able to scale this by adding more mining tabs. As mentioned before, this is in Alpha and certain devices may encounter issues but have you know, a man has already mined with his smart mirror!
Here are some resources and social medias you can check out to get more intel
https://bitcointalk.org/index.php?topic=2142232.0 (The bitcointalk post)
https://hadron.cloud/ (Hadron Homepage)
Hadron White Paper
Edit descriptionhadron.cloud
A platform for the discussion of the Hadron, a cryptocurrency driven by AI computations. *…
Founded by successful entrepreneurs from Stanford and Berkeley, Hadron is the first cryptocurrency that utilizes mining…www.reddit.com
Hadron
HADRON.cloud Web Mining for AIt.me
(Optional Referall, @UncleCletus in telegram chat to let them know how you found them)
Hadron
Decentralized Sharing Economyhadron.cloud
