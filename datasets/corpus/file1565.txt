The missile crisis of middle Earth and the place where innovation goes to die
“I know what your hearts are telling you.”
“The Gross National Product does not include the beauty of our poetry or the intelligence of our public debate. It measures neither our wit nor our courage, neither our wisdom nor our learning, neither our compassion nor our devotion. It measures everything, in short, except that which makes life worthwhile.” ~ Robert F. Kennedy [LI]

Pleonastic?
“If a thing can be done adequately by means of one, it is superfluous to do it by means of several; for we observe that nature does not employ two instruments [if] one suffices.” ~ Thomas Aquinas [LI]
The Admiral suggested our relationship will cause you pain.
You need to get to know me a little better.
