The partnership of HyperQuant and Zeus Capital

HyperQuant has established a partnership with Zeus Capital quantitative cryptocurrency hedge-fund.
As you know, we are building the platform for top-notch quant traders providing an efficient framework and tools for crypto asset management.
We’ve thoroughly investigated the trading approach of Zeus Capital. They have a proven reputation, that we have confirmed using our developments in blockchain technology.
HyperQuant provides our new partners with risk management software, while Zeus Capital is responsible for back-testing and maintaining trading algorithms. The transactions are stored on blockchain for the transparency and the safety of investors.
In the future we’ll announce the partnerships with other crypto traders and blockchain industry leaders.
HyperQuant Social Media




