Scaling Machine Learning at Uber with Mike Del Balso
TWiML Talk 116

In this episode, I speak with Mike Del Balso, Product Manager for Machine Learning Platforms at Uber.

Subscribe: iTunes / SoundCloud / Google Play / Stitcher / RSS
Mike and I sat down last fall at the Georgian Partners Portfolio conference to discuss his presentation “Finding success with machine learning in your company.” In our discussion, Mike shares some great advice for organizations looking to get value out of machine learning. He also details some of the pitfalls companies run into, such as not have proper infrastructure in place for maintenance and monitoring, not managing their expectations, and not putting the right tools in place for data science and development teams. On this last point, we touch on the Michelangelo platform, which Uber uses internally to build, deploy and maintain ML systems at scale, and the open source distributed TensorFlow system they’ve created, Horovod. This was a very insightful interview, so get your notepad ready!
Conference Update
Be sure to check out some of the great names that will be at the AI Conference in New York, Apr 29–May 2, where you’ll join the leading minds in AI, Peter Norvig, George Church, Olga Russakovsky, Manuela Veloso, and Zoubin Ghahramani. Explore AI’s latest developments, separate what’s hype and what’s really game-changing, and learn how to apply AI in your organization right now. Save 20% on most passes with discount code PCTWIML. Early price ends February 2!
About Mike
Mike on LinkedIn
Mentioned in the Interview
Michelangelo
Horovod
Horovod Github
Random Forest
Vote on the #MyAI Contest Entries Now!!
Register for the Artificial Intelligence Conference here!
Check out @ShirinGlander’s Great TWiML Sketches!
TWiML Presents: Series page
TWiML Events Page
TWiML Meetup
TWiML Newsletter
