Webography for 4 dummies to make it in machine learning — Chapter 21, Scene 1
Manifeste agile - Wikipédia
Le Manifeste pour le développement Agile de logiciels est un texte rédigé par 17 experts du développement…fr.wikipedia.org
Kanban
Kanban - Wikipédia
Un kanban est une fiche cartonnée ou un signal électronique ou simplement un emballage que l'on fixe sur les bacs ou…fr.wikipedia.org
KANBAN
Le terme désigne la méthode de gestion de production en flux tendu consistant à limiter la production d’un poste en amont d’une chaîne de travail aux besoins exacts du poste en aval.
C’est un mécanisme de type « juste à temps » permettant d’asservir la production ou l’approvisionnement d’un composant à la consommation qui en est faite.
Le poste de production ne produira que ce que demande le poste précédent ou ce qui est nécessaire pour recompléter la tâche en cours. L’ensemble du flux de production est donc piloté par la demande.
Chaque besoin (ou lot) est représenté par une carte (ou Kanban). L’ensemble des cartes du poste qui sont sur le planning Kanban représentent l’ensemble du carnet de commandes en cours.
Si, sur le planning il y a des étiquettes, cela signifie que le poste à de la production à réaliser. Si celui-ci est vide, cela signifie qu’il n’y a plus rien à produire.
Le support de l’ordre de reconstitution est une étiquette accrochée à chaque lot qui est produit ou approvisionné. Tant qu’il y a des étiquettes dans le tableau, le producteur prend la dernière, réalise le lot correspondant, y fixe l’étiquette et fait parvenir ce lot au consommateur.
http://www.cienum.fr/sites-internet-mobiles/projets/methodologie-de-projets/kanban
->
Principes et intérêts
L’approche Kanban consiste globalement à visualiser le Workflow (Le processus de traitement d’une tâche). On met en place un tableau de bord des items (demandes). Chaque item est placé à un instant donné dans un état. L’item évolue jusqu’à ce qu’il soit soldé. Chaque état du tableau peut contenir un nombre maximum prédéfini de tâches simultanées (défini selon les capacités de l’équipe contrairement à la méthode pomodoro) : on limite ainsi le WIP (Work In Progress). Il est primordial, pendant l’exécution des tâches, de mesurer le “lead-time”. Il s’agit du temps moyen pour compléter un item. Cette durée sera progressivement de plus en plus courte et prévisible.
Les intérêts de la mise en place de cet outil Kanban sont principalement :
Possibilité de mise en place progressive de la méthodologie Agile (moins directif que Scrum)
Les points de blocages sont visibles très tôt. La collaboration dans l’équipe est encouragée pour résoudre les problèmes de manière corrective, le plus tôt possible.
On peut se passer de la notion de sprint. Un sprint d’une ou deux semaines n’est pas envisageable dans certains cas de figure (réactivité supérieure exigée). La méthodologie Kanban est donc utilisée dans des services de support au client (gestion des tickets d’incidents).
Facilité de communication sur l’état d’avancement du projet.
La Définition du Done (Ensemble de critères permettant de considérer la tâche comme traitée) permet de garantir un niveau de qualité constant et défini de manière collective.
Différences Scrum/Agile :
https://www.nutcache.com/fr/blog/kanban-vs-scrum/
Généralement, on a tendance à utiliser le Scrum pour le développement d’une application et le Kanban est plus fréquemment utilisé pour les projets en TMA (Tierce Maintenance Applicative), c’est à dire la maintenance d’une application effectuée par un prestataire externe à l’entreprise, souvent en mode « Ticketing« .
Kanban vs. Scrum: What are the differences? | LeanKit
Many Scrum teams also use Kanban as a visual process and project management tool. While some teams prefer to use only…leankit.com
7 key differences between Scrum and Kanban | UpRaise
Project management world, especially software project management world is abuzz nowadays with two approaches - Scrum &…upraise.io
MySQL :: Sakila Sample Database :: 5 Structure
The following diagram provides an overview of the structure of the Sakila sample database. The diagram source file (for…dev.mysql.com
https://upload.wikimedia.org/wikipedia/commons/2/24/Extreme_programming.svg
XP flow chart
ExtremeProgramming.org home | Development | Project | Starting XP |www.extremeprogramming.org
Microsoft OneDrive - Access files anywhere. Create docs with free Office Online.
Store photos and docs online. Access them from any PC, Mac or phone. Create and work together on Word, Excel or…onedrive.live.com
Microsoft OneDrive - Access files anywhere. Create docs with free Office Online.
Store photos and docs online. Access them from any PC, Mac or phone. Create and work together on Word, Excel or…onedrive.live.com
https://i.pinimg.com/originals/69/1f/18/691f18a99d54c4d2723f96f7c88c9195.jpg
Talend Big Data Sandbox
Prenez le raccourci pour accéder aux Big Data avec Hadoop, Spark et le Marchine Learning.info.talend.com
9 OpenCV tutorials to detect and recognize hand gestures
The interaction between humans and robots constantly evolve and adopt different tools and software to increase the…www.intorobotics.com
Whose Sign Is It Anyway? AI Translates Sign Language Into Text | The Official NVIDIA Blog
Deaf people can't hear. Most hearing people don't understand sign language. That's a communication gap AI can help…blogs.nvidia.com
BelalC/sign2text
sign2text - Real-time AI-powered translation of American sign language to textgithub.com
https://ieeexplore.ieee.org/document/8227483/ https://hal.inria.fr/hal-01678006/file/deep_learning_action.pdf
[1711.11248] A Closer Look at Spatiotemporal Convolutions for Action Recognition
Abstract: In this paper we discuss several forms of spatiotemporal convolutions for video analysis and study their…arxiv.org
syed-ahmed/signsei
signsei - Automated Video Captioning Framework for American Sign Language Researchgithub.com

The 20BN-JESTER Dataset | TwentyBN
A large densely-labeled video dataset of generic human hand gestures.20bn.com
Why Scrum Is No Longer My First Choice
Why I have stopped using Scrum and moved to Kanban for software deliverymedium.com
