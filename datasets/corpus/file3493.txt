Datanieuws binnen en buiten het Rijk 04–07–2018
Save the date! 2e Data Meet-up Rijk op dond 27–09 (middag), Cathy O’Neil in NRC: “De valse magie van algoritmes” en artikel iBestuur Perry van der Weijden, Ric de Rooij en Johan Maas over oa Datagovernance
Infographic (s) van de week
Van BZK: over het gedeeltelijk verbod over gezichtsbedekkende kleding:
Van SZW bij het rapport Evaluatie van de Wet inburgering 2013:
Van BZK over de WNRA:
Data Vacatures bij het Rijk
De BD: Senior Data Analist, Data investigation specialist, Business Analist, Business Intelligence Developer,Business proces management specialist,
BZK (Rijks ICT Gilde):Medior software-engineer Rijks ICT-Gilde
EZK (DICTU):Ontwikkelaar business intelligence, BI-beheerder
Agenda:
Zomercursussen rondom data en statistiek aan de UU
En specifiek vanuit het CBS in samenwerking met de Universiteit Utrecht een‘Summer school on Survey Research’
4 juli: When Data Meets Disinformation (Public Keynote Lecture Summer School ‘Data in Democracy’)
18 september: Kick-off bijeenkomst pioniersnetwerk Open Overheid
19&20 september: Big Data Expo
27 september Stationscollege: Big Brother is guiding us (door Nart Wielaard)
Datanieuws binnen het Rijk
In iBestuur vanaf pag 21: Perry van der Weijden, Ric de Rooij en Johan Maas over Datagovernance, datagedreven werken en de Nationale Data Agenda. Een bijdrage van Jet van Eeghen in iBestuur.
Op publiekDenken: Interview met onze Staatssecretaris over de digitaliserings strategie
Nieuws van AP: billboards met slimme camera’s mogen niet:Autoriteit Persoonsgegevens informeert branche over norm camera’s in reclamezuilen
bestuurslid Gerben Everts van de AFM in een interview: we bezig de AFM om te vormen van een meer reactief naar een meer anticiperend en data gedreven organisatie. Nu al monitoren wij realtime de dagelijks beursontwikkelingen. Als zich opvallende koersbewegingen voordoen, kan worden besloten een onderzoek in te stellen. We zien steeds meer.”
Eerste editie van het magazine Nederland Digitaal van de Rijksoverheid
Min JenV: Kamerbrief over ICT-vernieuwing politie met een rol voor data en intelligence
Via VWS: Spoed Eisende Hulp toont met appp hoe lang het duurt


Datanieuws buiten het Rijk
Data en AI binnen overheid:
In The Times: Ministers need to start basing policy on hard evidence
van Tom Ford: Inventing a democratic vocabulary to free us from the tyranny of experts — algorithms in government.
Op towards datascience.com: Detecting Financial Fraud Using Machine Learning: Winning the War Against Imbalanced Data
Instituut voor de Fysieke Veiligheid: Startsein voor landelijke dataverzameling en -analyse
Op gemeenten van de toekomst: Fysieke Leefomgeving / Smart cities: durf te experimenteren! over voorbeelden uit Zwolle en Nijmegen
Op pEW trusts.org: How States Can Gather Better Data for Evaluating Tax Incentives Solutions for compiling and analyzing information on key economic development programs
van Bloomberg.com: Here’s How Not to Improve Public Schools The Gates Foundation’s big-data experiment wasn’t just a failure. It did real harm.
Commercieel alert, maar wel leuk: Strandbezettingsmodel voor Veiligheidsregio Haaglanden door Yformed
Data en AI algemeen:
In The FT: IBM’s debating computer suggests human brains are nothing special
Op The next web: Chinese AI beats 15 doctors in tumor diagnosis competition
Op medium: Want to be more data-driven? Ask these three (actually four) questions
Gatesnotes.com: Memorizing these three statistics will help you understand the world
Privacy & Ethiek
van MIT Sloan: Wait-and-See Could Be a Costly AI Strategy
terugblik: — ARTIFICIAL INTELLIGENCE SPECIAL: BEST PRACTICES AI-TOEPASSING IN HET OVERHEIDSDOMEIN
Artikel van DGOO/DIO collega Michiel Rhoen over AI en indirecte discriminatie, ism onderzoeker van de UU is vorige week gepubliceerd in het tijdschrift International Data Privacy Law (IDPL).
In NRC: De valse magie van algoritmes, Cathy O’Neil wiskundige/ Sinds haar slechte ervaring op Wall Street waarschuwt wiskundige Cathy O’Neil voor de gevaren van computersystemen.
The Gadget That Boosts Your Step Count While You Nap: “In April, Guangdong University of Foreign Studies in southern China announced that it would require students to reach at least 10,000 steps per day on WeRun. Some students found the target too high — fueling more demand for WeRun hacks.”
grappige read in The New Yorker: I Am the Algorithm
Op MIT tech review: Let’s make private data into a public good
In het FD: When algorithms go to war in the workplace,Businesses crunch data to gain power; workers should bend it to their own ambitions
In The FT: Someone must be accountable when AI fails
Op The Verge: SELF-DRIVING CARS ARE HEADED TOWARD AN AI ROADBLOCK. Skeptics say full autonomy could be farther away than the industry admits
Op medium: This Algorithm Can Tell Which Number Sequences a Human Will Find Interesting
HR analytics:
Op HI-re.nl Artificial Intelligence in recruitment — Welke ontwikkelingen staan ons te wachten?
Impact op de arbeidsmarkt:
in the FT: Work in the age of intelligent machines: How do you organise a society in which few people do anything economically productive?
Discussion paper van Mc Kinsey: Skill shift: Automation and the future of the workforce
Op CIO.com: Artificial intelligence gives HR an opportunity to transform the enterprise. Human resources has to play a strategic role in leveraging AI to shape future of work and organization agility.
Voor de Nerds
Artikel van Stanford: Text as Data: An ever increasing share of human interaction, communication, and culture is recorded as digital text. We provide an introduction to the use of text as an input to economic research.
Op towardsdatascience.com: What’s New in Deep Learning Research: Learning by Comparing Using Representational Similarity
Op towardsdatascience.com: Building simple linear regression model for a real world problem in R
Op towardsdatascience.com: Three techniques to improve machine learning model performance with imbalanced datasets
Op R-bloggers.com: The Financial Times and BBC use R for publication graphics
op datacamp.com: The Hidden Revolution in Data Science
Blog: (Data) Valkuilen bij A/B testen
Mooi overzicht van @lindaterlouw and @boonzaaijer over Datascience, Big Data Science en Machine Learning
Op towardsdatascience: Introduction to Model Trees
Cartoon van de week
IoT Cartoon:
