AdHive: the introduction of the team

The team and advisors are the engine of any blockchain project. After all, not only the idea itself is important, but also its professional implementation. Everything depends on the qualification of the selected team, so we want briefly tell about our participants.
Professionals of the most diverse branches are developing our platform. Let’s get to know AdHive better.
Who brings AdHive to the success
AdHive was co-founded by Dmitry Malyanov and Vadim Budaev, serial entrepreneurs with background in B2B services, machine learning & speech/visual recognition at Scorch.ai and Webvane. Dmitry has over 10 years of experience in sales and management, including Groupon. Vadim has worked as system architect and team leader for over 15 years.
The third co-founder, Alexandr Kuzmin, is an experienced trader and a former professional poker player. At AdHive, he leads model development and financial management.
Also the team includes:
Anna Tolstochenko — expert in working with advertising agencies and bloggers. Has more than 10 years experience in digital marketing;
Dmitry Romanov — strategy and business development;
Kristina Kurapova — specialist of legal support of the project;
Vitalii Tkachenko — art director and designer;
Denis Vorobev — software and AI developer;
Denis Dymont — complete and front-end developer of the stack;
Nikolay Papaha — software developer.
As you can see, the team consists of the real professionals with a long-term experience in their field. Advisors are also valuable for the startup. The following experts help to reach AdHive the heights:
Sergei Popov — scientific and token product concept advisor. The mathematical model and incentives of ADH have been developed by Serguei Popov, professor of mathematics at the University of Campinas, Sao Paulo, Brazil and AdHive’s scientific advisor. Sergey had previously authored the mathematical models of WINGS and contributed to the theoretical work on another crypto-currency, NXT.At AdHive, his models help the system distribute rewards within each acting group in a fair way that incentivizes users to learn more and become better at their roles, for the benefit of every individual user and the platform in general.;
Ivo Georgiev — ad tech advisor;
Ariel Israilov — investment advisor;
Larry Christopher Bates — community advisor;
Sergey Logvin — HR advisor, -investment and community management;
As you can see, AdHive has a highly professional team, as well as experienced and reliable advisors. The last and very important part of our team is our users. Join us and together we will bring video advertising a new quality, integrating the principles of influencer marketing and AI technology into the market.
