The story behind the AI-driven startup Deevio
From an industrial problem to a cutting-edge quality control solution
Introducing Deevio
Ever wondered how startups come to life? Let me introduce you to Deevio and the story of how we went from idea to reality.
Deevio comes from the selective innovation process of the Berlin-based deep tech company builder WATTx — a specialized, cross-functional team that is built to create the next wave of industrial startups. At the core is a user-centric approach to ideation and new business creation where only the best ideas survive.
So, let´s go six months back in time and see the process behind building the next generation of industrial quality control.
The idea: deep learning for image classification
The project started when we were approached with the idea of using deep learning for image classification in the fields of industrial quality control (machine vision). We were keen to give the idea a try, knowing the potential of the technology in manufacturing.
Experimenting with the tech
A week later, we received the first two data sets to work with and it quickly became clear that we had something special here. We rapidly achieved accuracy results of close to 100% with low resolution images using state-of-the-art data science methods. When presenting these results to experts from the machine vision industry, they did not believe what they saw — as the prevailing opinion is that the higher resolution you have, the better accuracy you will get. With our approach, that was no longer the case. This was the first indication for us that there in fact was great potential in the idea.
Summary of the first training results — high accuracy on downscaled images
Key takeaway: you don’t need expensive cameras, good algorithms get good results even on low-resolution images.
But wait: is there an actual problem to solve?
The first results looked promising, however, just having a great technology without a clear problem to solve is never a good start for a startup. It is something we at WATTx try to avoid at all costs by doing extensive research before committing to new projects, both from the user experience and the business side.
Quality control in manufacturing is often still done manually
So, before investigating the technology further, we spent time better understanding the process of quality control and visual inspection in the manufacturing space. We soon found out that quality control in manufacturing is often still done manually as traditional, rules-based machine vision systems fail to address a lot of use cases. This manual work is exhausting for the operators doing it and prone to errors as it is impossible for humans to detect every small defect of a product through an 8-hour shift.
We also found out that even though machine vision technology has been around for years, rules-based systems’ shortcomings limit a widespread usage of the technology. Especially the long setup time, the lack of flexibility and the high costs are limitations that came up regularly in the interviews.
Key takeaway: deep learning gives the flexibility needed to make machine vision a practical solution, as well as, massive cost benefits.
Is there a market for it?
Besides the strong interest we got from our interview partners and the potential of AI applications in factories, the spending on quality control is going from one record high to another. With this last piece of validation, we went on to a final sense check with the WATTx team to discuss whether to pursue the project or not.
In the meantime, we did another validation of what we can do with Deep Neural Nets: classifying a set of nails we bought at the hardware store next door
Key takeaway: When we overcome the limitations of machine vision systems, we believe that we can significantly increase the market demand for it.
Finally, Do or Die?
The goal of do-or-die sessions is to kill the project by finding reasons not to pursue with the project in order to detect all possible shortcomings. This way we ensure that only the most promising ideas survive and eventually become independent companies.
Since we had a clear problem to solve, strong results on the tech side, positive feedback from industry experts and first and foremost a team that was highly passionate about the project, we decided to bring this project to life.
The prototype: from nails to an actual case
Now, we had to tackle one of the hardest tasks when starting a startup — coming up with a name. After lots of meetings, brainstormings and ideation sessions, we came up with Deevio.
Deevio.ai - a name combining both worlds, artificial intelligence and machine vision
We continued to get a lot of positive feedback and ideas for use cases for Deevio from industry experts and soon these conversations turned into discussions on pilot projects. These became our first feasibility studies.
Parallel to working on the new data sets from industry partners, we experimented with different hardware platforms, industrial cameras and ways to integrate our solution to PLCs (these guys basically send the signals on production lines and are important for automating processes in factories) to build our first working prototype.
Our first prototype: a machine vision camera, semi-professional lighting, an Nvidia Jetson TX2 running our deep learning model for classifying the nails and an interface showing the output
Really understand the user needs
Then it was time to step up the UX research and design: we travelled throughout Germany to visit various factories and talk to the people working there. In this way, we ensured that we designed a solution that is best suited to the end-user needs. We got feedback from both line and plant managers as well as quality assurance representatives and used it to design the first interfaces. One major finding was to keep the interface for the operators as simple and clear as possible to avoid any uncertainties. Thus, instead of adding lots of features in the beginning, our designer came up with the most minimalistic interface he ever designed.
The Deevio team at the factory of our partner Viessmann with the mission to better understand product operators and identify potential application areas
The prototyping phase ended with a confirmed customer, ongoing feasibility studies, various other promising pilot projects in the pipeline and a lot of new insights from seeing all these factories and talking to users.
The MVP: launching Deevio
We are now launching Deevio with the mission of augmenting and helping the people that carry out visual inspections and providing real-time insights in the production lines for manufacturers.
If you want to know more about Deevio and our journey, stay tuned. Soon we will tell you more on what it’s all about, how we designed the customer interaction and how exactly we are supporting operators in their manual inspection tasks using deep learning.
***
If you’re interested in working with us or if you’re already having a use case from your production environment in mind, feel free to reach out to me directly at damian@deevio.ai or via our website.
