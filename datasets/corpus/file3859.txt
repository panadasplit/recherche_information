Rockit Conference Moldova 2018: ideas that inspire
27th of April is the day that united tech and entrepreneurship enthusiasts in Radisson Blu for the coolest event of the year. To sum up all the presentations and panels, I made a list of ideas from Rockit speakers:
Social media promotion
Show your work, everyone is an artist. Differentiate yourself, be a creator.
Internet made people too loud, people want to be heard on social media.
Have a clear message that comes from the heart.
Post things for a specific audience, do your research.
Follow a schedule, be consistant, daily posts are most effective, quality is important.
Understand your goal, find personal style
New digital era
Artificial Intelligence (AI) will free us from boring work.
Before every industrial revolution people were afraid of new technology and didn’t accept it right away.
Only 5% of jobs will be totally replaceable.
