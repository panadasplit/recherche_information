AI CHALLENGER GLOBAL CONTEST IS ON!
I’m so excited to announce that our #AIChallenger global competition platform is now officially online and all datasets are ready for download at challenger.ai.
Large open Datasets include:
Human Skeleton Keypoints Dataset consists of 300,000 images with over 700,000 people.
Chinese Language Image Caption Dataset consists of 300,000 images with 1.5 millions Chinese captions.
The English-Chinese Machine Translation Dataset consists of over 10 millions Chinese and English paired sentences.
US$300K Prize for 5 competitions include:
Human Skeletal System Keypoints Detection
Scene Classification
Image Captioning (Chinese)
English-Chinese Simultaneous Interpretation
English-Chinese Machine Translation.
Free GPU Resources
Free GPU resources to those who have limited budget, but are passionate about AI and ready to take actions.
Connect with AI talents around the world.
Since we announced AI Challenger on August 14, talents and institutions have registered include:
Students from Cornell University, Georgia Tech, NYU, University of Cambridge, Imperial College London, Karlsruhe Institute of Technology, Ecole Nationale des Ponts et Chaussees, University of Wollongong, Waseda University, Tsinghua University, Peking University, Chinese Academy of Sciences, Shanghai Jiao Tong University, Fu Dan University, Hong Kong U. of Science & Technology, Chinese U. of Hong Kong, National Taiwan University and more!
Developers from Microsoft, GE, Intel, eBay, Micron, BNP Paribas, Baidu, Alibaba, Xiaomi, Sohu, Qihu360, ZhongAn Insurance, China Mobile, China Telecom, DeepGlint, UISEE, Mobike and more
Champions and leaders from other top AI competitions such as Kaggle and Tianchi’s.
Winners from AI Challenger will have chances to job/internship or obtain investment opportunities with the organizers, present at top conferences, and receive advice from top experts such as former Microsoft Research senior researcher Yi MA, Former Google Senior Staff Research Scientist Dekan LIN, and Megvii Technololy Chief Scientist Jian Sun.
Competition Timeline
Training dataset is available for download after 10:00AM, September 4, 2017.
Open for registration until 23:59:59 on October 31, 2017
Final ranking will be determined by the scores at 23:59:59 on December 3, 2017.
Top 5 teams on the final leaderboard of each major competition will be invited to present their work onsite at AI Challenger finalist award event in Beijing in midÂ December.
Each competition will also have bi-weekly or weekly prizes.
Please help to pass the word, and more importantly, join AI Challenger, and show your talent to the world!
All at challenger.ai
