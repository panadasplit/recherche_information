Google Analytics “session” explained for dummies.
Yet another one story about analytics. But this one will be different. This story will not teach super advanced way how to calculate CLTV or ROMI. This read will try to explain what is one simple metric — sessions. And me, author, will try to make sure you will have the easy time reading it. Varom.
Things you must know before reading it:
What is a browser
What is a webpage
Who is the author of this golden post
If you are reading this (if at least someone is reading this) you should know what is a pageview in Analytics. For those who don’t know — it’s one-page load by a user. So if you grandma goes to www.recipes.com/kugel Google Analytics will count it as one pageview for kugel recipe page. Simple. Every page load from a user is a pageview by a user. In hard words — a page is a dimension, pageview — a metric. 
“Dimension??? Wth, is it the matrix or what?” Nah, it’s Google’s dimension. It’s like an X on XY chart.
Dimension in Google Analytics is like x line in xy chart
For those who had idea what pageview is, and now stopped believing the story I’m telling. You are right and I’m wrong. I have described a pageview from data point of view not from techinacal, where pageview is a simple pageload.
If you read this you are one of 20% who started reading this or one of my relatives (usually half of those 20%). Let me tell you another metric which is few times more complex — session.

In simple words, a session is all pageviews done by a user in one browsing session (that’s why it’s called sessions probably). So if you brows Page1->Page2->Page3 without going to have a coffee, you had a session of three pages.
Why did I add a coffee break to my sentence? Because Google Analytics identify sessions by timeouts between pageviews. All it scumbag cares is a time between pageviews. If you opened page1 and after short time page2 it’s a single session. But if you had a longer break, most probably, GA will count it as a new session. Clarifying “most probably” it’s 30 minutes timeout, but you can adjust it to whatever you like.

To make things harder I have to say that, sessions are totally another dimension than pageviews. Aww, ugly truth. Sessions is not a dimension, it’s a metric, but it can’t be combined together with pages. “Hypocrite! It’s made of pageviews, why I can’t combine it reports?”. Because, as I said, it’s different dimension, layer, level call it what ever you like.
Now I will leave you here. I won’t go deeper into details because it will make your head and my heart hurt. What I have said take as facts and only true. Remember that session is made of pageviews (or events if you have any). And compute it in your head.
See you next time, peace.
