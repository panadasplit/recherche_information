Data Pre-Processing for Adult Data Classification
If you read my post that I shared just before this one, you can get information about the script that will reshape my data-set to be used for the classification algorithm which is made by me before.
So, what would we do ?
First, we had a data like this:
25, Private, 226802, 11th, 7, Never-married, Machine-op-inspct, Own-child, Black, Male, 0, 0, 40, United-States, <=50K.
38, Private, 89814, HS-grad, 9, Married-civ-spouse, Farming-fishing, Husband, White, Male, 0, 0, 50, United-States, <=50K.
…
We wanted our data to be a matrix so that we can use it for our classification algorithm. After I write the script that reshapes data from a bunch of text to matrices, I finally got an output like this:
0 0 1 0 0 0 1 0 1 0
0 0 0 0 0 0 0 1 0 0
0 0 0 1 0 0 0 0 0 0
0 0 0 0 0 1 0 0 0 0
1 0 0 0 0 0 0 0 0 0
0 1 0 0 0 0 0 0 0 0
0 0 0 0 0 0 0 0 0 0
0 0 0 0 0 0 0 0 0 0
0 0 0 0 1 0 0 0 0 0
0 0 0 0 0 0 0 0 0 0
0 0 0 0 0 0 0 0 0 0
…
The exception that I did not tell in previous post:
1- I filtered the continuous data except ages of people.
2- I classified the ages in a way like this:
I divided the age of person by 10. Then, I rounded it. What did I actually do ? I just rounded the numbers to the closest number that can be divided by 10.
So, I give you the script below, hope it becomes useful for you, and you catch up with my next posts to see the result.
Wish you a great day…
