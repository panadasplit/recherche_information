Web Summit 2017 and the post-conference blues
I’ve just got back from my first Web Summit, and this was my experience.
Centre Stage at Web Summit 2017
The first thing that really hits you when you arrive is the sheer scale of Web Summit — with over 60,000 attendees from over 170 countries it feels much more festival-like than any conference I’ve been to before, including a heavy police presence and airport-style security.
That’s both a positive and a negative — it creates an atmosphere which draws you in and makes you feel like part of something much bigger, but it doesn’t come without hundreds of micro-stresses as you try to move around the venue, get to (and from) stages, and undoubtedly miss many fascinating talks because of schedule collisions.
As I’ve found to be the case at other conferences I’ve attended this year, the non-technical talks are much more engaging to me than the technical ones. The moderated discussion format also works incredibly well, and it surprises me that other conferences haven’t adopted this style.
The entire event runs like clockwork, and although most sessions started at least a little bit late and often overran, it wasn’t particularly noticeable or bothersome. That’s a testament to the hard work of the many organisers and volunteers who make this happen.
The variety of talks I ended up seeing were a pleasant surprise — I’ve included a (nearly complete) list at the end of this post — and most of them on topics I wasn’t even consciously aware I was interested in (I spent most of my time at AutoTech/TalkRobot and Future Societies).
For me, Al Gore’s speech on “The innovation community’s role in solving the climate crisis” was the clear Web Summit winner, but it would be unfair to compare any other talks to his. You can already watch it unofficially on YouTube if you’re interested.
There was a heavy bias towards AI, automation and our planet’s future throughout Web Summit (at least for the schedule I had!), but the different themes, perspectives and formats helped minimise potential burn-out.
My main takeaways from Web Summit:
Technology is unique in disrupting pretty much everything
Some of the biggest challenges we face are ethical rather than technological
Code literacy could be (or is) as important as reading, writing and mathematics
We sometimes focus too much on technology rather than the problems it can solve
Ownership, sharing and use of data needs to be much more transparent
Super-intelligence is a very real ‘risk’ — we should be prepared and take AI safety seriously
Some other thoughts worth mentioning:
‘Flying cars’ are already a thing — but they’re not really cars in the traditional sense
There’s an insignificant 20 million software developers, versus the 3.2 billion people online
The porn industry, though contentious, is a significant driver of technology innovation
Other industries demanded international treaties (e.g. a ban on bio-weapons) — AI needs the same
Technology can be used for good and bad — that’s our collective choice to make
So were there any negatives from Web Summit? Some, definitely, but they’re outweighed by the overwhelmingly positive experience:
The chairs were particularly uncomfortable
Tea doesn’t get the same kind of love as coffee
The mobile app was a little slow (and apparently worse on iOS than Android)
Perhaps the two most important ‘negatives’ from Web Summit, if you can call them that, are these:
My world-view has been irreversibly changed
Post-conference depression might be a thing
But I think that says more about the seemingly insignificant life choices which have, rather accidentally, got me to where I am today.
Web Summit has given me a lot to think about — both about our collective future and my own way forward in life. I expected Web Summit to be interesting, inspiring and motivational, but I hadn’t expected it to make me re-evaluate my place in the world and how I can make a more meaningful contribution.
In conclusion — if you get the opportunity to go next year, you won’t be disappointed!
My schedule

