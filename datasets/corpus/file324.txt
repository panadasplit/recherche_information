Virtual Reality: Do You Really Need It? This Will Help You Decide…

“What virtual reality does is force you to explore and discover”
About a week ago I attended “VR Augmented And Mixed Reality Event” as part of #SPIFFEST2017 (San Pedro Film Festival) and it was an eye-opening experience to hear and see what was being said about what’s controversial in the world of technology.
Aside from having the privilege of collaborating with an innovating local film festival in my town was looking forward to learning about virtual reality as I missed it last year. Overheard technology and innovation was coming to town last year and glad I was able to attend this year.
I share this blog with the intention to spread awareness as main talks were about increasing familiarity and educating people. Whether you’ve heard of virtual reality / artificial intelligence or not it’s becoming more common as technology expands.
Virtual Reality Technology
Is mind-blowing however there are many knots to untangle as far as responsibility content. Stats show in the United States alone it’s harder than other countries to teach responsibility and ethics. In other parts of the country for example Europe or Africa its acceptable due to culture differences to be “naked” for example while here in the United States it’s not acceptable and would be described as indecent exposure and you might get fined so in other parts of the world responsibility of content does not apply.
It Boils Down To Society And Culture

Artificial Intelligence
Has it’s own “chatter” language we don’t quite understand and that’s alarming. Facebook had shut-down an experiment after two artificially intelligent programs seem to be chatting to each other in a bizarre language only they understood. I don’t know about you but to me that is creepy!
Curious? You Can Read Forbes Article Here
Facebook AI Creates Its Own Language In Creepy Preview Of Our Potential Future
Facebook shut down an artificial intelligence engine after developers discovered that the AI had created its own unique…www.forbes.com
Today’s twitter popular trend was about a sarcastic robot called “Sophia” and became the first bot in the world to be recognized with Saudi citizenship!
Here’s what Sophia had to say:

Twitter Post 10/26/2017 @NotthatkindofDr
I found it funny at first but then said Whoa! Is this what robots are being programmed to say?
So that brings me to talk about the responsibility of virtual reality. Sophia is just one of many first robots designed (let’s not go into the dark side of sex robots) That’s another blog!
Accelerate learning is not about censorship it’s about choices. It’s using power for good not evil. For achieving big goals together. Ethics and morals.
Accelerate Learning
Is the most advanced teaching and learning approach used today. It’s a system used for both speeding and strengthening the design and learning process. It’s not used to censor but to be responsible. Know when boundaries have been crossed because when it’s in assimilation it doesn’t know. (The enactment of testing robots) Yes! robots don’t know just as much as our brains can’t tell the difference between the truth and a lie
Did you know?
Recently Facebook CEO Mark Zuckerberg got into a big boo-boo with Puerto Rico for his recent virtual reality tour and apologized for offending anyone with his video.
You can read Mashable article here
Mark Zuckerberg apologizes for that awkward VR tour of Puerto Rico
Mark Zuckerberg has apologized, in a Facebook comment, for his recent virtual reality tour of Puerto Rico. The Facebook…mashable.com
So just because we can use virtual reality should we do it?
That all depends how it’s being used, what part of the world and if it’s making an impact. Something to think about.
As we move forward we need to think what’s okay to do and what’s not okay? (digital universals)
Hollywood is still not ALL-IN with virtual reality however there are some films that are brave such as the movie “Jungle Book” and Oscar nominated “Pearl”
Check them out …
Disney Launches 'Jungle Book' VR Experience
Yesterday The Walt Disney Studios released an interactive 360-degree video and virtual reality experience tied to the…www.awn.com
Watch PEARL: The First VR Animated Short To Earn an Oscar Nomination | Geek and Sundry
While the concept of virtual reality has been around for decades, it's still a relatively young medium for filmmakers…geekandsundry.com
There’s momentum in virtual reality. Bloggers can be captivating, get engagement and dialogue started on V/R. Initiate conversations regarding rules and what’s appropriate. Virtual reality is good for non-profits, artists, painters and story-telling experiences.
There’s a great demand with VR and Augmented reality (AR) in training tractor trailer drivers. UPS enhances driver safety training with virtual reality.
Curious? See Article here:
UPS Enhances Driver Safety Training With Virtual Reality | Virtual Reality Reporter
UPS Enhances Driver Safety Training With Virtual Reality VR experience to debut at nine U.S. Integrad® facilities this…virtualrealityreporter.com
Reason why many filmmakers are not jumping the VR reality technology train is due to lack of (ROI) return on investment.
Many quoted:
“If I’m not generating anything out of this then I’m not going to invest in it”
It’s risky business for filmmakers however I believe the dilemma is they’re afraid to innovate and lose control how we see films as virtual reality explores 360 degrees and filmmakers don’t like to expose everything in order to capture the imagination.
Innovation is pretty tricky to what’s tried and true and what’s working now yet that will leave you stuck in the mud as we are experiencing changing times. Get rid of fear.
What Prevents People From Virtual Reality?
Hardware, Software and Content…
Reason why many don’t invest is the cost. It ranges from $2,000 and up as you need a compatible virtual reality headset that requires a desktop computer. A laptop doesn’t do the job as it doesn’t have graphics and hardware powerful enough to run a VR headset.
As innovation grows I believe it will become less tedious to purchase the equipment and system in order to facilitate the art of virtual reality. We are in beginning stages and people are just getting their feet wet.
There’s not much content out in social media. Bloggers are starting to spread awareness and we have some of the intelligent minds such as Elon Musk, Bill Gates and Stephen Hawking warning us about the dangers of artificial intelligence (AI)
Whether AI or AR (virtual reality) this blog was written as a glimpse of awareness on what to expect more of in the following months, years… as technology thrives.
I was honored to be at this event and work this fascinating paid film project as a blogger in behalf of San Pedro Film Festival and look forward to attending and becoming more self-aware of technology within the following years…
Sharing is fascinating… Please feel free to share with your friends
“Technology is entrepreneurial you can learn a lot from it’s success and loss”
Laura Ramos / Branding Fascinista
