Writing for Fast Cash!
The following are three quick tips for earning money by writing:
Be in it for the right reasons. This post will get a disproportionate number of views because everyone thinks they can write and phrases like Fast Cash are catnip to the masses.
Realize good writing is inversely correlated with speed. Did you really think you could just vomit into Word and your PayPal would explode?
Stop clicking on these links. The reason clickbait exists is because people literally can’t help themselves. As someone with a shred of conscience, I feel bad for those who truly think one of these links will eventually pay off. But as someone with a shred of duty to his fellow man, I must tell you that they will not. Ever.
If you want to be a writer, you have chosen an arduous journey; one with payoffs so infrequent and small as to discourage even the most passionate among us. Those not blessed with iron will and Armstrongian endurance have a better chance playing the lottery. Which you shouldn’t do either.
