Can a robot love us better than another human can?

I discussed this with Michelle Tsng on my Podcast “Crazy Wisdom”.
She says that a robot can love us better than a human being because there is no judgement. Human beings, particularly ones who have been traumatized can subconsciously detect when someone is judging them. They know when to keep their true feelings hidden from people who judge them and thus the best guide they can find is someone who can withhold judgmental thoughts and just express a safe, warm, and loving connection.
As robots become more sophisticated they might be able to provide this loving and warm connection. In this audio clip, Michelle discusses her experiences talking with Sofia, a robotic companion to human beings. She says that soon we will build robots who are better at love than humans are.
What do you think? Would you ever feel comfortable sharing your most intimate experiences or seeking therapeutic treatment from a robot?
You can check out the full interview on my website.
