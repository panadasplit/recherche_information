60 Seconds with WITS Southeast Keynote Speaker — Shaloo Garg
Shaloo Garg is the Head of Oracle’s Innovation and Global Startup Ecosystem, and a Global Advisor on Technology and Innovation for UN Women. Her keynote talk at WITS Southeast is “The Next Digital Frontier- Impact of AI & Machine Learning on Global Economy, Society and Philanthropy”.

Why is speaking at WITS important to you?
Diversity across all spectrums is an important component of a constructive conversation. WITS brings an audience from various verticals, levels of management and solution areas. Over the years, I have seen WITS grow and bring converstions to the table which are pertinent for change and growth for women and girls in our society.
What inspires you?
The strongest use case of technology is it’s use in the social impact space. What inspires me every single day is how technology is changing the world around us. What drives me every single day is to uncover use cases where conservative ways of working can be done away with the help of technology especially in under developed, developing and under served communities.
Most useful article you have read in the last one month
Want An Innovative Business? Start With Gender Equality
Want an innovative business? HP's Nate Hurst says that to do this you need to start with advancing gender equality…www.forbes.com
What did you want to be when you were a kid?
My dad was an aeronautical engineer and we often travelled with the family. I noticed that the air hostess in the airplane had a selfless job. They would serve food, water to the passengers with dedication. I was totally fascinated by that. For the longest time, I dreamt of being an air hostess :)
Blurb about your session
The Next Digital Frontier- Impact of AI & Machine Learning on Global Economy, Society and Philanthropy
Innovations in digitization, analytics, artificial intelligence (AI), and machine learning are creating performance and productivity opportunities for business and the economy, even as they reshape our daily lives and future of work. The wave of AI and Machine Learning is coming at us very rapidly. In fact, it has already permeated parts of our professional and personal lives. In this power packed session, learn about the impact of AI and Machine Learning on the Global economy, society and philanthropy. Let’s also talk about channels of disruption that will be the front runners in this race and these will be the ones that will be the game changers !
Whether we are want it or not- are you prepared to capture value from the oncoming wave of innovation of AI and Machine Learning? Let’s find out !
