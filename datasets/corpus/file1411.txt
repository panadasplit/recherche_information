Is ‘Minority Report’ already here?

Why does artificial intelligence (AI) get a bad rap in Hollywood?
Movies like Terminator, Blade Runner and Ex Machina all paint a future where intelligent robots threaten the existence of human beings.
Fortunately, in real life, AI has assumed less sinister forms. The recommendation engines from Netflix and Amazon give suggestions on movies or books based on your previous choices. Google Maps reroutes drivers to avoid traffic accidents on their way home. There are smartphone digital assistants like Microsoft’s Cortana and Apple’s Siri that answer questions about sports and weather.
But there’s another use for AI that does mimic sci-fi: personalised advertising.
In a scene in Minority Report, John Anderton (Tom Cruise) is bombarded by augmented reality (AR) billboards that call out his name.
(Embed or screenshot of scene: https://www.youtube.com/watch?v=3JNXuCaewOg)
Many brands are already using AI to create better customer experiences, and they are just scratching the surface. It’s been predicted that global spending on these cognitive systems will reach $31.3 billion by 2019. Accenture predicts that AI might double annual economic growth rates and improve labour productivity by up to 40% by 2035.
Although there is solid proof that AI can provide cost savings for many companies, the bigger goal for marketers today is using it to make their brand experiences even more predictive and personalised — enticing customers to buy more.
Consumer first
AI’s most public face is arguably IBM’s Watson, a cloud-based cognitive engine that claims to think just like a human, using machine learning and natural language processing. Watson is currently working with multiple brands in more than 20 industries across 45 countries.
GSK Consumer Healthcare, Toyota, Unilever and Campbell Soup Company have been using a cognitive advertising system called Watson Ads. By using data from The Weather Company, which IBM bought in 2015, Watson Ads help brands provide relevant one-to-one brand experiences that are built on new product and consumer insights uncovered by Watson. For example, consumers can ask Watson for Campbell soup recipe ideas, and Watson will answer with data built around personal preference, past choices, weather and even the groceries on hand.
Meanwhile, The North Face also worked with Watson to create a smart website that will match customers with the right gear for their needs. Sesame Street and Watson have also teamed up to build an AI-powered vocabulary learning app.
Other companies are choosing to build their own AI platforms. USAA, an insurance and financial services company, uses AI in its enhanced virtual assistant named EVA. Customers of USAA can ask Eva questions to help them navigate and search for the information they need. Although a more proactive interaction is being developed, EVA can already continually evaluate and review a USAA customer’s financial data and then reach out to them with personalised money-management suggestions.
Car company BMW also uses AI to improve owner experiences. The company’s opt-in system and personality mobility companion, BMW Connected, gathers data on their customers and the cars they use. It’s capable of checking traffic to suggest an earlier departure time, using ‘Time to leave’ alerts to act as a digital personal assistant, recognising and storing regular destinations, and showing the best way to a final destination even after exiting the vehicle.
Beyond cognitive advertising?
AI is driving advertising to a new frontier. It’s a cooler, less-annoying way to reach customers beyond programmatic and targeted pop-ups.
More than selling a product or a service, AI-powered products are helping brands promote an ideal that consumers can enjoy, whether it’s a connected home, a killer recipe or a safer ride.
Ultimately, brands will always look to make a profit first. They see AI as a tool to gain better insight into how consumers use their products. Take AI services with a grain of salt — they’re still data-gathering machines.
So is living in a Minority Report world such a bad thing?
Actually, brands have a lot to lose if their AI services become invasive or persistent. Consumers can opt out at any time, so it’s actually in the brand’s best interest to make their services focused on improving our lives. We can all rest easy knowing we probably won’t have to endure screeching AR billboards within our lifetimes.
The billions that advertisers are investing in AI could also benefit us on a grander scale. What brands are learning about consumer data can be used to improve healthcare, travel and even governance.
Learn to embrace the medium — but be critical about the message.

Originally published at rarebirds.io on November 27, 2017.
