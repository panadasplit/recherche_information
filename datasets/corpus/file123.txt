Application of Random Forest Algorithm in Remote Sensing Imagery Classification and Regression
Poem:
Even though I walk through the valley of the curse of dimensionality,
I will fear no overfitting,
for you are with me;
your bootstrap and your randomness,
they comfort me.
You prepare a prediction before me
in the presence of complex interactions;
you anoint me data scientist;
my wallet overflows.
Credit: (http://machine-master.blogspot.com/2014/02/random-forest-almighty.html)
According to Wikipedia, “Random forests are an ensemble learning method for classification, regression and other tasks, that operate by constructing a multitude of decision trees at training time and outputting the class that is the mode of the classes (classification) or mean prediction (regression) of the individual trees. Random decision forests correct for decision trees’ habit of overfitting to their training set.”
Brieman (2001) was the first person to propose random forests algorithm formally. It combines the concept of random subspaces and bagging. As the name implies, RF algorithm creates the forest with a number of decision trees. More decision trees (trees in short) mean more robust classification leading to high classification accuracy. Decision tree concept is based on rule-based system.
For example, we may want to classify the vegetation of East Africa using rules. In ArcGIS the conditional statement or rule would be based on precipitation(prec), the ratio of actual evapotranspiration to potential evapotranspiration(aetpet), temperature, vegetation growth days, etc.
Con(“aetpet” < 0.45 & “prec” < 125,6,Con( “aetpet” >= 0.45 & “petprec” > 1,5,Con( “prec” >= 125 & “petprec” < 1,4,Con(“aetpet” >= 0.45 & “mtcm” < 17,1,Con(“mtcm” >= 17 & “mtcm” < 20 & “gdd5” < 4000,1,Con(“mtcm” >= 20,1,Con(“mtcm” < 20 & “gdd5” >= 4000 & “aetpet” < 0.18,2,Con(“aetpet” > 0.18,3))))))))
Accordingly, we would be able to classify the potential natural vegetation types in East Africa, apart from cropland, using the above decision trees.
1. Forest and montane communities.
2. Moist Woodland.
3. Dry Woodland.(AET/PET>0.18)
4. Semi-Arid (non-semi desert).(AET/PET<0.45, PET/P <5 and Precipitation less than 700 mm)
5. Semi-desert. ( AET/PET<0.45, PET/P < 5, and Precipitation less than 500 mm)
6. Desert.( AET/PET <0.45 and PET/P >= 5)
Now we understand rule-based classification, let’s see how random forests are made. Given the training dataset with polygons of multiband image for each class (i.e. labelled data) , the decision tree algorithm will define the set of rules. The same set rules are used to test beyond the training data polygons on test datasets which are 1/3 of all sampled pixels.
The random forests algorithm trains a number of trees on slightly different subsets of data (bootstrap sample), in which a case is added to each subset containing random selections from the range of each variable. This group of trees are similar to an ensemble. Each decision tree in the ensemble votes for the classification of each input case.
Ensemble classifiers in RF are a group of classifiers or randomly created decision trees. Each decision tree is a single classifier and the target prediction is based on the majority voting method. Every classifier will vote to one target class out of all the target/output classes. The target class which got the most number of votes considered as the final predicted output class.
Random Forest (RF), as an ensemble learning method uses several weak classifiers for discrete data to classify discrete entities. For example, a given number of land-cover classes (built-up, vegetation, soil, water bodies) are classified from a multiband satellite imagery based on their distinct spectral properties in the different bands of the satellite sensor.
Input: Multiband imagery Output: Land-use land-cover classes
Let’s say the number of classes we want in land-cover classification system is five. First, we label some areas in the image as cropland, soil, built-up, forest, and water-body based on visual interpretation of their spectral reflectance properties. For example, in a false color composite (FCC) satellite image (For example: bands 543 in Landsat 8) forests are dark red, water bodies are black (absorb infrared). In addition, band ratios are used to enhance the difference between land covers. Built-up areas (NDBI: Normalized Difference Built-Up Index), vegetation and soil (SAVI: Soil Adjusted Vegetation Index) and water bodies (MNDWI: Modified Normalized Difference Water Index) are identified visually from the band ratio image.
We draw polygons of these land cover classes which are used to train the random forest model. This process is called supervised classification which means that the machine(PC) is trained to know what digital numbers (DNs) are represent each land cover and apply this knowledge to the rest of the image. Although this is a humanized version of a decision tree, the questions for the tree classes are:
Ø If a sampling polygon has high IR (Infrared Radiation) reflectance, then it may be a forest.
Ø If a sampling polygon has low reflectance in IR, then this may be a water body.
Ø If a sampling polygon has high NDBI, then this may be a built-up area.
Ø If a sampling polygon has a high SAVI, this may be a cropland, if has low SAVI then this may be soil.
On the other hand, in regression, predictors of vegetation biomass/ area/year in a given location may be regressed. Given the dependent variable (biomass) and several independent climatic predictors such as rainfall, temperature, vapor pressure, solar energy (continuous variables) and land-cover (discrete variable as dummy), we would be able to know how much of the variance (in percent) in biomass between places is explained by these dependent variables (See my previous posts).
The process flow of random forest algorithm
A random sample of the number of cases is taken, this may be compared with individual pixels in a multiband image. Subsequent samples for other trees are done with replacement (a pixel may be selected once or more and no pixel may be left out from the sampling).
A subset of variables (the number of which is represented by the term m) is chosen, being much less than the number of variables, and the best split (based on the Gini score) is determined on this subset of variables. Choose values of m that are not too low or too high. The m-number is the only setting in the algorithm to which the model is sensitive. i.e. The number of variables used in the algorithm largely determines the classification accuracy. The more variables we use the more the correlations between decision trees (bad) and the more the classification accuracy (less misclassified pixels). Decreasing m decreases the correlation between classification trees but will be less accurate or its predictive power diminishes.
From the sampled pixels, about one-third (1/3) of the cases are used to create testing data set. These pixels have no labelled class assigned to them. This testing data set is used to compute the error rate of prediction of the class. The average error rate is calculated from all trees built.
The importance of each band or band ratio is calculated by running the out-of-sample data set down the tree, and then the number of votes for the predicted class are counted. Then the values in each variable are randomly changed and run down the tree independently. The number of votes for the tree with the changed variable is subtracted from the number of votes for the unchanged tree to yield a measure of effect. Finally, the average effect is calculated across all trees to yield the variable importance value.
Advantages of Random Forests
1. The random forests algorithm has relatively high accuracy among algorithms for classification.
2. It is less likely to overfit as you increase the number of variables (i.e. model failing to generalize from the training data).
3. It can handle hundreds (or even thousands) of variables and large data sets.
4. It yields each variable’s importance like neural nets.
5. It has a robust method for handling missing data. The most frequent value for the variable among all cases in the node is substituted for the missing value.
6. It has a built-in method for balancing unbalanced data sets (one class less frequent than other classes).
7. It runs fast! Hundreds of trees with many thousands of cases with hundreds of variables can be built in a few minutes on a personal computer.
I have added some context to remote sensing in random forest classification. I hope you had gained something. If you need clarity or require further description please let me know at gis10kwo@gmail.com.
Reference:
 Handbook of Statistical Analysis and Data Mining Applications, Chapter 11, Classification, Elsevier Inc.
http://dataaspirant.com/2017/06/26/random-forest-classifier-python-scikit-learn/
