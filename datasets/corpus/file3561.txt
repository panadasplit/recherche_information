My landscape on artificial intelligence quantitative funds and DIY funds
High frequency traders are not anymore the cool kids on the block. Margins are shrinking and major players such as Virtu and KCG are now merging to profit from economies of scale. On the other hand, artificial intelligence and quantitative strategies seem to be the new solutions to the alpha generation headache. There has also been lots of news (noise) about DIY (Do It Yourself) hedge funds (Wired, Bloomberg and also FT). More recently, Point72 announced that it handed its first check of c.$10m of the $250m promised to Quantopian.
The AI & Quantitative startup spaces are interesting and have the potential to disrupt a $3.2tn industry. In order to better understand the different actors, I tried to map and to categorise the different players in the market. Please find below a landscape mapping and thereafter the full list of companies as well as some brief comments.
PS: Views are my own. Also, I am sure I forgot many companies and did many mistakes. Do not hesitate if you have any recommendations!
Enjoy.
Feel free to share it but please quote the source
Institutional hedge funds using AI quant strategy and also source talents via challenges, academics and partnerships
Hiring is the major challenge for quantitative funds. They now compete directly with tech companies offering bean bags and other cool perks as well as high salaries. The fierce competition has forced quant funds to redesign their working environment and also to adopt new hiring strategies including partnering with crowd sourced hedge funds like Quantopian, strengthening their relationships with universities and also launching datathon in order to find the next James Simons.
Cantab Capital (UK), acquired by GAM in June 2016. The fund has a strong link with Cambridge through the Cantab Capital Institute for the Mathematics of Information hosted within the Faculty of Mathematics.
Citadel (US), launched a Datathon with a prize worth $100k for the winner.
Man Group (UK), established the Oxford-Man Institute (OMI) with “ the aim to create a stimulating environment of research and innovation where ideas flourish and practitioners from a wide spectrum of disciplines can bring together their skills to collaborate and learn from each other.”
Point72 (US), private investment vehicle of Steve Cohen. It invested in Quantopian and also announced that it will invest up to $250 million in a portfolio of algorithms managed by Quantopian.
Two Sigma (US), One of the first to offer a public Financial modeling challenge with Kaggle where quants have three months to create a predictive trading model from four gigabytes of financial data provided by Two Sigma.
WorldQuant (US), The WorldQuant Challenge provides the opportunity to compete and potentially earn a Research Consultant position at WorldQuant.
Institutional hedge funds using AI quant strategy
These funds are some of the most respected ones and use among others quantitative strategies as well as artificial intelligence. However, none of them seem to have adopted a partnership / challenge hiring strategy to attract new talents.
AQR (US), founded in 1998, AQR manages $175bn and offers a variety of quantitatively driven alternative and traditional investment vehicles to both institutional clients and financial advisors.
Bridgewater Associates (US), the world's biggest hedge fund with $150bn and at the forefront of the artificial intelligence research. The fund is very secretive on its plan and hiring strategy.
D.E Shaw (US), founded in 1988 and today manages about $40bn.
Renaissance Technologies (US), founded in 1982 the fund is one of the pioneers of quantitative trading. Renaissance’s flagship Medallion fund, which is run mostly for fund employees “famed for one of the best records in investing history, returning more than 35 percent annualized over a 20-year span.”
Winton Capital (UK), one of the most respected quant funds in Europe with around $28bn of AuM. The fund has recently launched its incubator and a venture capital arm.
Crowdsourced quant hedge funds
Everyone talks about these guys. So far, Quantopian seems to be the one leading in terms of funding, institutional backing and also Google Trends…
Quantopian (US), one of the most well funded neo-quant fund with $49m from investors including Point72, Andreessen Horowitz and Bessemer. Users can share their investment algorithms. Quantopian then selects the best algorithms and license them in exchange of a share of the return.
Numerai (US), with $7.5m from the likes of Union Square, Playfair Capital and First round, Numerai manages a long/short global equity hedge fund. It transforms and regularizes financial data into machine learning problems for its global community of data scientists. Also, one of the only neo hedge funds offering full anonymity and payment via bitcoin.
Quantiacs (US), often referred as one of the world’s first and only crowd sourced hedge fund matches freelance quants with institutional investment capital. Quants own their IP but license it to Quantiacs for 10% of lifetime profits.
Online community for quant traders
These platforms are like forums on steroids where traders can share tips, reviews and also have access to data and can do backtesting.
Backtrader (US), enables to focus on writing reusable trading strategies, indicators and analyzers instead of spending time building infrastructure.
Quantconnect (US), offers the ability to design and test strategy with its free data set and allows to deploy live directly via brokerage. Users can code in multiple programming languages and harness cluster of hundreds of servers to run backtest to analyse their strategy in Equities, FX, CFD, Options or Futures Markets. A key difference is that they have an open source infrastructure and don’t profit from strategies.
Uqer.io (CN), China’s first algorithmic trading platform. Uqer offers free and unlimited access to over 10 million financial datasets; Quants can create and backtest strategy at anytime anywhere.
Cloud9trader (UK), Cloud9Trader lets developers profit from algorithms by trading via a broker.
Pure AI quant hedge funds
These funds represent the next-gen hedge fund category. Their goal is to automatise all the investment research and trade in order to create high returns but with a much lower cost base than legacy hedge funds. Many people like Igor Tulchinsky from WorldQuant think that AI is more a tool to help human traders rather than replacing them. It will be interesting to see the returns of these funds over the next years.
Aidyia (HK), deploys advanced artificial intelligence technology to identify patterns and predict price movements in financial markets. Started trading last year in 2016.
Binatix (US), one of the first to use machine learning algorithms to spot patterns that offer an edge in investing, Recode wrote an article back in 2014.
Kimerick Technologies (US), ML and Artificial Neural Network-driven Predictive Trading.
Pit.ai (UK), a ML powered hedge fund, adopted into the YC W17 class. Techcrunch wrote an interesting article on the Company in March ‘17.
Sentient Technologies (US/HK), one of the companies with the largest war chest, $140m of funding from Access Industries, TATA Ventures, Horizon Ventures. If you want more information I would recommend this Bloomberg article.
Tickermachine (US), a low-frequency proprietary algorithmic trading firm based on behavioural economics principles.
Walnut Algorithm (FR), applies the latest advances in artificial intelligence to systematic investment strategies and for the moment focuses on liquid equity index futures in the US and Europe. For more information on the company, check out their recent interview.
Algorithmic marketplace
iQapla (ES), audits and ranks algorithms before allowing users to select different strategies and to create their automatic trading portfolio. Recently selected by Santander in their innovation lab
Pure AI quant hedge fund, open to public investors
Clone Algo (US), The Clone Algo Application runs and operates a social network. Its ecosystem allows users who are connected to brokers, banks & hedge funds, to easily clone trades from master accounts on to their own account.
Tools to design executable quant strategies without coding skills needed
Algoriz (US), lets users build trading algorithms with no coding required. For more info check out TechCrunch.
Alpaca (JP/US), raised $1m and announced in 2015 the launch of its deep-learning trading platform, Capitalico, that lets people build trading algorithms with few clicks and visually from historical chart.
Portfolio123 (US), translates an investment strategy into an algorithm.
Tool to optimize trading quant strategies
Sigopt (US), offers optimization solution for algorithm. The tool could be applied to algo trading but also to different areas. The company raised $8.7m from Andreessen Horowitz, Data Collective and Ycombinator.
Social trading platforms
Investors can easily mimic trades from other retail / semi-professional investors. Indeed, all of these traders are not using AI / quant strategies but they were one of the first to try to offer to non institutional investors an access to new strategies in few clicks.
Ayondo (UK), founded in 2008 the firm raised $10m from Luminor Capital and SevenVentures among others.
Collective2 (US), lets users easily snap together trading strategies, algorithms, and human traders to form a (virtual) customized hedge fund that can be traded in a person’s regular brokerage account.
Darwinex (UK), founded in 2012 the company is a FCA (UK) regulated vertical marketplace that pairs skilled traders with active investors. Raised about $4m.
eToro (IL), one of the most well known companies in the social / trading gamification with $73m of funding from leading investors including CommerzVentures and Spark Capital.
Instavest (US), Investors can list their trades on Instavest, including the company, share amount and rationale behind the investment. Other users can invest alongside the people willing to share their own purchases and sales. The firm is a Y combinator alumni and raised $1.7m.
Wikifolio (AT), Austria-based online platform for investment strategies of traders and asset managers. The firm raised about $7m from SpeedInvest among others.
Thank you for reading my article.
Do not hesitate if you have any comments!
Etienne
