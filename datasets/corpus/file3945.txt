Our Continuing mission….
The 60s were a heady time. The Space Age, we called it. Heavy with shows like the Jetsons and Star Trek, and theme parks like Epcot Centre. Even with the backdrop of the Cold War, we were also building an unbridled optimism. There is something of an irrepressible explorer in us as a species, and this showed in the explosion of very good science fiction.
“Then in the years that followed, Science Fiction became science fact.”
We don’t have Rosie yet, but we have the Roomba. We don’t have the tricorder yet but the first Motorola flip phone sure looked like the communicator. Inspired by Elon Musk’s Master Plan series, we thought we’d come up with our own.
Black coffee starts everything. And that’s what happened in July of last year. We had:
Following the triple H formula (Hacker, Hustler, Hipster), we still needed a hipster. But we thought we’d make up for that in the coffee that we consumed. We talked long into the night for several days, and settled on what we call “accessibly futuristic”. This turned out to be something, anything that dealt with: Artificial Intelligence, Big Data & Internet of Things.
More brainstorming. More coffee.
We looked through the projects we’d done and we saw that we did a lot of web development, eCommerce development and software enterprise applications. But, owing to a great deal of luck that we cultivated by having a bias towards the crazier ideas, we also had some happy surprises:
“Ok, that last one was a result of a bunch of our devs playing a little too much Pokemon go.”
This was a good start. We had a sun theme going so this became the Ra Products & Platforms line. Here’s a few of them
RaCom — Chatbot for Ecommerce Support & Sales
A recurring theme in sci fi is the uncanny valley, and we’ve come a long way from Eliza. (And even Cleverbot). While we’re very sanguine about the effect that robots will have on employment, we’re far more excited about how we can use a chat interface to improve online shopping through artificial intelligence applications.
“While Ra can do lots of things, his first incarnation is as:
Racom, your personal SHOPPING ASSISTANT”
We’re huge fans of shopping online. If you know exactly what you want, there’s nothing out there that compares. But what if you don’t know what you want, or you want to return something, or cancel something? Our research has shown that people who shop truly value two things: frictionless transactions and responsiveness. Our data shows that companies are spending a lot on call centres to respond to transactions that can far more seamlessly be handled by a bot.
What does it offer customers?
The biggest value add is not having to navigate multiple menu paradigms. After all, what’s more intuitive than: “Hey RaCom, I ordered a shirt yesterday, but I really meant to order a blue one, can you do that for me?”
V-Commerce for furniture
Like a lot of people last year, we played a lot of Pokemon Go. Our devs got a little too carried away, and soon we were thinking about how cool it would be to beam furniture over to where you wanted it. This was, of course, an insane idea, but we didn’t want to discount it just because of that. We settled on a v-commerce platform that lets you check out how the furniture will look in the exact room it’s going to be in. We’ll even tell you if it will fit through your door.
“See Products in Your Environment with RaAR. Fun. Fit. Foresee.”
What’ve you got RaAR?
RaAR allows customers to see their furniture in real time in their environment. You can do things like move the furniture around, change colour, material, and fabric, as well as take final pictures to share with friends to get an opinion. With TrueMatch, we’ll even make sure that it fits through your door. We built RaAR as a platform making it easy to onboard any furniture shop. We are now a full fledge Augmented Reality Development Company. Some other features include
Creation of 3D Models I Conversion of 3D to Augmented Reality I Learning about Customers through Analytics I CRM Integration I App Store Optimization for your Brand
We love the project services part of our business. It’s where we meet all the cool founders we’ve partnered with who have gone on to grow revenue-generating businesses. Our roots are in building solutions for businesses and businesses themselves. One of our happiest clients is Zarafa, for whom we created a robust & disruptive Single Page Application for effective collaboration with team or colleagues are continuing their journey with us. While we can’t share their names yet, as we don’t want to steal their thunder, A few others we’ve helped start are:
Smart Online Learning Platform
Our partner was already a leader in eLearning and education, but he came to us with those four magic words: “Let’s make it better”.
“Turns out, engagement was what we were looking to improve. Most failures of eLearning were because people stopped partway through the course.”
We got to work and we’re still at it! A sneak peek!
On-Going Engagement with Other Students & Teachers
Interaction with Teachers
Non-annoying options for Notifications & Alerts
Push Notifications to Engage Students in the Course
Skill Development for MNCs or Corporations
The platform will be launching soon and we can’t wait to tell everyone about it. We aim to contribute to the education industry through this platform and enables students to get education and all kinds of courses online.
What’s Next?
Our CEO recently gave a talk in Norway about achieving the Smart City with the help of Internet of Things & Artificial Intelligence. The turnout was incredible. Thanks for the love, Oslo. We are beyond amped.
If you’ve got other ideas around anything “accessibly futuristic”, we want to hear from you.
We’re not out exploring strange new worlds yet, we’d like to get more people on board for the coming journey. There’ll be a bit of chop, of course, but like Admiral Grace Hopper said “A ship in the harbor is safe, but this is not what ships are for.”
Originally published at marici.io on October 12, 2017.
