Found This Week #113
Photo Of The Week
Fiery Sunset, this photo is available to licence on EyeEm.
We got treated to some more fantastic sunsets courtesy of the summer weather in Swansea. Here are some more for your viewing pleasure.
In this Edition: Music, Blockchain, scaling blockchain, tuning ML models, growth handbook, introduction to AR, DIY aircraft tug, vortex ring collisions, Aquilla and thermal camouflage!
Originally published at www.foundthisweek.com on Jun 29, 2018.
Subscribe here to get Found This Week in your inbox every Friday :-)
Analog On, Andrew Leslie Hooker — NAWR ANOIS #5

For the latest NAWR ANOIS concert, we welcomed Analog On and Andrew Leslie Hooker to Swansea for some modular synths and no input mixing magic sounds. Check out the photos of the concert here.
Swansea Laptop Orchestra in Worcester

This week, The Swansea Laptop Orchestra played at the Clik Clik Collective takeover of the Weorgoran Pavillion in Worcester.
Check out the photos from the gig here.
Blockchain Explainer
Image: Reuters Graphics
Reuters graphics have put together a really nice site describing the basics of blockchain including lots of animations describing the steps involved in adding records to blocks.
Model Tuning and Bias-Variance Tradeoff
Image: R2D3
R2D3 have created a fantastic data visualisation describing how data models can be tuned to avoid over-emphases on biases of variances during the training phases.
The Intercom Growth Handbook
Image: Intercom
Intercom have released a new book called The Growth Handbook, aimed at delivering industry tested advice on how to manage customer retention, growth experiments and driving word of mouth growth.
Introduction to AR & ARCore
Image: Coursera
Google have launched a new Introduction to AR & ARCore course on Coursera. The course covers AR tools and platforms, popular use cases for AR and how to create realistic AR experiences.
Homemade Aircraft Tug
Image: Anthony DiPilato
Anthony DiPilato made a homemade iPhone remote controller electric tug for his Cessna 310. The tug can pull the 2,268kg Cessna using two 13 inch motors and two 12V power chair batteries.

Blockchain Scaling Options
Image: Hackernoon, Preethi Kasireddy
This interesting article on Hackernoon from Preethi Kasireddy describes the current limitations on scaling blockchain and describes many different solutions and methods that could be used to allow scaling, such as SegWit, Sharding, increasing block size and off chain state channels, among others.
Funny Thing Of The Week: Chair.exe

Cool Thing Of The Week: Vortex Ring Collision
Image: Youtube, SmarterEveryDay
Check out his amazing video from SmarterEveryDay of two vortex rings colliding perfectly to produce cascading concentric circles.

Facebook Cancels Aquilla
Image: Facebook
In 2014, Facebook launched a high altitiude platform station (HAPS) project called Aquilla, aimed at developing a high altitute glider drone that can transmit internet to remote areas.
In a recent blog post, Facebook confirms that they are no longer developing Aquilla but will contine to work with partners like Airbus on HAPS connectivity in general.
Thermal Camouflage
Image: American Chemical Society
Researchers at the University of Manchester have created a 3 layer material that can camoflauge thermal heat when an electric current is applied to it. It sounds much better than Arnie’s mud version!
See you next week!

P.S. If you like this Found This Week, please click the clap icon below.
Also, subscribe, or tell a friend, or both! Thanks :-)
Originally published at www.foundthisweek.com on Jun 29, 2018.
About Found This Week
Found This Week is a curated blog of interesting posts, articles, links and stories in the world of technology, science and life in general.
Each edition is curated by Daryl Feehely every Friday and highlights cool stuff found each week. The first 104 editions were published on Mediumbefore this site was created, check out the archive here.
Daryl Feehely
I’m a web consultant, contract web developer, technical project manager & photographer originally from Cork, now based in Swansea. I offer my clients strategy, planning & technical delivery services, remotely & in person. I also offer freelance CTO services to companies in need of technical bootstrapping or reinvention. If you think I can help you in your business, check out my details on http://darylfeehely.com
