Paper Review for Affective Computing (1)
Our team at orbis.ai is dedicated to creating affective computing engines that are capable of understanding and expressing human emotions. Every two weeks, we internally dedicate a part of our day to reviewing important papers for affective computing. We decided to share some of what we review to the world, since we believe more people should know the importance of emotion in HCI and in creating artificial emotional intelligence (hilariously portrayed by HBO’s Silicon Valley using Sophia)

Paper 1: HIDDEN MARKOV MODEL-BASED SPEECH EMOTION RECOGNITION
https://www.researchgate.net/publication/224929735_Hidden_Markov_Model-based_Speech_Emotion_Recognition
Why this paper is important: Many recent efforts for emotion recognition utilize deep learning, usually using convolutional architectures. Albeit some good successes, such approaches have limitations in explaining the recognition results. This paper brings us back to 2003, where they use good old HMM and hand-picked features for classifications, and show some great results.
Key takeaways:
This paper introduces two HMM based methods of recognizing emotion from speech.
single state HMM for global statistics for each emotion
continuous HMM for classification using instantaneous low-level features
The work uses a total of 20 hand selected features for global statistics classification, including pitch and energy related features. 86.8% accuracy for 6 class emotion classification on in-house collected dataset is reported.
For continuous HMM, they use instantaneous pitch and energy, and first and second derivatives of each, totaling 6 features. Surprisingly, they report a lower accuracy of 79.8%.
Paper 2: Deep Learning for Emotion Recognition on Small Datasets Using Transfer Learning
https://dl.acm.org/citation.cfm?id=2830593
Why this paper is important: Datasets available for training emotion recognition models are small in size, and most often imbalanced. This work aims to compensate for these problems by transfer learning from pre-trained ImageNet architectures. They end with an anticlimactic but important conclusion.
Key takeaways:
Without transfer learning, AlexNet and VGG architectures perform better than baseline (32%) on EmotiW dataset. Any further transfer learning led to marginal improvements in performance. The authors conclude that since EmotiW and Fer32 was much smaller in size than ImageNet dataset, the improvement was marginal.
Data imbalance problem also led to lower classification accuracies on labels with smaller sample numbers.
All in all, transfer learning or not, a dedicated emotion recognition dataset that’s large enough and balanced is important (but then, when is it not?).
Well that’s it for today, stay tuned for next paper review.
Check us out at https://www.orbisai.co
