How to survive automatization (AI) and boost your career

Artificial intelligence and machine learning algorithms have been with us for quite some time in everyday applications that we use, for example, the recommendations Facebook uses for the publications, Uber or Waze algorithms when driving, these are just some examples of technology that we already have used advances in artificial intelligence and machine learning for some time.
Let’s take into account that every time an “industrial revolution” begins, after some jobs that disappear, new ones also appear. As I will explain in the following article, we must prepare ourselves to develop our human critical thinking skills and exploit all those qualities that prepare us for the jobs that are coming.
What the AI represents to the world of work
About 47% of the U.S workforce is in jobs at high risk for becoming automated within the next two decades, according to the 2013 Oxford University study.
Customer care is in the midst of mass disruption. Technologies are advancing at record speed, driving customer expectations higher than ever. With the integration of AI with cognitive capabilities, they offer a much cheaper alternative from the point of view of companies to reduce costs. IBM is one of the pioneer companies in developing this type of disruptive technologies on the customer service industry as this 2018 trends article explain.
This represents a big alert for countries like mine (Guatemala) where a large part of the young generation belongs to the customer service industry, where they generate more than 35,000 jobs, according to the Guatemalan economy minister.
This means that many of the jobs that represent performing repetitive tasks will become automated. This will initially generate a certain degree of inequality for people whose jobs are affected.
Prepare yourself
Always remember that this in the first instance means more than the dissolution of complete works only those tasks that can be automated. To prepare for this future, investigate AI tools in your own field. Learn how to use them and exploit them to increase your own productivity.
Learn how to think, prepare for jobs don’t exist yet
Let us also remember that, like other international revolutions, they have served to create new jobs. In fact 65 percent of today’s schoolchildren will eventually be employed in jobs that have yet to be created, according to this U.S. Department of Labor report. That also means that many currently employed workers for the first time since the industrial revolution must be thinking about what they will do to make a living 10 to 20 years from now. So let’s exploit our creativity, curiosity and beings aware of consciousness to work hand in hand with these new tools.
According to the World Economic Forum these are the second of the skills to be reinforced for future positions.
Image: World Economic Forum
Explore as a Freelance
Today, more than 57 million workers — about 36% of the US workforce — freelances. Based on current workforce growth rates found in Freelancing in America: 2017, the majority of the US workforce will freelance by 2027. The youngest workforce generation is leading the way, with almost half of millennials freelancing already.
So a good way to be prepared is to begin to familiarize yourself with the freelance work mode and the tools. Such as UpWork , Freelancer or Fiverr.

Remember our responsibility as technology creators
This fourth industrial revolution has to be created always thinking of teamwork so that the AI serves as a tool for productivity and empowerment and not as a substitute. Our work as developers of technology must always be to put human needs and that the machines are to make us more useful and productive.
‘We use the term augmented intelligence [rather than artificial intelligence],’ Paul Ryan, head of Watson Artificial Intelligence, IBM UK, tells Metro.co.uk at the AI Summit.
‘It’s all about helping humans do their jobs better, it’s man plus machine being greater than man or machine.’
