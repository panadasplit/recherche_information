Day 15 — Gradient Boosting Model (GBM)
今日主題: Gradient Boosting Model
有一些中文翻譯直翻提升方法，大概跟估狗翻譯小姐的翻譯功力差不多。但我沒好到哪去，根本不知道怎麼翻，就用英文原文吧。
參考資料
Friedman, Jerome H. “Greedy function approximation: a gradient boosting machine.” Annals of statistics (2001): 1189–1232.
Prince Grover
Wikipedia
Towards Data Science
itw01
Complete Guide to Parameter Tuning in Gradient Boosting (GBM) in Python
這次的主題相對中文尤其正體中文的資料少了很多。大概能找到寫得比較好又是專門針對GBM的就是這些了。
[1]就不用多提，GBM的創始論文，5525次引用。
[2]講了許多關於Gradient Boosting的基礎概念。並不專講GBM但是把數學理論簡單介紹了一下。
[3]連中文翻譯頁面都沒有，大概還真的是沒人去翻譯吧?
[4]有點像是[1]的講解，看不懂或沒時間看[1]的話就看這個就夠了。
[5]是少數找到完整的中文資料，果然用母語學東西快得多。如果不想看其他的引用資料其實光看[5]就足夠懂個大概。
[6]偏向python實作的文章。裡面把各個要調整的參數都列了出來，實戰上調整參數的重要性就不用我多說，透過這篇文章去了解需要調整哪些參數是很有幫助的資源。
心得
GBM的核心觀念是為輸入資料建立M種不同的分類/迴歸模型。這M種模型都是比較簡單的弱分類器(Weak Learner)。每次分類都將上一次分類錯誤的資料提高權重再分類一次。最後得到的M個模型就會有M種權重，最後的分類結論就是將這M個模型按照權重加總起來就是答案了。
數學公式可以下圖來解說

參考[5]，對每個模型m，都對前m-1個模型求梯度，得到最快下降的方向，就是這第m個模型的梯度。
演算法流程如下

分支之一: Least-squares regression

分支之二: LAD TreeBoost

分支之三: M TreeBoost

原文還有其他分支以及評估方法，就得回去查了。
Code:
參考[6]的範例
