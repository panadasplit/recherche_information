Data science self-study learning path
Data science/Machine learning is the hot trending topics of today and everyone wants to get on the ride. But Data science can be daunting to beginners as the field is relatively new and really vast. Especially for the ones looking to change the domain of their job by following the self study path. Anyone who is on the self study path soon finds himself/herself in an ocean of resources and it is very easy to get lost. This is my attempt at summarizing self learning path which I followed to transition from software engineer to a data scientist.
Data science is a blanket term used for multiple technologies and paradigms. Process of learning data science can be broken down into 3 chunks: Theory, Tools and techniques. All these three are interconnected, related and are essential. In my personal experience many people start the journey with absolute theory(mostly Andrew NG’s course) and fall out of it in between as theory/math becomes complex or many just doesn’t appreciate the need of it.
Why can’t I just use scikit library and call a method to classify? Why should I learn so much theory?
This is the question for most of software engineers who are used to leverage libraries or ‘one line’ code to achieve everything. Well, I was one of them.
Generally learning starts with theory but considering huge and complicated theory space of data science domain, I propose bottom up approach as theory can be daunting at first and we cannot appreciate the necessity of its understanding before understanding data science space in general. This has personally worked for me hoping it could help someone else as well. Let me know what you think in the comments!
The learning path
Understand the data science domain and its capabilities(Tools) -> Get hands dirty on a real data science problem(Techniques) -> Deep dive (Theory+ Revisting techniques + Revisiting tools)
Phase 1: Understand data science domain and its capabilities
· 2–5 hours — free: Tutorial competitions on Kaggle.com like Predicting survival of Titanic passengers
· 50–60 hours — free: Udacity’s Intro to machine learning course by Sebastian Thrun and Katie which is free and part of bigger nano degree program
Phase 2: Get hands dirty on a real data science problem
Fail early, fail fast
· 20–30 hours — free: Attempt a real-world competition with real data on Kaggle.com which is an excellent community of data science enthusiasts and experts.
· At this stage, we will know the capabilities of data science and would have got a flair for it. Also, we would be in the state to appreciate the need of theory understanding
Phase 3: Deep dive— Free and paid versions available for each of these
· Statistics: https://www.udacity.com/course/intro-to-inferential-statistics--ud201 and https://www.udacity.com/course/intro-to-descriptive-statistics--ud827
· Linear algebra: https://www.youtube.com/playlist?list=PLE7DDD91010BC51F8
· Machine learning: https://www.coursera.org/learn/machine-learning
· Visualization: https://www.udacity.com/course/data-visualization-and-d3js--ud507
