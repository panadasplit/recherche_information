Photo by Scott Warman on Unsplash
Talking AI in a bar
I am currently doing research in artificial intelligence in a company downtown Montreal. Whenever I go for drinks with my friends, I often find myself having to explain what I do and it can easily get over complicated. It’s even harder to explain my tasks when people don’t know what Artificial Intelligence is. That’s how I’d break it down.
First things first
My goal is to solve problems, issues that are well defined and understood. I have to solve tasks that a humans could do but needs to be automated.
The problems that I solve always follow the same pattern. We have an input, and an output. The output is the result, the solved task. Just like humans, machines can learn. Just like humans, machines use example to understand and recognize patterns in an input/output pair.
The Process
A machines needs a tons of example to understand patterns. We are talking thousands and millions and evens billions of examples in some cases. Imagine that you need to go to the bathroom. You’d be able to identify which of the two doors is for the men’s restroom and which is for women’s. But why? There are millions of ways to identify them and yet, we know which one to use.
Photo by Juan Marin on Unsplash
How it works
To have a clear idea of how we can make a machine learn things, we need to understand what is going on between the input and the output. A classic analogy is the black box. An input enters a box and an output is retrieved. So what is going on inside the box? Since computers work with numbers, so must the black box.
There are 3 steps to learning: try, measure, learn. The box is first applying transformations to the input until we get an output. That’s our first try. Then we compute difference between the output produced by the box and the true output. We now have a number that measures how far the black box is to produce an output accurately. Using this metric, we can teach our model how to get closer the next time.
Boring black box
The box here acts like a function. Some functions can be as simple as those from high school [ y = ax + b ], other are way more complicated. The parameters here (‘a’ and ‘b’) need to be learned. The metric expressing the difference between the true output and the predicted one is a guide that points towards the perfect ‘a’ and ‘b’. You might already know that there are infinitely many different type of functions and that some are better at representing a problem than others. In other words, some black boxes perform better than others; it depends on the tasks to solve.
Photo by chuttersnap on Unsplash
It goes without saying that there is an infinite number of possible parameters and this is why we cant brute force our way towards the right ones. It is critical to use the measurement metric previously described.
What I actually do
My job is to find the perfect box for a task. Find the architecture of the box that will allow me to solve the problem. From the format of the input to the transformation applied to it, going through the best metric to measure the correctness of the black box. That’s what I do. I design black boxes. I’m a box architect.

Hey! That was my very first post on Medium. I’m open to any constructive comments, questions, insults. Let me know what you think.
