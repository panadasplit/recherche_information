Lydia — A Tour Guide based on Image Recognition
Introduction
With the improvements of AI and Machine learning, one particular area that has benefited greatly is Image Recognition. Today’s Image Recognition algorithm can not only identify objects from a 2D Image, but they can also correctly classify them with great accuracy.
The Error rate for the latest ImageNet Challenge was ~3%. The Error rate for human on the same data set around 4% mark.
This same Image Recognition technology can be used to enhance travel experiences a lot of travellers. Giving each one of them a travel guide of their own.
Market Analysis
Tourist Arrivals Prediction — UNWTO
The number of tourists arriving in a destination has always been increasing. It is expected to be around 1.8 Billion by 2030.
More and more people are looking for an “Local Experience” — This is evident by the rise of services like AirBnB and HomeAway.
Also, according to Bookings.com, 33% travellers prefer to stay in a holiday rental and 12% would prefer if they did not have to meet the owner at all.
There is a growing sense of self reliance. This presents a unique product proposition in the travel domain.
We see a growing trend where tourists want to discover locations and not learn about them from tour guides.
We see a growing trend where tourists want to discover locations and not learn about them from tour guides.
The Product
The Product is essentially a mobile application with which the tourist clicks photos of the famous (or not so famous) landmark, the app recognises the landmark using Image Recognition.
The application can then give the tourist factual information about the landmark or it can also lead them on to a more discovery-based experience.
Image Recognition by Google Vision API — It has Recognised The Taj Mahal
Factual Information of the Landmark
Building a mobile app that provides factual information is a big improvement on today’s traditional tourist discovery methods (destination pamphlets).
But factual information does not necessary provide what a tourist guide does, a Story or a Experience to remember.
Discovery Experiences
When we build mobile apps that have the ability to provide various experience to tourist, we ensure that the customer is always in command of his experience.
Sample Discovery Experience Screen
In the sample that I have provided. After the tourist has taken the photo of the Taj, the mobile application recognises that the landmark was “Taj Mahal” and loads the experience screen.
The tourist can then decide which experience is more interesting and the mobile app can then guide them to various spots and narrate (AR maybe ?) various experiences.
This guided tour will continue till he has covered that particular experience for the Landmark.
Sample Experience Route Around the Tag
Monetisation
The Mobile App essentially has to be free. Once we reach critical mass of customers using this app. A marketplace can be developed where local suppliers can be asked to advertise their hotel rooms, restaurants, activities etc.
The idea is to provide only those Ads or offers that can help our customers get the local discovery experience.
Keys to Success
This product will have a few things required to make it successful.
The Mobile App has to be free with minimal screen space given to Ads. Specially during an discovery experience. Each experience can be followed by a Ad or an offer we determine that the customer might enjoy.
We should also start from Europe — It has a high density of historical monuments and about 51% people travel to Europe.
We want to ensure that people are able to enjoy their trips and have unparalleled experiences.
Key Opportunities
The following are the Key Opportunities that will face the product —
We have the ability to define experiences around various famous landmarks.
We can integrate with various Online Travel Providers to provide discovery-experiences to their customers.
We can partner with various government agencies and help promote regions and drive their tourism based economies
Key Threats
Major threats will be from Tourist guide unions and online travel agents.
Apart from that there will be pressure to start converting our bottom line green as soon as possible — Meaning pressure on conversion of Ads. This might start influencing with out discovery experience.
Conclusion
This is just one way how Image Recognition can be bought to the market in form of a product that can help people.
The objective here is to help people travel better and discover better experiences.
Hope you enjoyed the articles, please share your thoughts and ideas in the comments section. If you would like to cover a topic please email me at me@yasarsiddiqui.com
