On the Importance of Subjective and Objective Measures

Subjective metrics (e.g. happiness) alone aren’t very consistent or reliable data points. Objective metrics (e.g. distance run) are great for statistical analysis but don’t yet exist for many of the things we care about. To get around that, we use markers or proxies of the things we care about. I argue that a combination of subjective and objective metrics is a powerful tool in gaining insights.
Here’s an example:
The latest version of my personal tracking framework has around 40 data points for both Subjective Productivity Rating (SPR) and for Deep Work hours.
SPR is a measure of how productive my day was with reference to the things most important to my life and career, on an integer scale of 1–5.
Deep Work hours are a related objective metric, simply measured by how many hours I logged in my retrospective calendar as being spent in a state of Deep Work. This is really only a proxy for how productive I’ve been, as my hours in this focused state are not always equally intense, so an hour of Deep Work is of varying impact.
On their own, neither is a very good indicator of how productive I’ve really been. SPR is subject to bias, flawed perceptions and the inherent noisiness of quantifying a psychological experience, whilst Deep Work hours are only a reference point for how productive I was. Both are great things to track individually, but together they allow me to get a sense of:
How productive I perceive my time spent working to be.
If/when more time in deep work starts having diminishing returns.
How closely each may be tethered to my actual productivity.
Looking at the data
SPR vs Deep Work (h) for 40 days
As you can see (from the limited data I’ve gathered in this new framework), hours spent in Deep Work on any given day (X-axis) and SPR for a given day (Y-axis) only correlate with R² ≈ 0.41 on a linear fit. This isn’t very high, but visual inspection of the graph reveals more insights.
Days of fewer than 2 hours in deep work have SPR values ranging from 2–5 in a clustering pattern. Then there is a huge gap in the data from 2 to 4 hours. I clearly get into a flow state after 2 hours of work and then just keep going for 4+ hours. On the upper end, when I’ve spent 4+ hours in a Deep Work state, my subjective experience is always one of high productivity – most values are 5/5.
Takeaways
Spending more time in a state of Deep Work makes me perceive myself as having been more productive. It also makes my perceived productivity more consistent. This could be due to the fact that I’m actually more productive, or merely a subjective experience of being more productivity.
I tend to work deeply for less than 2 hours or more than 4 hours, but seldom in between. This is likely due to entering the flow state.
The expected positive relationship between SPR and Deep Work hours is seen and of reasonably-high correlation given the noise levels of subjective data measures.
Ideally, I need some concrete measure of my productivity – e.g. lines of code committed, words published on Medium, assignments completed, etc. to compare with both these measures to validate how accurate my perception is and how effective my deep work truly is. This is something I’ll probably look into when I’ve got more data.
Self-tracking is as much of an art as a science at this point. I hope some of the ideas presented here inspire you to track and quantify yourselves in new and helpful ways.
