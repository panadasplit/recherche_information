Day 2/28: My Journey to understanding Artificial Intelligence
Is AI coming for your job? Dialogue between Yann LeCun and Matissa Hollister, Oct 11th 2017, Montréal
I just finished a book and the last words were:
“There are three types of people: those who make things happen, those who watch, those who have no idea what happened. The choice is yours. Never too late to catch the train”.
They’ve stuck with me ever since.
It’s my second day writing this journal for a class at McGill University. We are asked to pick a topic and explore its implications in retrospect to societal themes discussed in class. I’ve picked the topic of Artificial Intelligence, and every day, for 28 days, I will be posting my thoughts from books, articles, conversations with experts, comments, etc..
And so it starts…I’ve just caught the train and I am on a journey to be apart of ‘those who watch’. My starting point is:
Artificial Intelligence is bound to disrupt the workplace. To ensure positive societal development a system of ethics must be developed. AI, understood by the few, will increase inequalities.
What is Artificial Intelligence ?
I understand AI as being a process by which a machine is capable to imitate intelligent human behaviours.
Why all the hype around Artificial Intelligence?
I study in Montréal and the city is becoming a hub for AI. I’ve subsequently developed an interest in the field. Attending sold out AI events, like the Google Brain talk or Yann LeCun’s intervention, has made it apparent that I am not the only one curious of its developments. And rightly so, considering the speed of its development!
So, What’s the big deal?
Its applications concern all of us; AI is applicable in every industry from science, to sports, to music, to finance, and the list goes on. AI is said to be the biggest change since the ‘Industrial Revolution’, where step-by-step it will transform industries for the better or for the worse. It is for us to decide.
Will AI take over humans ? Meh… Singularity (the point at which AI will surpass human intelligence) is far far away. But not something to ignore, Masayoshi Son, founder & CEO of Softbank, recently announced a $100 billion fund to support his 30 year vision plan. He believes in 30 years time, singularity will be achieved.
Will AI takeover our jobs ? Yes, many. AI is increasingly taking translator jobs, as its applications are getting better and better. If you compare google translate today to what it was 3 years ago; whilst there still is some work to do, it is much better today. AI will be taking over the job of drivers with autonomous cars — when? remains a question of essence — especially after the first crash in Vegas on the first day of launch. Thank god, no one was injured!
Its late gotta catch some sleep...zzzz See you tomorrow morning!
My last review of AI was quite pessimistic, I’ll reverse things around tomorrow, and try speak of it positively.
Cheerio :)
