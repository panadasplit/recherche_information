Insights: Empathy and Behavior with AI
Shout Outs! In addition to countless others that gave us feedback in writing, I’d like to offer a special thank you to Jim Dries, Peter Kim, Janine Williams, Steve Baxter, Ilias Beshimov, Keith Tamboer, and Sarah Hague. We all really appreciate the fact that you took time out of your busy schedules to share some insights over the phone!
The topics for the last couple of weeks have centered around how A.I. can help give us context, guide us, nudge us in the right direction, and predict events based on behavior.
So, without further ado, let’s share a fictional story of how this would look in a land not so far away :-).
The Companies
There are two companies:
OldSchool, Inc. OldSchool has the latest Martech and CRM toys. But, they rely on traditional sales and marketing processes to fill the funnel and close deals.
NextGen, Inc. NextGen also has the latest Martech and CRM toys, but they have augmented these solutions with machine learning and deep learning services that predict actions based on user behaviors.
Old School Marketing and Sales Process
https://shirtoid.com/
OldSchool automates everything with the latest and greatest Martech and CRM stacks. Many of OldSchool’s processes are automated. For example, a user clicks on an Adwords campaign and that kicks off a sequence of emails with a specific conversion goal. OldSchool even has a marketer dedicated full time to A/B testing to optimize their processes!
However, the contact’s behavior or context outside of the inbound marketing channel campaigns aren’t tied to the propensity for conversion. In OldSchool’s case, clicking on a Blog link may help fill the funnel and help provide a nice lead score, but they don’t help guide OldSchool’s sales team with empathy or context. Said another way, optimizing what content works best for conversion must be A/B tested (a manual process) and once the user has engaged with one of OldSchool’s representatives there is limited context during the engagement process, except for ‘John Doe downloaded a white paper.’
NextGen Marketing and Sales Process

Like OldSchool, NextGen has the latest and greatest Martech and CRM toys. And, also like OldSchool, many of their actions are automated and A/B tested to improve performance.
However, there is a fundamental difference between NextGen and OldSchool. NextGen considers intent from a variety of sources to measure a customer’s propensity to meet a certain goal (such as a conversion). NextGen also creates interfaces that have empathy built in.
In other words, NextGen doesn’t just measure intent based on interactions with their own content, but measures intent by tracking the user’s behavior in channels that have nothing to do with their inbound marketing campaigns and content.
Twitter sentiment? Check
LinkeIn job change? Check
News site comment? Check
Participation in a trade show? Check
(…) Check
This gives sales pros a lot of power because they can engage the potential customer with leading questions! It also allows firms to predict future behavior based on how they may answer some simple questions. So the lack of data in B2B environments may not be the end of us!
For example compare:
Hey John Doe, we noticed that you downloaded the <Fill in white paper title here>. We have a discount for the quarter so we encourage you to <click on some link> and buy our stuff.
vs.
Hey Jane Smith, first of all, congrats on your new job promotion! It looks like quite a change from your previous job at Acme, Inc., but there’s nothing like a new experience to keep us on our toes! Feel free to reach out to me directly if you need any clarifications, we would love to help you achieve <fill in goal here>.
I bet NextGen will be more successful than OldSchool, all else being equal!
