PROOF OF EXISTENCE FOR DIGITAL ASSETS

BigDataGuys — Blockchain Use Case on creating Proof of existence for digital assets.
PROOF OF EXISTENCE FOR DIGITAL ASSETS — Blockchain | BigDataGuys
The proof of the existence of anything can be hashed. So, anything with a single digital representation can be hashed and stored in the blockchain. Indeed, any user (node) can query whether the element was hashed and added to the blockchain.
Furthermore, there are many more examples of things that can be implemented with Ethereum blockchain platform.
A Simple Login System using Ethereum
In Ethereum, the addresses are (by definition) systems to prove ownership. The rightful owner of the address is who can perform operations with this address. So, this is the consequence of the underlying public-key infrastructure that is used to verify transactions. For example, we can create a login system based on Ethereum addresses.
In a login system, it is always created a unique identity that can be managed by whoever can pass a method to prove that the same entity that created the account in the first place is the same entity doing operations now. Actually, most systems have a username and a password. So, anytime the system requires proof of its identity, then it can request the password for that username. But, in Ethereum we already have a system for proving identities, which are the public and private keys.
For example, a simple contract (which can be used by any user to validate his ownership of an address) would have the next logic:
First, a user accesses a website to login. Then, when the user is not logged in, then the website requests the user to enter his digital address.
Then, the backend for the website receives the address for the user and creates a “challenge string” (to verify his identity) and a JSON Web Token (JWT). Then, these are sent back to the user.
Then, the user sends the “challenge string” (used to verify his identity) to the “Login contract” and stores the JSON Web Token (JWT) for later use locally.
Then, the backend listens for login attempts using the “challenge string” (used to verify the user’s identity) at the Ethereum network. When an attempt with the “challenge string” for the right user is seen, then it can assume that the user has proven its identity. Furthermore, the only user that can send a message with a digital address is the holder of the private key, and the only user that knows the “challenge string” is the user that received the challenge through the login website.
Furthermore, the user gets notified or polls the website backend for confirmation of his or her successful login. Then, the user must use the JSON Web Token (JWT) issued in step 2 for accessing the website. Or, a new JSON Web Token (JWT) can be issued after a successful login.
Want to learn more on how to write Ethereum smart contract to validate his ownership of an address? Consult us or get trained through one of our Blockchain Bootcamps at BigDataGuys
Main Office: 1250 Connecticut Ave NW, Washington, D.C 20036
Phone:
202–897–1944 |
202–897–1966
For Training — Training@Bigdataguys.com For Consulting- Info@Bigdataguys.com
Twitter https://twitter.com/BigDataGuys
Linkedin https://www.linkedin.com/company/13190381/
About Me https://about.me/bigdataguys
www.bigdataguys.com
