Got any Crypto Mate?
Someone asked me recently if I was investing in ‘Crypto’. As it turns out I was, albeit indirectly. It’s what you might call a ‘picks and shovels’ play on the new age ‘mining’ industry.
This is probably considered cheating, but have a think about NVidia. They are a leader in the field of computer processors. Crypto currencies and blockchain generally rely on distributed computing power. ‘Mining Farms’ are large banks of privately owned computer processors competing to ‘mine’ or process blockchain activity around the world. When I say compete … check out this video to get a sense of the scale of it.

… and if the whole crypto craze comes crashing down, there are much broader trends to keep NVidia going … like AI, machine learning, deep learning, computer gaming, CGI in movies, virtual reality, drones, self driving cars, internet of things, health sciences, whatever other sciences and cloud or distributed computing generally.
