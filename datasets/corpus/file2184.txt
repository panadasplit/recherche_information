Automating Life
I am going to automate or remove any manual task in my life that doesn’t provide me value by doing it manually.
“All jobs will be automated in the next 50 years…” OK (add sarcastic tone)
There is a future of autonomous self-driving, crypto cashless payments, predicted shopping orders, off the grid power with a quick trip to Mars for a weekend break. We are experiencing parts of this today in varying degrees of complexity and reward, but the truth is no one knows which way the future will turn out (that is the cool thing). By 80s predictions, we should all be in silver suits and zooming around space like the Jetsons.

The one thing that has remained constant in history is human desire to automate, driven by greater goals or simply increased profits. Either way, automation is a continuing force in our lives, spanning generations, no one is sitting with an abacus at his or her desk, and the office calculator is a thing for only a few. As humans, we move with the times.

The technology revolution has begun, and though we believe it to be in full swing, personally I feel it has not even started. Automation of once manual tasks will accelerate with exponential growth over the coming decades, and though will seem slow on a macro basis, we will see a rapid change in the major of job roles in our lifetime.
So why try and automate in your life?
Three factors (Now, Near, Future)
Now — Life is busy (I do cause this though), there are many things I prefer to do and believe I can gain time to do them now by automating those lack value or add negativity.
Near — Doing many tasks you dislike isn’t good for you mentally or physically, I want to protect myself in the near term from mental and physical fatigue.
Future — Automation is happening, I want to get mentally prepared for the future, both through learning and enjoying the extremes of free-time we may live to enjoy.
This series is not a “How to automate your life in 3 months” series, but a mindset shift for the next 30–40 years. I do aim to automate the least valuable and mundane tasks of my life in the next 6–12 months.
So what is the point…
There are 8,760 hours in a year (not that many when you read it like that), and I want to get value from as many of them as possible. Sharing between work, health, fun, family, self, and activities. That is 168 hours per week. Currently, I need a solid 7.5 hours of sleep a night, so that leaves 115.5 hours per week (16.5 hours per day) to optimise.
Below are some of the areas I am focusing on first, this shift is not about cutting a journey time from 20 minutes to 15 minutes but looking to see if that journey does not provide value or is negative to creating value. Such as changing the timing of that journey, so I am not in stop and go traffic which causes frustrating and not adding any value to my week.

Key areas to start with: fitness, travel (commutes/business trips and holidays), household chores, sleep/rest, paperwork, food shopping, family coordination, consuming content, business meetings, family time and writing.
Why document? I have always been a fan of automation in business and personally and want to share and collaborating with others as feel this will be something we all ask ourselves at some point.
“Are the things I am doing providing me value.”
If your thinking about giving it a try, just do it and please let me know how you got on. you can email me via leemallon.com if I can help in any away.
Subscribe for updates on how I am automating my life via www.leemallon.com
