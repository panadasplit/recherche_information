Quick Guide to CoreML
Well, I know most of you already heard about CoreML, but for you who still have no idea about it, this is a really quick introduction about it with a practical example.
CoreML
CoreML is new machine learning framework released by Apple which enables you to do inference inside your iOS, macOS, tvOS or watchOS apps.
But what is the inference?
In fact, the idea of this publication came up from an app that I were working on, which it has a camera feature where it needs to detect if the cam is seeing a cat or a dog as well. So I needed to use machine learning in order to solve it. The camera looks like that.
Camera
So, machine learning has two steps, first one is training which you can’t do using coreml, probably you will find out around a lot of machine learning tools like Caffe, Keras, turi, and the second step is inference.
So for my case, I will need to pass a pet image through pre-trained model and than to get what kind of pet actually is. This process is called inference and looks like that:
Inference, Machine Learning
Basically, this is followed by a layer approach, on the top is your app. There also are released two new frameworks: Vision and NLP.
The Vision framework will allow you to do all the tasks related to computer vision. But what’s the computer vision?
As humans use their eyes and their brains to understand and get visually sense what is around them, also computer vision is science that aims to give similar capability to a machine. So using Vision we can do things like face detection, object tracking etc. There is also NLP Api to include more languages and more functionality. If these are not enough there is CoreML or why not combining all of these. At the end, these are on top of Accelerate and MPS (Metal Performance Shaders) frameworks which will give high performance of the app.
Layered approach
With CoreML you can integrate machine learning into your apps, you don’t need to have internet connection to do prediction and it supports many machine learning models like neural networks, tree ensembles, support vector machines, and generalized linear models. So as it runs locally, there are some advantages. First, user’s privacy. Your users will be happy to know that you’re not sending their images, personal messages to a server. Also they will not need to pay extra data cost just for that prediction. You will not need to pay server as well and will be always available for you.
But, where do models come from?
To have a quick start you can use pre-trained models like Inception V3, ResNet50, Places205-GoogLeNet etc. that are already on .mlmodel format, which is the format that the trained model should be in order to use it in XC0de with coreml.
Machine learning is out for a while now, that means that there is a huge community around, also there are a lot model that are not in .mlmodel. But for our help there is Core ML tool written in Python, it is open source and with that you can convert models from other ml tools like Caffe, Keras, XGBoost etc. to a .mlmodel. The bad news is that there is no direct support for tensorflow, but no problem you can create your own converter or probably you will find something on Github 😉.

In my example I will use Inception V3 pre-trained model which you can download from here: https://developer.apple.com/machine-learning/ .
Once you have downloaded the model, drag and embed it into your iOS project and it is ready to use. There is Swift or Objective-C api so you can use it as any other model. Usually when you create a new project there is a basic UIViewController, so you can use it. After you have the Inception V3 model into your project, try to import CoreML and initialize it like that:
Once, you have done that you will need to take or choose a picture, convert to CVPixelBuffer and should be able to pass through Inception model for prediction. The code perhaps will look something like that.
Converting image to CVPixelBuffer
By the way, don’t worry I have created a github repository with some examples about machine learning in iOS. You can download and explore from here CoreML-Demo.
Finally
Thank you for reading. If you have any question feel free to write me on Linkedin or follow me here on Medium.
