Alien Attraction
Credit: Pixabay
While ghost hunting is our favorite form of paranormal investigation, we also enjoy a good alien conspiracy. We recently watched an episode of Ghost Adventures which felt like a crossover with Ancient Aliens, as the Ghost Adventure crew investigated a UFO sighting hotspot.
In the episode, titled Stardust Ranch, one of the UFO witnesses said that he believed one reason for the frequency of UFO sightings was the proximity to a nuclear power plant. UFOs, the witness posited, must either collect radioactive energy from nuclear plants, or have a mission to monitor humankind’s use of nuclear power.
That got us thinking: wait, what? Is that true? Maybe UFO sighting do happen more frequently around nuclear power plants?
It’s worth noting that this same witness claimed to have killed eighteen aliens with his katana, so he’s definitely an expert.
Anyway, we decided to answer the question:
Do people actually report UFO sightings more frequently around nuclear power plants?
To investigate this, the first thing we needed was a database of UFO sightings. Luckily the National UFO Reporting Center (NUFORC) maintains an awesome database, including dates, locations, and detailed descriptions of UFOs. There are about ~100k sightings available, but we had trouble downloading them all in bulk. Instead, we focused on 23,665 sightings where the UFOs were classified as “lights”.
Unfortunately, the sighting locations are listed as a city and state, so we needed to convert this information into a more quantitative location. You’d think we could just use the cities and an API to look up latitudes and longitudes…but, apparently, most free services online can’t handle 20k requests.
Instead, we used zipcode, a Python module which lists zip codes for every US town. Finally, we used this table available on GitHub to convert our zip codes to latitudes and longitudes.
Blue: UFO Sightings in the continental US. Image generated with Python using BaseMap
We also needed a database of nuclear reactor locations, which we got from the US Energy Information Administration. Again, we converted zip codes to latitudes and longitudes. This data contained the locations of 22 currently operational nuclear reactors.
Blue: UFO Sightings in the continental US; Red: Nuclear power plant locations. Image generated with Python using BaseMap
Hey! These do look kinda correlated by eye. However, you can also see that UFO sightings seem to trace major cities, so we suspected that both nuclear reactors and UFO sightings just follow population density. To explore this possibly confounding variable, we found a database of US population density from data.world in the form of census population data by zip code.
Blue: UFO Sightings in the continental US; Red: Nuclear power plant locations; White: Population density. Image generated with Python using BaseMap
Yeah, they kinda all look correlated. Let’s do some ~~statistics~~ to make sure.
To take a closer look at this we wanted to answer the question: are there more UFO sightings per person in the area around a nuclear reactor than around a random location with a similar population density?
We randomly selected a large set of locations and used rejection sampling to recreate the same distribution of population density as seen with the nuclear reactor sites. The metric we used to measure population density was summing the population of people within a 10 km radius of a given location.

After we had our set of locations, we normalized the population densities with the number of UFO sightings within the same 10 km radius.
We then average the UFO sightings per person for all of the nuclear reactor sites and compared that to the UFO sightings per person for all of the control sites.
We found:
Average sighting concentration within 10 km of nuclear reactors: 
5± 6 sightings per 1 million people.
Average sighting concentration within 10 km of control sites: 
5± 6 sightings per 1 million people.
In other words, they’re literally the same.
There is no (interesting) correlation between nuclear power plant sites and UFO sightings.
However, it’s true that nuclear power plants and UFO sightings appear correlated at first glance, if you do not account for population density. This is one example of why it’s so important to account for confounding variables in statistical analysis.
All this said, the only alien sighting involving a katana (that we know of), did occur near a power plant. Perhaps one day we’ll have more data to better substantiate this tantalizing correlation.
