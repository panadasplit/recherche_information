The Next Generation of Distributed & Autonomous Files.

Files haven’t changed since they were born in 1950. They have always been used to storage all kind of information onto bites and make it accessible to everyone.
Files can be adapted based on new available techonologies and provide more value to the end users.
That’s why in FileNation we want to build the next generation of intelligent files (IPFS + Ethereum) and actively contribute to all these changes that computer files are going to have to face over the following years. But how do we see the files of the future?
We see them as files that can allow the sender know when the file has been opened, as some popular apps are including nowadays. The next generation of files will have the option to be opened in an specific time or event or even auto-destruct at an specific event, files will then be created also for the future to improve even more user experience.
This will also have to include an option to send files only to an specific person and make sure that this person is the only one allowed to open the file, even only at an specific place as Geocaching-based files.
Files will also be allowed to be tracked around the world, and to be fully analyzed by their expansion and engagement by the user. We can also call them Smart Files as they will have a bot to talk with the user and their name and color will be automatically generated identifying the content and size of the file.
We strongly think that file sharing has to be also modified and improved, they won’t be shared if one of the users with access doesn’t agree and the permissions system will also be modified. Users will be allowed as well to replace or modify the file in real time using new and disruptive ways like voice control.
Finally, unlocking of files is also important. We are thinking about files that can only be unlocked if a certain amount of people wants to or files that can only be unlocked once a payment is made.
This is just the beginning for a new generation of files, we have the tools and the oportunities are endless. Do you have any other ideas on how Smart Files can improve file sharing?
If you liked the post, please show some love and clap your 👏 hands say yeah 🎵
Follow Alex Sicart on Twitter
Try FileNation
Follow FileNation on Github
Follow FileNation on Twitter
Thanks David Andres for helping.
