Computational design
“Computational design is a different kind of design with a different kind of value approach to how things are made and the idea of perfection doesn’t exist in the same way … you’re constantly adapting to peoples’ needs” — John Maeda, Automattic

Today on the High Resolution podcast, I heard the awesome term “computational design” for the first time.
In the interview, John Maeda differentiates between three kinds of design:
Classical design — the kind of work that goes into making a table or a pair of glasses
Design thinking — design applied to organisations with large teams to solve large problems
Computational design — design involving data, continuously shifting and augmenting for the senses
According to him, this is no wondrous new process but one which he has seen in practice since the 90’s and particularly leveraged in Silicon Valley.
By “the idea of perfection doesn’t exist”, he does not mean that good work itself does not exist but that in computational design, there is never a finished artefact or product because peoples’ needs are always changing and computational design takes this into serious account by leveraging the power of big data to glean key insights and act upon them.
I found this idea particularly fascinating because the buzz phrase one hears everywhere at design meetups these days is that of “design thinking”. However it seems there is clearly a new kid on the block, which I look forward to learning more about and seeing whether it works with design thinking or if it replaces it as a whole new process in itself.
