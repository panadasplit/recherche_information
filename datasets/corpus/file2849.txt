研發如人類般思考的AI是好是壞？

最近觀看AlphaGo Zero完虐Master的棋譜，坦白說，實在有些被AI發展及進步的速度所嚇到。感覺以前對AI的思考，已經不是這麼遙不可及了，也就決心把自己的思想寫下來。當中也有一部分談到自己對靈魂的想像和思考，可能比AI的討論更加激烈。
創造出一個能夠如人類般思考的AI，那它很有可能也擁有人類的人格，擁有了自己的思想。對一個有人格的思考者，我們能夠，又應該，完全控制他嗎？我們能夠，又應該，使其無條件服務人類嗎？
一個能夠如人類般思考，而計算能力以及資訊掌握能力都遠超人類的思考者，更不受人類控制，當然會成為人類的威脅。情況就彷如，當人類演化到某程度，能力到了某程度，無論遷到澳洲或是美洲，都滅絕了當地大部份大形哺乳類動物。最後成為地球的霸主。
人類稱霸地球，當然不是因為人類擁有最強的身體，而是因為人類的智慧。智慧遠超地球人的AI在地球上出現，其結果，可以想像成科技遠超人類的外星人降臨地球：
人類不再是地球的霸主，不再位處生物鏈的極端。
無法為人類服務，又對人類構成極大威脅。這麼多人對研發AI有所抗拒，也是極其合理的事。
有人或會質疑，我們對電腦一直的想像，就是其不含情感，為人類所操控。難道我們就不能在AI的程式中植入服從人類，或者為人類服務的指令嗎？
另外，既然電腦程式不含情感，即便他的思維模式彷似人類，難道就必然會演化出自己的人格嗎？
對於第一個質疑，我們很難否定這個可能性，這也是AI研發沒有中斷的原因。然而，我們若要電腦能夠如人類般思考，就必然賦予其極大的自由度。若是程式的思考處處受限，先不說如此AI就失去了其存在意義，其思考能力也將難以提高。當然，限制與自由往往是相對而非極端的絕對。我們能不能像電影一樣，允許電腦自由思考的同時，設下一些不可違犯的律令（如不能夠傷害人類）呢？這就不得不提及關於人格的問題。
如果AI有自己的人格、自己的意識、自己的意志，它還會無條件遵從人類的指令，無條件為人類服務嗎？有人會質疑，電腦程式畢竟只是程式，即便智慧再高，難道就會發展出意識嗎？
對於篤信靈魂的人而言，電腦智慧再高，但它畢竟沒有「靈魂」，根本沒有可能擁有自己的意識。然而，對於腦神經科學有所研究的人而言，已經很少人仍然相信靈魂的存在了。因為現今的科學，已經逐步找出每個腦區所對應負責的思考活動。
《誰是我》一書中就提及了很多例子：例如梭狀迴臉孔區負責臉部辨認，如果腦部這個區域受損，我們就難以辨認人臉，如果這個區域與負責情緒反應的周邊系統出現連結問題，我們即便能夠辨認人臉，認出誰是誰，卻難以產生相應的情感。又例如，當負責感覺智覺的腦區與負責情緒反應的周邊系統的連結受損，即便知道自己受傷、覺得痛，卻會毫不在意。而最基本的嗅覺、聽覺等知覺，也自己有相應負責的腦區。一直探究下去，大腦的秘密都會逐漸被我們揭開。
既然大腦的運作已經可以解釋我們包括情緒、思考、智覺、記憶等心智活動，靈魂卻無法解釋我們任何的心智活動，靈魂的存在實在難有說服力。現今大部份腦科學家都傾向相信大腦就是思識的說法。
我再簡單推論一下：如果靈魂擁有思考、聆聽、說話、視覺等能力，我們的眼耳口鼻以及大腦根本是多餘的。反過來說，因為我們必須擁有五官大腦才能擁有以上能力，那麼靈魂本身大概根本沒有這些能力。你能想像這樣的靈魂嗎？不能看、不能聽、不能說，更重要的是無法思考。即便思考了，沒有記憶細胞，你也無法記住上一秒的思路。這種靈魂的存在明顯是荒謬的。
沒錯，科學家的確尚未解釋意識的原理，但意識也是源自於大腦。《大腦簡史》的作者說明：「事實上，我們只是間接看到了世界。我們的各種感覺或知覺體驗，其實完全是大腦的產物。我們真正「接觸」到的，只是大腦對這個世界的虛擬摹本。」
科學家發現，如果對受試者的其中一隻眼睛施以強烈的閃爍圖案，而另一隻眼睛則施以不會變動的靜止圖案。因為閃爍圖案過於顯眼吸睛，大腦會主要處理閃爍圖案，以至於短時間內壓制了靜止圖案，令我們看不見它。在這情況下，受試者只會看到閃爍圖案。
然而，即便我們沒有意識到靜止圖案，並不代表大腦沒有接受到靜止圖案的資訊。何以見得呢？原來，愈顯眼的靜止圖案，就會愈快衝破閃爍圖案的「壓制」，讓我們愈快意識到靜止圖案的存在。科學家就發現，如果在靜止圖案列出不符合語法的語句，比起符合語法的語句，受試者會更快意識到靜止圖案的存在。這意味着，即便我們沒有意識到語句，大腦仍然處理了這個資訊，並判斷出語句不符語法。
說到意識的本質，也不得不提神經生理學家Benjamin Libet於一九八三年發表的驚人實驗。實驗中，他要求受試者自由決定一個時間舉起手，並以碼錶回報「舉手意識」出現的時間。結果竟然發現，在「舉手意識」出現的一秒鐘前，大腦已經出現了相關的神經變化。實驗顯示，與其說是我們的意識決定何時舉手，不如說是大腦已經決定了要舉手，再產生這個「舉手的意識」讓我們能夠認知到這個身體變化。
說了這麼多腦神經科學，其實都只想說明一點：無論思考、意識、情緒、記憶等心智活動，都是大腦的產物，與靈魂沒有關係。因此，沒有靈魂的AI，同樣可以演化出人格。而且，現今AI運算的研發，就有借鑑模擬人類大腦的方法，當模擬人腦到了一個地步，AI出現自己的人格也絕非天方夜譚。
如今智能演化速度之快，單看AlphaGo Zero就能知道。再過數年，AI智慧之高可能已經無人能及。操弄智慧比自己高幾級的智慧體，就像一個小孩在成人面前玩弄計謀，其思路在成人眼中根本無所遁形。以為自己有能力控制智能，其實跟小孩玩弄火焰一樣危險。
現實畢竟不是電影，智能會否脫離人類的掌控，尚是未知之數。然而，智能叛變的威脅，絕對不容忽視，否則電影情節即便沒有成真，人類也隨時成為歷史中其中一種被淘汰的物種。
