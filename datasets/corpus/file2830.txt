Inferring the effect of Brand Search using CausalImpact

Did you ever wonder if your Brand acquisition is worth it? Are you willing to understand the incrementality of your brand keyword?
#Just for Fun
if { (Answer(is)==’Yes’)) print(‘this post is for you’) }else{
 print("continue going blind and trust your intuition with fingers crossed"))
}
This post will explain the methodolgy for testing the incremental value given for PPC in Brand Search.
Let’s take a step back and discuss the reasons why brand paid search is incremental:
You take more space in the first page by having paid and organic results
You generate more clicks & conversions
You can control the AdCopy, sitelinks etc etc.
In Travel or others competitve industry with high CPAs you might have some competitors bidding in your brand. (in this case your screwed)
Wait, why don’t you use the adwords Paid & Organic report? https://support.google.com/adwords/answer/3097241?hl=en
If you have a Google Search Console acccount you can link them to your adwords account and view the data directly under this report.
Issues with this report? Look at the average position that triggers for Search Result Type == Is organic search only

Despite the fact that I don’t like viewing data without clearly understanding how the test is runned, the avg position output in my case is innacurate. Don’t get me wrong we are talking around half million euros/year spent in Brand Search, for major brands this is not micro optimization.
The best way to measure to uplift of Brand search is a GEO experiment, this way you can create a solid predictor with a set of control time series not impacted by the intervention.
How does it work? I’ve designed an output digram to make it more visual.

#For those of you who need a refresh or learn about Causal effect

What’s a GEO Experiment?— With this approach you can isolate test & control group with cities.

Measuring Ad Effectiveness Using Geo Experiments
Advertisers want to be able to measure the effectiveness of their advertising. Many methods have been used to address…research.googleblog.com
#Github code
https://github.com/gustavobramao/data-sets/blob/master/GEO_Experiment

I’ve added my github project where I extract predictors that are correlated in the times series and used as a synthetic control method to isolate and compute the counterfactual.
The brillant Google’s R causall impact library does the rest, It trains a machine learning model to estimate the counterfactual. :)
The observation between the counterfactual and potential outcome is the causal effect.
In this case I’ve added the Google’s original plot as example.
Not all test are conclusive, you need to check the probabilty of obtaining this effect by chance. If the p value is ≤0.05; Then the causal effect can be considered statistically significant.
Cheers,
Gustavo.
