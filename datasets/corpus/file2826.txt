Welcome to the Machine, Marketers
Marketers should celebrate the Artificial Intelligence movement or “AI” as the industry calls it. Don’t worry. AI is an overhyped Silicon Valley term that in its current iteration represents more of a natural progression in technology — machine learning — than the birth of sentient beings.

In the marketing technology space, AI attempts to replace many menial marketing tasks. This empowers more creative minds and strategic thinkers to focus on the work they love, rather than the “vulcanesque” data rich tasks that drive them crazy.
The machine learning marketing evolution brings a couple of caveats, of course:
1) First, strategic marketers must comprehend machine learning on macro level and how to use those systems to inform strategy and lead day-to-day tactical exercises.
Second, some more tactical marketing roles will be either replaced or impacted by algorithmic bots.
2) The idea of tasks being performed by bots scares people. But it shouldn’t. Marketers are seeing some of the most time consuming tasks in their business automated, specifically those that revolve around data points. Almost every marketer I know complains about having to do 3x work in 1x space. This relieves much of the pressure.
Still, to not understand machine learning tools antiquates one’s skill set. To stay relevant, marketers need to embrace new technologies like AI and see how they can be incorporated. Algorithms and bots are not sentient. They need guidance to successfully interact with human.
5 Marketing Functions Impacted by Machine Learning

Ever notice how the tech industry positions AI as a heroic savior of efficiency? Yet most people are afraid of it.
Here are the five marketing functions I believe will most likely get impacted by machine learning over the next three years:
1) Community Management
The inability to scale created the biggest knock on social media. Deploying teams of humans across the Internet to reply, engage, and build communities on various social networks remains the luxury of only the largest brands. Further early iterations of automation via Hootsuite and Buffer received low marks from conversation purists who found these offerings inhuman and unengaging.
Current iterations like Social Drift for Instagram level the playing field. New bots and algorithms are let community managers engage in real conversations while delegating mundane tasks of following, liking, and unfollowing fall to the wayside. Further, these new bots fulfill a critical role in identifying and targeting influencers, reducing hours of research.
Community managers should find their tasks to be more enjoyable and less frantic. They will have many tools to make their jobs more fruitful and successful if they adapt the latest tools. Community managers who fail to adapt will likely find themselves falling to the wayside based on performance.
2) Content Marketing
Content marketers will greatly benefit from machine learning. From finding competitive content and relevant source material to optimizing message and delivery preferences, algorithmic programs will greatly assist research and content creation.
Scoop.it is one content research example with its Internet scouring capability. While very helpful, Scoop.it results requires a lot of weeding. This is common with machine learning apps. The early results are not great. The better bots improve as their algorithm optimizes based on human input.
In the near term, AI is unlikely to replace content marketers, simply make them better and more efficient. However, like community managers, content marketers must stay up to date with the latest machine learning tools in order to stay relevant and functional in their jobs.
3) Customer Service

Will customer service bots make contacting companies a better or worse experience?
From chat bots and smart helpdesk sourcing of solutions to actual AI bots answering calls and of course, Alexa, Cortana, Google, and Siri-enabled applications. Yes, there will be a need for real live voices, but only for the most difficult problems.
Did you know companies can integrate Amazon Alexa into their app to answer questions and help you?
Unfortunately, this is one of those areas where machine learning will create significant job loss, particularly in call centers. Expect to see customer service costs and jobs reduced significantly over the next three years.
4) Data Analyst
The data analyst, the person who combs through reports to find prescient data points, and then spits out reports for management will likely see their task automated in the very near future. Increasingly, reports will be offered by algorithms, which when steered and customized to a unique business, will eliminate much of the weekly dashboard testing.
Strong machine learning will also identify emergent trends before a human can, too. Lead scoring system Infer offers a great example of superior data analysis, identifying SQL opportunities well before the human-induced lead scoring algorithm used by most marketing automation systems.
A need remains to steer machine learning to source the right quality data points and ensure that algorithms continue to evolve and meet customer and business model changes. The more strategic data scientist(s) will be required for larger enterprises.
In small businesses, the marketing lead will need a deeper understanding of data science to remain relevant and functional. In essence, someone needs to guide and ensure that data analyzing bots are on point and at a minimum providing useful predictive information.
5) Digital Advertising
Digital advertising is becoming a game of bots. It should be no surprise as this is where the money is at. Google leads the AI ad market with its increasingly complex machine learning-based Google Ads solution that cross pollinates multi-channels to reach customers, even on their smartphones.
As machine learning-based advertising platforms mesh with automation systems and CRM databases, marketer interactions with those systems will increasingly revolve around management. Advertising bots will take inputted spend, target audiences and initial creative and then offer machine learning suggestions. There is a great need for ethics here, as evidenced by the Cambridge Analytica scandal.
In many ways the digital advertising manager will simply approve or correct these suggestions, and then allocate resources as necessary to fulfill them. In the near term, digital advertising agencies can get a leg up on customers by mastering the latest bots and algorithms.
These are just a few of the marketing roles that AI will impact over the next few years. There’s a bot to assist almost every marketing function available now.
But is worrying about job or role replacement the best way to approach the issue? Or should we dive in and embrace the tools provided to us?
Originally published on my marketing blog.
