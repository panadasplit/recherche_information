CityFALCON launches Interactive Personalised News on Amazon Alexa

For decades, we have listened to a one-way radio to get our daily dose of news and music. Spotify came along and redefined the whole experience of consuming music. And now, we, at CityFALCON, are changing your consumption experience for news.
Get started by activating our skill for English-US or English-UK on your Echo.
You could create new watchlists, add topics to follow (from stocks, commodities, foreign exchange, indices, cryptocurrencies, and more), and identify opportunities through trending topics. You could also give feedback on individual stories by liking or disliking them. CityFALCON delivers a personalised experience, and so the more you interact with the feed, the better it gets.
Here are some possible interactions with CityFALCON on Amazon Echo: “Alexa, ask CityFALCON what are the top stories for Apple”, “Create a new watchlist”, “Add Google to my watchlist”, “What is trending right now”, and more. Read more about what you can do with Alexa here.
As a start-up, this is our minimum viable product (MVP) and hence, just a start. We are constantly working on improving the service. Currently, we are a part of Kickstart Accelerator program in Switzerland where have an opportunity to test our product also in the Swiss market and work together with the local corporates.
Here is our vision for the future:

We’ll soon launch our service on Microsoft Cortana and Google Home. If you’d like to sign up for early access, please register your interest here.
