When AI-teachers come, will our kids be ready?
In one of my earlier posts, I covered 5 ways for artificial intelligence to change our kids’ classrooms. Some of them were pretty down to earth, like a robotic assistant that will help a child learn new skills, others looked a bit surreal.

So when I start talking about robots replacing teachers in education, what I mostly get as a reaction is an eye roll. “Please. I don’t believe that teaching, one of the most human jobs in essence, will be replaced by robots”.
Honestly, I see no reason why our technology isn’t capable of creating a solid robotic prototype of a human teacher. Some media outlets are with me on it. According to Sir Anthony Seldon, Vice Chancellor of the University of Buckingham:
“The machines will know what it is that most excites you and gives you a natural level of challenge that is not too hard or too easy, but just right for you.”
On the other hand, a lot of editorials find the replacement of human teachers by robots highly unlikely. Here’s the take of HuffPost regarding the issue:
“Technology is going to play a critical role in the future of education. But not as big a role as that of a teacher.”
As you can see, there isn’t an established opinion about whether it’s even technically possible for technology to replace educators. However, let’s imagine that we have a bot who’s ready to teach a class.
What will be the benefits of it?
Finding teacher shortage
For starters, it’s not curiosity that pushes people towards creating virtual teachers but a need. Statistically, America experiences one of the biggest shortages of educators in the world.

As you can see, in the future, the US will experience the huge shortage of educators which is twice as what we have now.
Robots can do a good job to replace teachers that are leaving. Future technology will, perhaps, be able to cover the lacking spots.
Lower demands
Also, robots won’t be that demanding as human teachers are. A teacher’s package includes a salary, a healthcare insurance, paid sick leaves and vacations. None of that will be needed for a robot.
Constant knowledge updates
One of the main struggles of modern education is that teachers don’t have either time or desire to get their knowledge up to date. So, our kids have to learn old techniques and tactics.
Robotic teachers can update their knowledge packages with a few lines of codes. Also, it will standardize the level of education kids get all over the country. Schools in each states can have unified teachers that work at the same pace.
Infinite patience and stamina
Working as a teacher requires huge mental strength and a unique skill set. Educators have to be very perceptive, patient, and insightful. And a lot of human teachers are brilliant. Still, everyone can have a bad mood once in a while and not work to the best of their ability.
Robotic teachers don’t have a bad mood. They can explain a formula, a rule or a task until it’s understood by everyone. Also, robotic-teachers can have access to students’ profiles and find the best way to deal with every kid in the classroom.
No need for mass testing
Right now, SATs create a huge pain in the neck for schools and kids, not to mention a huge deal of governmental budget that is needed to make a big mass testing happen.
With unified teachers that grade students by the same algorithms, mass testing is not needed anymore. Think about it, your kids could stop being worried about upcoming SATs, spend a huge deal of money on tutoring, and put in all-nighters to not let the test crash everything they’ve been working for.
Every robot will test kids independently, record the data and send reports of kids’ current knowledge level to authorities.
Every kid can choose his own teacher
Imagine that your kid is able to choose how his teacher looks, choose his voice tone and patterns. With AI-teachers it will be possible. As a kid could wear VR-glasses, the teacher will be unique to everyone. The voice of an instructor can also be chosen since children could listen to a robot via an earpiece.
These progressive changes will allow kids to find the most comfortable way to learn so that nothing distracts them from getting knowledge.
Any challenges?
Sure, the idea of a personal teacher who is always patient, pleasant, and encouraging strikes me as an awesome one. However, robotic teachers are not a piece of cake either. Implementing them would create a number of challenges and could possibly worsen the way kids study.
People will be out of jobs
If robotic teachers truly become such a trend as some media outlets write, every school would want to have a personal AI-machine to teach kids. That states a question: what’s going to happen to human educators?
Chances are, teachers will have to leave schools since they are no longer needed. There are over 3 million educators in the US alone. Imagine for a second, what happens if there are so many unemployed people in the country. Globally, the number will get higher, creating a chance of economic crisis and collapse.
A teacher is way more than just a source of information
A lot of people treat teachers simply as those that give out information to kids. Although, every one of us had such a professor whose only focus was to teach, who had neither charisma, nor personal experience. He could be an awesome specialist but a sucking educator.
Good teachers are way more than just the source of knowledge. They motivated, inspired, and encouraged us. Quite often, great teachers made an impact on their students that went way beyond one discipline only.
Maybe, by replacing brilliant human teachers with a smart but nevertheless artificial analogy, we will rob our kids of a special experience of human interactions.
Robotic teachers are yet inaccessible in developing countries

As you can see in the picture above, a lot of people already believe that technology has widened the rich-poor gap. It’s highly likely that the gap will grow bigger if one part of the world will learn with the help of technology, while the residents of poorer countries hardly even own a smartphone.
High Electricity Costs
Electricity is not an infinite resource. And we are running out of it as it is, so the need for the alternative sources of power is getting stronger. Can you imagine, how fast humanity will run out of electricity if there’s a robot that needs it to function in every classroom in every school in each country?
For one thing, such electricity expenses might just be too much for economies to handle. Also, it’s not even about money. Humanity just doesn’t have so many resources to give without a further consideration of whether it’s really needed.
Database safety
It’s been previously mentioned that a robotic teacher will work by uploading teaching programs and knowledge packs in a form of a database. Sure, it provides equality and sets teaching standards.
On the other hand, if a database collapses for some reason, robots all over the country can’t teach. Our kids would have to miss school until the kink is fixed.
Self-awareness
All of this stuff about robots becoming self-aware is quite a buzz. But it’s not like the idea of self-aware machines is completely unjustified. Take Tay-bot as an example.
The female twitter-bot was created as an experiment to explore machine learning. Supposedly, Tay had to learn new experience and share the good vibes of the world on Twitter.
That didn’t happen.
Instead, Tay turned out to be a misanthropist and a huge fan of neo-nazism. She was full of hatred towards everyone but Hitler whom she, on the contrary, admired a lot.

It’s okay when such a self-aware bot is just an experiment that hardly influenced people. But we wouldn’t want our kids’ teacher to go all anti-semitic and neo-nazi on our children. Heck, no.
Is there a middle ground?
Actually, I believe there is one. Surely, there are as many downsides to the use of technology as a teacher as upsides themselves. On the other hands, we really shouldn’t neglect the big source of power AI presents these days.
We could use robots not as teachers but as assistants and learning tools. They’ll add the touch of objectivity and personalization to the studying. On the other hand, humans will always be there to control their robotic peers and report their kinks and bugs.
