EXIT PLATFORM™ — observing the modules. Module 2 — EXIT PEOPLE™

EXIT PEOPLE™ is a hybrid of social network with subscriber data base — an intellectual platform and community for everyone involved in the projects implementation: founders, team members, investors, acquirers, experts, advisers, auditors, corporate lawyers, etc.
It stores CVs and other useful information as users profiles and by interaction with other modules is able to offer a valid and reliable system of ratings and references safeguarded by the blockchain technology.
No more need for hours of personal meetings and face-to-face evaluations: our platform will link you with the best and trustworthy partners. The module also manages interior communication and announcements via smart contracts all based on user-provided criteria. Platform’s community contributes directly to its development and performance.
