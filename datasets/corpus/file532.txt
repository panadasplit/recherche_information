AI Biweekly: 10 Bits from May W3 — May W4


May 15th — Fujitsu and Nissha Develop AI Solution for Bookstores
Fujitsu and Japan Publishing Co. (Nissha) together introduce SeleBoo, an AI solution the leverages information on 3.5 million books and some 3,000 bookstores nationwide to automatically stock books based on characteristics of each bookstore. Bookstore staff can better understand customer purchase trends and update book displays accordingly.
May 16th — Sony Real Estate Launches AI Website for Condominium Sales
Sony Real Estate releases an AI-powered intelligent website, “Apartment AI Report,” which can estimate price ranges of condominiums based on prefectures, villages, stations and other information. The system can automatically estimate purchase price changes due to the age of the house for example. It can also rank average condominium sale prices by area, age of building, floor plan, etc.
May 17th — Intel and Mobileye Test Self-Driving Car Fleet in Jerusalem
Intel and Mobileye start the testing phase for their fleet of 100 autonomous vehicles in Jerusalem’s challenging traffic conditions. Israel-based Mobileye’s technology is driving the project with a Responsibility-Sensitive Strategy to improve safety and prove their technology is capable of handling the city’s diverse geography and driving conditions.
May 21st — Johns Hopkins & Lustgarten Develop AI Solution for Pancreatic Cancer Detection 
The world’s largest centres for pancreatic cancer treatment employ NVIDIA GPUs to develop a deep learning solution for early detection of pancreatic cancer. With support from the Lustgarten Foundation, Johns Hopkins Hospital has launched the Felix project, which leverages big data to train a machine learning algorithm to spot pancreatic cancer at an early stage. Test results of the machine learning model detected pancreatic cancer with a success rate of 90%.
May 22nd — Samsung Announces Three New Global AI Research Centres
Samsung Research will launch AI Centers in Cambridge, the U.K. (May 22nd), Toronto, Canada (May 24th), and Moscow, Russia (May 29th), joining existing centres in South Korea and the United States. The Toronto Centre will be led by Dr. Larry Heck, Senior Vice President of Samsung Research America, and focus on developing core AI tech with strategic cooperation from local universities; while the Moscow Centre will draw from Russia’s expertise in mathematics, physics and other fundamental science. Samsung Research plans to raise its global number of AI researchers to 1,000 by 2020.
May 22nd — Amazon Adds Calendar Appointment Capability to Alexa 
The Alexa voice assistant gains a new smart calendar function which will allow users to reschedule meetings based on availability. Users can ask Alexa to move an existing meeting, and the Amazon assistant will search through the different schedules of meeting participants to suggest available time slots for rescheduling.
May 23rd — Intel Introduces the Latest Nervana Neural Network Processors 
At the Intel AI DevCon, Intel VP and GM Artificial Intelligence Products Group Naveen Rao introduces optimized Intel Xeon Scalable Processors which deliver significant performance improvements in training and inference compared to previous generations. Based on feedback from the Nervana Neural Network Processor (NNP) prototype Lake Crest, Intel is now building the first commercial NNP product, the Intel Nervana NNP-L1000 (Spring Crest), which will be available in 2019.
May 24th — Singapore Collaborated with London to Launch AI in Finance Online Course
Singapore’s Ngee Ann Polytechnic (NP) and the London-based Centre for Finance, Technology, and Entrepreneurship (CFTE) jointly launch the first industry-led AI in Finance online course. The collaboration addresses the need for finance and technology professionals to update their skills. NP and CFTE have brought together 20 industry and thought leaders to share their AI and fintech expertise.
May 24th — Uber Builds Technology Centre in Paris to Develop Flying Taxis
Uber will spend US$23.4 million over the next five years to develop the AI algorithms, air traffic control standards and other tech necessary to send taxis soaring over cities. The project will include machine learning-based transport demand modeling, high-density low-altitude air traffic management simulations, integration of innovative airspace transport solutions and the development of smart grids.
May 24th — Intel AI Lab Open-Sources Deep Learning-Driven NLP Library
Intel AI Lab open-sources a Natural Language Processing library, which will enable researchers and developers to build conversational agents on chatbots and virtual assistants. The library includes valuable features such as name entity recognition, intent extraction, and semantic parsing of words. Intel AI Lab also open-sourced libraries for reinforcement learning and neural networks. The reinforcement learning library allows users to embed an agent in training environments, while the neural network library is able to strip away non-relevant neural connections.
* * *
Analyst: Synced Industry Analyst Team | Editor: Michael Sarazen
* * *
Subscribe here to get insightful tech news, reviews and analysis!

* * *
The ATEC Artificial Intelligence Competition is a fintech algorithm competition hosted by Ant Financial for top global data algorithm developers. It focuses on highly-important industry-level fintech issues, provides a prize pool worth millions. Register now!

