The Coinscious Summer Tour — July
For us here at Coinscious, this has been the summer of conferences and expos. We’ve been on an international tour — Amsterdam, Atlantic City, Montreal, San Francisco, DC, and Krakow. Our goal: to show businesses the new Coinscious Collective™ platform and services, and to connect with thought leaders doing groundbreaking work with blockchain and cryptocurrency.
First up was the Blockchain Expo in Amsterdam, packed with 8,000 attendees. Day one focused on ICOs and cryptofinance, while day two focused on the effect of blockchain on insurance, banking, and payment options. Both days delivered insight on how blockchain tech is transforming existing business models. There was also significant focus on challenges developers face as they try to build blockchain apps.
David Buell (Coinscious Marketing Director) and Binh Ho (Coinscious COO) at the Blockchain Expo in Amsterdam, Netherlands
Between conferences, our CTO, Daniel Im, made his way to Atlantic City for the Blockchain World Conference. He listened to leaders weigh in on potential US blockchain regulations. There he also offered his congratulations to Amir Dossal for winning the inaugural Blockchain Humanitarian award, an award created to honor leaders who are using blockchain technology to solve real-world challenges.
Allison Shealy (Attorney at Shulman Rogers), Daniel Im (Coinscious CTO), and Amir Dossal (‘Blockchain Humanitarian’ Award Winner)
From there, we attended Startupfest in Montreal. This has been called “a music festival for startups” by Reddit Founder Alexis Ohanian. The event takes pride in rethinking how conferences should be done. It’s a place where startups can shine, make connections, meet interested investors, and learn from others who have successfully navigated their own startup journey. While there, our CTO, Daniel Im, discussed how to bridge the gap between AI research and the AI industry.
Daniel Im (Coinscious CTO) at Startupfest Montreal
Next, it was on to San Francisco for Distributed 2018, where we were a sponsor. Distributed focuses on the power of the decentralized business and aims to bridge the gap between Eastern and Western blockchain initiatives. At one panel, leading cryptocurrency exchange leaders discussed the quickly changing landscape surrounding the coin market, its infrastructure, regulations, and oversight.
Coinscious was a sponsor at Distributed 2018 in San Francisco. Shown here are Binh Ho (Coinscious COO), Agnieszka Osuch (Conscious Communications Manager), Daniel Im (Coinscious CTO), and Ena Vu (Coinscious Project Manager).
Cointime interviewed Coinscious at Distributed 2018. With Ethan Skowronski-Lutz, David Buell (Marketing Director), Binh Ho (COO), and John Gidding
Also at Distributed, Daniel connected with Bianca Chen, who is currently executive producing the docuseries Next: Blockchain.
Bianca Chen (“Next: Blockchain” Executive Producer— middle left), Daniel Im (Coinscious CTO — middle right)
In Krakow, Poland, we hosted an event for local blockchain and cryptocurrency enthusiasts. Our COO, Binh Ho, spoke to attendees about AI and data-driven insights for the coin market.
Binh Ho (COO) hosting a blockchain event in Europe
On this tour, we met a lot of amazing people and companies. These interactions showed us that, now more than ever, there’s a need for our platform. The blockchain industry is really starting to develop. Thought leaders and professional and amateur investors are looking for a platform like the Coinscious Collective™ that can help them navigate and respond to the nuances, complexity, and volatility of the coin market.
Our whirlwind summer tour isn’t over yet. During August, we’ll be in Asia, connecting with people passionate about blockchain and cryptocurrency. We’ll also be in Las Vegas at BlockShow. Be sure to follow our journey on social media:
Coinscious (@coinscious_io) | Twitter
The latest Tweets from Coinscious (@coinscious_io). AI & Data Driven Insights for the Coin Marketwww.twitter.com
Coinscious | Facebook
Coinscious. Follow us on Facebook!www.facebook.com
Coinscious (@coinscious) * Instagram photos and videos
46 Followers, 1 Following, 11 Posts - See Instagram photos and videos from Coinscious (@coinscious)www.instagram.com
Coinscious Chat | Telegram
You can view and join @coinscious_chat right away.t.me
