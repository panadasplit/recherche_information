
Why AI Terrifies the World’s Greatest Minds and How it’s Inevitable Machines Take Over
AI’s most simple concept is terrifying
If it *only* reaches the same level of intelligence as us, it’s ability to operate at a far higher speed means that in 6 months it would have effectively operated for our equivalent of 500,000 years.
For comparison — it has taken us around 200,000 years to reach where we are now as a species
Let that sink in for a moment
In a period of 6 months for us, AI will have accumulated half a millions years worth of knowledge on top of everything we already know meaning that we will be unable to compete with them within days of them achieving the same levels of intelligence as us.
We won’t even have time to react because machines will have surpassed us literally within the blink of an eye.
In the same way we couldn’t relate to the earliest humans AI won’t be able to relate to us
This, at best relegates us to the role of how we treat pets now
Or worse — Ants
When ants stay out our way we leave them alone.
When they invade our homes or obstruct our intentions we obliterate them without a second thought. They are an insignificance that can be eradicated.
This is the likely outcome for AI and us
It is why AI is a zero sum game.
How would Russia or China react if they though the States were on the brink of engineering such dominance?
How would they react if there were even murmurs of rumour that it was close?
This isn’t a technology that can be competed with — if you are 6 months ahead you have 500,000 years worth of knowledge more than the competition.
The winner literally takes it all
Then likely loses it all to the AI itself
This exceeds the Manhattan project by several orders of magnitude, both in terms of danger and graveness to the future of humanity.
This is why the world’s greatest minds are terrified
This isn’t just the most likely outcome — it is inevitable
If any rate of progress is assumed it is unavoidable that we reach this point in at an undetermined point in the future.
But it will be far sooner than we think

In singular tasks, machine already bests us with ease. Chess, Jeopardy, and Go are all examples of this. Machines can lift far more than we ever could and calculate things that would take us years.
A computers ability to operate computationally millions of times quicker than we are able to comprehend is an insurmountable barrier to our operation alongside them.
It is why enabling human machine connectivity is critical to the survival of our species. Without it, we are worse than cavemen. It is critical that we expand our bandwidth as soon as possible.
Yet people are scared of what Crispr means for development of a different class of human. Sure, genes may be edited to create a superior version of man — more intelligent, more beautiful, less susceptible to serious illness — but they will still be human.
AI won’t be
I know where my fears are placed
If AI accumulates the equivalent of 2 years of our knowledge each minute we are finished. Potentially we already are — we have never taken a step back from an impending technological innovation because of it’s danger to the survival of humanity — just look at nuclear bombs.
It doesn’t even matter if they are benevolent
Eventually, they will be operating so far out of our realms of comprehensionthat their actions will affect us as a byproduct of their intentions.
It’s as simple as that
And that is if you assume they only reach the equivalent intelligent as us and never progress past that.
But They will
All that it now requires is a way to combine all these individual elements of intelligence into a singular AI that is able to make use of them all. Verticallythey already exceed us in every area. Horizontally they don’t come close. This is to mean their general intelligence lets them down in the broadness of it’s capability.
Once they achieve this it is game over
In the blink of an eye a single machine has accumulated years of human level knowledge, literally.
The second our goals diverge the AI is in control of our destiny
It’s evolve or face extinction
Killed by our own creation
