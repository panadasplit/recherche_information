AI and Machine Learning Applications we use every day without knowing
Buying a mobile phone online? Then “People who bought this also bought these” is a recommendation system which you probably noticed in Amazon.in. This is a popular Machine Learning usage you have encountered. So, there are plenty of applications where Machine Learning and AI are being used in our everyday life without our acknowledgment.
So today, we are looking about those applications where Machine Learning and AI are implemented and still go unnoticed.
Gmail- Machine Learning
Have you ever wondered, how your mailbox automatically segregates spam, important, updates, social? Well, if you are using a public mail service app like Gmail, then you would come across these labels in your Inbox. Google is a tech company where you are given the products for use for free. So, when a mail arrived and way before landing in a particularly labeled inbox, it is scanned by an algorithm which determines where this mail has to be landed. In case it from a person from your contact list, it should be in Important, otherwise spam if it contains any unknown sources, suspicious links, keywords that sound so true to be good.

Recently, the Gmail is facelifted with material design and stuff and if probably noticed, there are few instances where while writing/typing you are suggested with some words or even sentences. Well, this has to be by the Machine Learning techniques. This feature had been launched in early 2018. Google says that this feature can help in more productivity.
Google Lens — AI
Most of us using Android mobile phones know also using the default photos app by Google. It has gained much significance recently as they are offering free storage space. Moreover, they have more tech like “Google Lens” implemented into the Photos app. Which can able to recognize text and some other features from the photo. This is the most interesting and next-generation features of AI.
Also, this app can adjust the photo and make it more beautiful like adjusting the filters, brightness etc. With just one tap, you can do all of this.
Gboard / IOS keyboard — Machine Learning
A mobile keyboard. And I hope you didn’t expect this coming. Well if you then that’s good. Yes, a mobile keyboard uses Machine Learning to predict things which we are going to type and suggest words that may be useful for our communications.

Amazon Recommendations- Machine Learning, AI
So, have you wondered what might be the other products which people tend to buy from Amazon when they are buying a particular product? If you want to know that, then check this “People who bought this also bought this” kind of section next time when you are buying something from the online tech giant.


Voice Assistants– Machine Learning, AI
We are finding voice-enabled assistants everywhere. Be it is Alexa by Amazon, Google Assistant, Cortana by Microsoft, Siri by Apple. All these searches are powered by these new technologies.

Conclusion
So, these are few of the major applications where the Machine Learning and AI are being used in our day to day lives. These are just the start of the AI revolution and there is a lot of scope of implementation of this technology.
Also Read:
Originally published at www.flipdiary.com on September 30, 2018.
