Cortex项目进度报告<20180615第六期>
各位关注Cortex项目的小伙伴大家好，
Cortex第六期项目进度报告新鲜出炉，
和大家介绍近两周Cortex项目的最新动态。
端午佳节，Cortex Labs团队所有成员
祝福所有支持者、投资者阖家欢乐！砥砺前行，风雨同舟。
社区建设
（图注：数据来自Coinmarketcap ）
截止2018年6月15日，参考Coinmarketcap数据，CTXC流通市值为：117,456,461 USD/17,754 BTC/228,893 ETH。
CTXC持仓地址数量达到20667个，交易量达到33321笔，持仓数量和交易笔数均稳中有升（数据来源：Etherscan）。
空投活动持续中，社区热度进一步提升。近期空投活动中出现的bug得到修复，社区热度进一步稳定增长，社区环境得到改善。
扫描下方二维码获得CTXC空投教程：

参与Cortex的空投活动赢取CTXC奖励入口：
pc端链接：http://t.cn/R3AvZ9V
或扫描以下二维码：


截止6月15日下午，
Twitter粉丝关注人数增长至22.1K ；
Telegram英文群组人数增长至81K+；
Telegram中文群组人数增长至26k+；
Reddit关注人数增长至6K；
微博自建立之后的一个月里从零增长至2.2K。
登陆SWFT Blockchain
2018年6月5日，Cortex正式登陆一站式转账平台SWFT Blockchain。被硅谷誉为“下一代区块链全球转账协议”的SWFT Blockchain运用区块链，机器学习，大数据，实现了费用低廉，安全迅速的币币兑换。

媒体曝光和线上线下活动
日韩之行
2018年6月7至8日，Cortex Labs团队受邀参加韩国首尔举办的Blockchain Korea Conference。项目创始人兼CEO陈子祺在会上发表了关于AI+Blockchain的演说，并和多位项目创始人在会议研讨环节进行对话。次日，Cortex举办了韩国市场首场线下Meet Up活动，吸引了当地投资机构，学界和媒体等多方参与。CEO同时接受了韩国当地权威媒体Asia Economy TV的特邀专访，对媒体提出的各类技术层面问题进行回答，并介绍了团队情况。Cortex团队同样前往日本拜访了当地人工智能行业企业，商讨相关合作事宜。



美国之行
2018年6月11至12日，Cortex Labs受邀在美国硅谷举办的CPC Crypto Developers Conference会议上发表演讲并参与会议专题讨论，同时该会议吸引了来自全世界数千名开发者的参与。在会议上， Cortex团队会见了项目学术顾问2015年图灵奖获得者Whitfield Diffie教授。


台湾之行
2018年6月14日的OKEx全球Meet Up系列活动台北站里，Cortex团队同样受邀参与，并进行了Cortex项目介绍。

Reddit AMA
2018年6月8日北京时间上午9:30–12:30，Cortex团队在Reddit上与Cortex国际社区的项目粉丝进行了AMA（Ask Me Anything）互动。这也是继4月份的首场Reddit AMA后，2018年Cortex与社区进行的第二场问答互动。在Reddit的AMA活动中，全世界各国的社区粉丝皆可进行参与，项目方必须在活动进行期间对粉丝们提出的各类问题进行一一解答。

主流媒体报道
Cortex在美国市场同样获得了各大主流媒体的报道，其中包括NBC，CNN，CBS，China Daily等，一些媒体将Cortex项目评价为“The First Next Gen（新一代技术中的先驱）”。


联系我们
网站：http//www.cortexlabs.ai/
Twitter：https//twitter.com/CTXCBlockchain
Facebook：https//www.facebook.com/cortexlabs/
Reddit：http//www.reddit.com/r/Cortex_Official/
Medium：http//medium.com/cortexlabs/
电报：https//t.me/CortexBlockchain
中国电报：https//t.me/CortexLabsZh

