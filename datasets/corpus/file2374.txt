Did You Know This About AI?

Originally published at domenica-cresap.com on January 19, 2018.
With each passing day, artificial intelligence (AI) is transforming into something bigger and greater than what we knew of it yesterday. Because of this, it sees more attention than ever from investors. Advancements in AI isn’t going to impact just a business’s products and services, but instead almost every aspect of their business. Here are some interesting facts about where AI is today.
AI is a catch-all phrase
 The term “artificial intelligence” is an umbrella term for a few different definitions such as machine learning and deep learning. The AI we think of that is shown in movies is not quite where technology is at yet. To learn more about this, check out Chris Neiger’s article.
AI will boost our future global economy
 A 2017 report from PwC claims that by 2030, AI will have the potential to contribute over 15 trillion dollars to the global economy. Not only that, but North America is expecting to see an increase of 14.5% of GDP as well.
Booming car industry
 By 2027, only nine years from now, it’s expected that the self-driving car industry will be worth $127 billion. AI plays an important part in making this become a reality.
Better home connections
 Amazon’s Echo line and Alphabet’s Google Assistant are both powered by their own artificial intelligence. These are just the big names in the business. It’s expected to see more growth with the increasing popularity of these AI-powered, personal assistants.
Impacting e-commerce
 Amazon is already implementing the use of AI technology to recommend specific products to its users. Also, it will be helping a business determine which deals to offer and at what time for better customer experiences and to help grow the business.
Alphabet already has a leg up
 Google’s parent company has its own AI processor which makes its services, such as its search engine and Gmail, smarter. This processor is called the TPU — the Tensor Processing Unit.
It could be a dangerous path
 The technology guru, Elon Musk, has not been quiet about his concern for humanity with the advancements of AI. He suggests there be regulations set in place and the complete avoidance of autonomous weaponry. Read more about his warnings here.
Loss of jobs
 According to Stephen Hawking, AI will make many jobs disappear without being able to create new ones in exchange at the same rate. But, it’s not just Hawking making these claims, PwC has also predicted that over the next 15 years, we could see a decrease in 38% of U.S. jobs.
