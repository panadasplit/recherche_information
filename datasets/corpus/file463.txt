One of the pressing questions that arise with artificial intelligence is how to account for the actions of machines that make decisions by themselves. Image credit — ITU Pictures, licensed under CC BY 2.0
Digital age ‘desperately’ needs ethical and legal guidelines
Technologies such as artificial intelligence and robotics raise new problems for society.
by Joanna Roberts
Digital technologies such as artificial intelligence and robotics, ‘desperately’ need an institutional framework and system of values to help regulate the industry, an ethics expert has told leading scientists and policymakers.
Jeroen van den Hoven, professor of ethics and technology at Delft University of Technology in the Netherlands, was speaking at a session on ethics in science and technology at the EuroScience Open Forum (ESOF) 2018, which is being held in Toulouse, France, from 9–14 July.
‘People are becoming aware that this digital age is not neutral…, it is presented to us mainly by big corporations who want to make some profit,’ he said.
He called for a Europe-wide network of institutions that can provide a set of values, based on the EU’s Charter of Fundamental Rights, which the technology industry could operate within.
‘We have to set up, as we’ve done for food, for aviation and for traffic, … an elaborate system of institutions that will look (at) this field of artificial intelligence.
‘We need to think about governance, inspection, monitoring, testing, certification, classification, standardisation, education, all of these things. They are not there. We need to desperately, and very quickly, help ourselves to it.’
Prof. van den Hoven is a member of the European Group on Ethics in Science and New Technologies (EGE), an independent advisory body for the European Commission, which organised the session he was speaking at.
In March, the EGE published a statement on artificial intelligence (AI), robotics and autonomous systems, which criticised the current ‘patchwork of disparate initiatives’ in Europe that try to tackle the social, legal and ethical questions that AI has generated. In the statement, the EGE called for the establishment of a structured framework.
The European Commission announced on 14 June that they have tasked a high-level group of 52 people from academia, society and industry with the job of developing guidelines on the EU’s AI-related policy, including ethical issues such as fairness, safety, transparency and the upholding of fundamental rights.
The expert group, which includes representatives from industry leaders in AI such as Google, BMW and Santander, are due to present their guidelines to the European Commission at the beginning of 2019.
‘People are becoming aware that this digital age is not neutral…, it is presented to us mainly by big corporations who want to make some profit.’
- Professor Jeroen van den Hoven, Delft University of Technology, Netherlands
Bias
Ethical issues surrounding AI ­– such as bias in machine learning algorithms and how to oversee the decision-making of autonomous machines — also attracted widespread discussion at the ESOF 2018 conference.
One major concern emerging with the fast-paced development of machine learning, is the question of how to account for the actions of a machine. This is a particular issue when using AI based on neural networks, a complex system set up to mimic the human brain that enables it to learn from large sets of data. This often results in algorithm becoming what is known as a ‘black box’, where it’s possible to see what goes in and what comes out, but not how the outcome was arrived at.
Maaike Harbers, a research professor at the Rotterdam University of Applied Sciences in the Netherlands, said that this was an important issue in the military, where weaponised drones are used to carry out actions.
‘In the military domain, a very important concept is meaningful human control,’ she said. ‘We can only control or direct autonomous machines if we understand what is going on.’
Prof. Harbers added that good design of the interface between humans and machines can help ensure humans exercise control at three important stages — data input, processing and reasoning, and the output or action.
Even in technologies that use AI for purposes that seem to be overwhelmingly positive, such as companion social robots for children, raise some tricky ethical issues. The conference audience heard that researchers working in this area are grappling with the effect these technologies can have on family relationships, for example, or whether they could create inequalities in society, or if they might create social isolation.
In the field of automated transport, researchers are also looking at the impact self-driving cars might have on wider issues such as justice and equality. They are investigating questions ranging from how to ensure equal access to new forms of transport to who should benefit from any cost-savings associated with automated transport.
However, the values we instil in AI may be a key factor in public acceptance of new technologies.
One of the most well-known moral dilemmas involving self-driving cars, for example, is the so-called trolley problem. This poses the question of whether an autonomous vehicle heading towards an accident involving a group people should avoid it by swerving onto a path that would hit just one person.
Dr Ebru Burcu Dogan from the Vedecom Institute in France, said research shows that while people were in favour of a utilitarian solution to the dilemma — for example, killing the driver rather than five pedestrians — they personally wouldn’t want to buy or ride in a vehicle that was programmed in such a way.
‘We all want to benefit from the implementation of a technology, but we don’t necessarily want to change our behaviour, or adopt a necessary behaviour to get there.’
If you liked this article, please share it.

See also
We want to end the de-industrialisation of Europe — Prof. Jürgen Rüttgers
‘Earworm melodies with strange aspects’ — what happens when AI makes music
Computers learning to read, watch and understand
Dreaming robots and creative computation — the future of AI takes shape
Creative computation and the What-If Machine
More info
European Group on Ethics in Science and New Technologies
ESOF
Originally published at horizon-magazine.eu.
