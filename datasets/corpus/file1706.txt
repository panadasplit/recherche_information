🤔 Unexpected Consequences of Using Ai, Liberate Ai from “guys with Hoodies”, Physiognomy’s New Clothes…

Unexpected consequences of using AI meeting schedulers — medium.com 
 True AI is great. You can use it to predict system errors, drive cars across the country, and stay healthy, wealthy, and wise. But please keep it out of my inbox. AI assistants are all the rage and…

Melinda Gates and Fei-Fei Li Want to Liberate AI from “Guys With Hoodies” — backchannel.com 
 Melinda Gates and Fei-Fei Li discuss the promises of artificial intelligence, and how to diversify the field, in a Q&A with Backchannel’s Jessi Hempel.
Accepted Papers, Demonstrations and TACL Articles for ACL 2017
chairs-blog.acl2017.org
The below lists the accepted long and short papers as well as software demonstrations for ACL 2017, in no particular order.

Every single Machine Learning course on the internet, ranked by your reviews — medium.freecodecamp.com 
 A year and a half ago, I dropped out of one of the best computer science programs in Canada. I started creating my own data science master’s program using online resources. I realized that I could…

Physiognomy’s New Clothes — Blaise Aguera y Arcas — medium.com 
 Deep learning is a powerful tool for analyzing human judgments to unmask prejudice. When models are trained on biased data, then called “objective”, the result can be a new kind of scientific racism.
