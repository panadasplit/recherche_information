
Diseñando una experiencia digital de salud para pacientes/usuarios. Parte 1
Por: Pamela Armstrong consultora UX senior y docente, Consuelo Pizarro C. consultora UX/UI senior.
En este artículo relataremos los aprendizajes de nuestra experiencia como equipo de UX-UI diseñando un portal de salud que concentra la información de clínicas, centros médicos y clínicas dentales.
El desafío era homologar en un solo lugar información que cada negocio trataba de manera distinta, considerando a nuestro usuario/paciente en primer lugar y su necesidad de resolver un problema de salud. Por otro lado debíamos contemplar necesidades del negocio y del área médica.
Trabajamos sobre un modelo que integra los sitios web de las clínicas y centros médicos, buscando optimizar la operación en red para la organización y los clientes. En esta integración debíamos considerar: 9 clínicas, 3 clínicas dentales y 32 centros médicos.
El encargo que recibimos como equipo era diseñar una plataforma que incluyera a todos los prestadores de salud juntos. Para eso, desde ese momento hacia el futuro, la idea es generar un HIS (Hospital Information System) que concentrará toda la información que se comience a reunir desde el sitio web, la agenda y en el futuro las fichas de los pacientes y funcionamiento interno.
Nuestro objetivo principal era Diseñar un portal que permitiera:
Encontrar información sobre clínicas, clínicas dentales y centros médicos.
Agendar desde el portal, con cualquiera de los prestadores/médicos.
Teníamos que incluir la información y funcionalidades de todos los sitios web de las clínicas y centros y disponibilizar la información de cada uno y también sus diferencias de negocio, sin perder el mayor atributo de cada una, por ejemplo la clínica X tiene como foco la maternidad y todos los servicios asociados a este tema, la clínica Y se enfoca en segmentos medios-bajos de clientes y tiene convenios con Fonasa.
Había que generar una nueva identidad, ahora serían todas las clínicas y centros juntos una sola cosa y desde este momento la operatividad del negocio sería la misma para todos.. Teníamos la oportunidad de diseñar algo nuevo, más cercano y útil para los usuarios/pacientes.
Los 12 aprendizajes
A medida que fuimos realizando el proyecto con el equipo, nos enfrentamos a diversas situaciones, de las que aprendimos mucho sobre aspectos que no están totalmente mapeados como parte del rol de UX como tal, pero que, de cierta forma, son parte de la gestión de un proyecto exitoso. Nuestros aprendizajes fueron 12.

Generar equipo y un espacio de confianza
Desde el primer momento tuvimos claro que la relación con el cliente sería clave. Era un proyecto de varios meses de trabajo y necesitábamos generar una buena relación que nos permitiera hablar con confianza y no tener miedo de contradecir a nuestra contraparte sin caer en enfrentamientos y conflictos.
Realizamos talleres, jornadas de trabajo y revisión, y reuniones semanales (donde la comida era un detalle importante). Además cada miembro del equipo, desde su área de expertise, era capaz de comunicarse con el cliente para no depender exclusivamente de una persona en particular.
Gracias a lo todo lo anterior, logramos que el cliente se sintiera parte de nuestro equipo. Lo empoderamos para que fuese capaz de defender con argumentos claros las decisiones de diseño, como si fuese uno más de nosotros.
2. No llevar problemas, ofrecer soluciones
Sabíamos que el cliente no tenía dedicación exclusiva para este proyecto, por lo que cada vez que detectamos un problema, pensamos posibles soluciones. La idea era detectar el problema pero siempre ofrecer alternativas de solución.
Uno de los principales problemas que descubrimos fue que cada clínica hablaba un lenguaje distinto, por lo que generamos sesiones de trabajo y entrevistas para entender mejor la información. Propiciamos la comunicación entre los diferentes stakeholders y generamos matrices y definiciones de servicios, unidades, programas y centros. La tarea más compleja fue la homologación de los servicios y las especialidades, para lo cual se realizaron comisiones y se estableció un calendario de reuniones con el fin de tener fechas de entrega claras.
3. Adelantarse a los problemas, coordinar y conducir
Detectar el problema era el primer paso, el siguiente era orquestar todos los factores que llevasen a buen puerto nuestras propuestas. Mandar emails, coordinar reuniones, llamar a quien fuese necesario e impulsar la comunicación entre los interesados.
El proceso de homologación de servicios y especialidades fue el mejor ejemplo de esto. En este proyecto fue clave nuestra intervención. Tuvimos que impulsar la formación de una comisión médica para la homologación de servicios, unidades y especialidades. Nuestro rol fue moderar estas instancias y hacer que los integrantes de la comisión pensaran en el lenguaje más cercano al usuario.
Para apoyar la encontrabilidad, diseñamos un motor de búsqueda predictiva, que estaría conectado con la información disponible en el sitio, con artículos y estrategia de SEO, de tal forma que el usuario pueda llegar a la información precisa tanto desde dentro como fuera del sitio.
4. Tener el rol de facilitador
El estar a cargo del diseño de un nuevo portal de salud fue una tarea difícil y compleja, considerando que debíamos unificar necesidades del negocio, comerciales, de la nueva marca, del área médica y de los pacientes/usuarios. Por lo tanto, nos propusimos hablar con todos y atender los requerimientos de cada área. Realizamos talleres de estrategia con los representantes de cada clínica; reuniones con el cliente; reuniones con la agencia de branding; sesiones de trabajo con médicos para la homologación, confección de prototipos navegables; desarrollo de un webkit y documentación de cara a la etapa de desarrollo.
5. Mantener un norte claro en todo momento
Cada vez que nos sentimos perdidos o tuvimos dudas, recurrimos a los pilares de marca y a la estrategia de la experiencia digital completa, los que estaban definidos desde el principio del proyecto.
Las decisiones de arquitectura de información se basaron en este ciclo de la experiencia.
Nos propusimos priorizar los pilares considerando la persona, la necesidad, el canal, contexto y escenario de uso. Y nos dimos cuenta que, si bien el sitio web debía hacerse cargo de los pilares de marca, el pilar de Disponibilidad era el que debía predominar. ¿Cuál es la motivación del usuario? ¿Qué necesita? Necesita solucionar su problema de salud, reservando una hora de atención lo antes posible, lo más cerca de su lugar de trabajo o residencia. Y nos encargamos de recordárselo al cliente cada vez que sentíamos que nos alejábamos de ese objetivo.
6. No traspasar al usuario el lenguaje del negocio
Esto era clave. Lo que es lógico para el negocio no tiene por qué ser lógico para el cliente, y lo que es lógico para los médicos no tiene por qué ser lógico para el paciente.
Para el negocio, los exámenes, toma de muestras, imagenología y radiología son mundos distintos; para el usuario son exámenes.
Para los médicos, las especialidades son claras, pero los usuarios ¿saben con qué especialista reservar hora según los síntomas que presentan? La mayoría de las veces no.
Continúa leyendo el resto de nuestros aprendizajes en la publicación de mi co-autora Consuelo Pizarro C.
