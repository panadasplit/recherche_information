
This week — Vladimir Putin on AI; the ethics of human augmentation; how to regulate AI; robots tax could become a reality in San Francisco; on gene therapies; and more!
H+ Weekly is a free, weekly newsletter with latest news and articles about robotics, AI and transhumanism. Subscribe now!
More than a human
The Ethics of Experimentation: Ethical Cybernetic Enhancements
Here’s a transcript from Alex Pearlman’s lecture at King’s College in London, where she argues that members of the grinder subculture who experiment on themselves with cybernetic augmentations with the goal of becoming cyborgs are performing ethically permissible, non-therapeutic enhancements that should be tolerated and even embraced by medical and regulatory institutions.
Artificial Intelligence
Vladimir Putin: Country That Leads in AI Development “Will be the Ruler of the World”
Vladimir Putin, the President of the Russian Federation, spoke on Friday on the potential of leaders in AI to use the advanced technology to rule over the world. Soon after Elon Musk tweeted that AI race could cause WWIII.
How to Regulate Artificial Intelligence
In this article, Oren Etzion from the Allen Institute for Artificial Intelligence outlines three rules to make AI behave nicely. These rules are: AI must be subject to the full gamut of laws that apply to its human operator, AI must clearly disclose that it is not human and AI cannot retain or disclose confidential information without explicit approval from the source of that information.
Where will AGI come from?
Here are slides from Andrej Karpathy’s talk from YConf where he explores the current state of AI and tries to find out where will artificial general intelligence (AGI) come from.
We Can Create a Great Future With AI If We Start Planning Now
Physicist Max Tegmark is optimistic about the future of artificial intelligence and its limitless potential. However, he believes people have a limited view of what AI truly is, and that there isn’t enough being done to ensure we’re safe from it.
Noriko Arai — Can a robot pass a university entrance exam?
Meet Todai Robot, an AI project that performed in the top 20 percent of students on the entrance exam for the University of Tokyo — without actually understanding a thing. While it’s not matriculating anytime soon, Todai Robot’s success raises alarming questions for the future of human education. How can we help kids excel at the things that humans will always do better than AI?
This Is How Google Wants to ‘Humanize’ Artificial Intelligence
In order to make AI more useful, Google launched PAIR (short for People plus AI Research) project, which goal is to find more compelling uses of AI with “focus on the ‘human side’”. The initiative also hopes to discover ways to “ensure machine learning is inclusive, so everyone can benefit from breakthroughs in AI.”. PAIR would also create AI tools and guidelines for developers that would make it easier to build AI-powered software that’s easier of troubleshooting if something goes wrong.
Robotics
Bill Gates’ Plan to Tax Robots Could Become a Reality in San Francisco
The idea behind this tax seems noble. Tax the robots that take jobs and fund basic income with it. On the other hand, the robot tax could affect the research in robotics or make the goods produced with them more expensive.
China’s blueprint to crush the US robotics industry
Not so long ago, Chinese robots were just copies of those from US. Now, China is the biggest player in robotics. All thanks to a massive push by the Chinese government to be a world leader in a number of high-tech industries, such as medical devices, aerospace equipment and robotics.
The World’s First Drone Equipped with Robotic Arms
Ready to fly over and steal your stuff.
How Flytrex launched the ‘world’s first’ urban autonomous drone delivery system
I don’t know if they are the “world first”, but if you live in Reykjavik there is a chance that your next takeaway will be delivered by a drone.
These Dancing Robots Are Breaking Records
Dance, dance, robotic revolution!
Biotechnology
Has the Era of Gene Therapy Finally Arrived?
The FDA just approved the first gene therapy for sale, but such therapies remain far from fulfilling their early promise. We still don’t know that much what side effects or unintended mutations it can cause. On top of that, the price tag for gene therapy is high. That one therapy approved by FDA, Kymriah, costs $475,000.
Lab-Grown Brain Balls Are Starting to Look More Lifelike
Sergiu Paşca, a neuroscientist at Stanford University, alongside with other researchers is growing little balls of human brain tissue, about four millimetres in diameter, from stem cells in the lab. With prompting from the right chemicals, these cultures grow into neurons and other cell types that organize themselves over weeks and months into structures that resemble actual regions of the human brain, at least to some degree. These mini-brains are then used in experiments which previously would need a full-grown human brain to conduct.
Thanks for reading this far! If you got value out of this article, it would mean a lot to me if you would click the heart-icon just below.
Every week I prepare a new issue of H+ Weekly where I share with you the most interesting news, articles and links about robotics, artificial intelligence and futuristic technologies.
If you liked it and you’d like to receive every issue directly into your inbox, just sign in to the H+ Weekly newsletter.
