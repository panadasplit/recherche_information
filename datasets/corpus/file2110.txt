CAT Percentile Predictor 2017
CAT Percentile Predictor
CAT 2017 Percentile Predictor is an online tool offered by MBAUniverse.com and some coaching centers that helps you to know what CAT 2017 score and percentile you are likely to get on basis of your performance in CAT 2017 exam. The more accurate you are in understanding your total number of attempts and correct answers for each of the CAT sections, the nearer you are to predict your CAT 2017 percentile.
How is CAT 2017 Percentile Predictor Prepared
IIMs have disclosed the process of “CAT score normalization” which is used to prepare CAT percentile on basis of raw score leading to scaled score and finally getting the shape of CAT percentile.MBAUniverse.com and other coaching center websites also use this approach.
For reporting purposes, Scaled Scores for each section (Section I: Verbal Ability and Reading Comprehension (VARC), Section II: Data Interpretation and Logical Reasoning (DILR), and Section III: Quantitative Ability (QA)) and Total along with the Percentiles shall be published. 
 The process of Normalization is an established practice for comparing candidate scores across multiple Forms and is similar to those being adopted in other large educational selection tests conducted in India, such as Graduate Aptitude Test in Engineering (GATE). For normalization across sections, we shall use the percentile equivalence.’
Readmore…

