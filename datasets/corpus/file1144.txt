Git-busters | How to make your team repository errorless in GitHub
Photo by rawpixel on Unsplash
This is the second part of the series. Please read the previous article, “Git-busters | How to set your team repository right in GitHub “
As I emphasized in the previous article, setting a GitHub is very easy and intuitive. You will almost never face any error message using GitHub on your solo work. However, your hatred toward the git rapidly haunted on you once you join one of the team GitHub repository.
Always habit yourself to pull/commit/push your works and sync with teams update.
Part 1: how to set a team repository right for the team leader
Part 2: how team join a collaborative workspace and navigate
Part 3: how to handle GitHub error messages and fix it in your team repo
Steps for the team member, collaborators
Step 1) Join your team repo by clone it directly
Click the green button on your right side
Step 2) Each members will create their own branch in a local machine.
When you successfully set your branch, you will see “On branch {your branch name}” in the first line when you typed git status
Step 3) pull down the most updated work from the remote
Before you are stacking your own codes to the collaborative files, make sure to command the pulling git.
Again, get used to check and pull down the contents before you start your work. Otherwise, you will have double extra works to fix and merge codes later.
Step 4) Make your changes and push it back to the remote
This is an important part, please make sure to push it back to the remote branch once you finish your work.
Step 5) Ask for “pull request” to merge your changes into the master branch
In your team repository GitHub page, you will find ‘pull request’ menu and you can request pull request.
Select the pull request menu
Click the green pull request button
or in the branch menu, you will see ‘new pull request’ button next to your branch name. (captured from another repository of my team project)
In the branch menu, ‘new pull request’ button is available for you.
When you make ‘pull request’, make sure to leave your comments precisely what you changed so that the reviewers and other collaborators can capture your thought process easily.
Step 6) Once your “pull request” confirmed and merged by others, you will repeat the step 3 to 5 for your future collaborative work.
What is pull request?
“Pull requests let you tell others about changes you’ve pushed to a repository on GitHub. Once a pull request is opened, you can discuss and review the potential changes with collaborators and add follow-up commits before the changes are merged into the repository.”
- About pull requests, the official GitHub document
In my own definition, a pull request is a discussion panel review platform and procedure to talk between the teams whether to accept the changes or not.
Additional source: Pull Request Tutorial
In the series of the two articles, I shared how to set the team repository and invite members to the collaborative workspace which will be working under the error-free. In the next article, I would like to share how to handle GitHub error messages and fix your problems. Let’s git-busters!
