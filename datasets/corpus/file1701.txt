
Zero UI: The End of Screen-based Interfaces and What It Means for Businesses
Zero UI: The End of Screen-based Interfaces and What It Means for Businesses
The television introduced us to the world of screens for the first time.
Today, not a minute goes by without interacting with a screen, whether it is the computer or the mobile.
Soon, however, we will be entering the era of screen-less interaction or Zero UI.
A lot of devices and applications such as Google Home, Apple Siri, and Amazon Echo are already engaging with their end-users without the need for a touchscreen.
What Is Zero UI?
In simple terms, Zero UI means interfacing with a device or application without using a touchscreen.
With the increasing proliferation of the Internet of Things (IoT), touchscreens will eventually become out-of-date.
Zero UI technology will finally allow humans to communicate with devices using natural means of communication such as voice, movements, glances, or even thoughts.
Several different devices such as smart speakers and IoT devices are already using Zero UI.
As of 2018, 16% of Americans (around 39 million) own a smart speaker with 11% having an Amazon Alexa device, while 4% possess a Google Home product.
Zero UI is fast becoming a part of everything, from making a phone call to buying groceries.

Components of Zero UI
Tech companies around the globe are using a variety of technologies to build Zero UI-based devices.
Almost all these technologies are related to IoT devices such as smart cars, smart home appliances, and smart devices at work.
Haptic Feedback
Haptic feedback provides you with a motion or vibration-based feedback.
The light vibration felt when typing a message on your smartphone is nothing but haptic feedback.
Most smartwatches and fitness devices use this technology to notify the end user.
Personalisation
Some applications also eliminate or minimise the need for a touchscreen with the help of personalisation.
Domino’s “Zero-Click Ordering App” relies on the consumer’s personalised profile to place an order without requiring a single click.
If you already have Dominos Pizza Profile and an Easy Order, the app will place your order automatically in ten seconds after opening it.
You can also open this app using your smartphone voice assistant such as Apple’s Siri.

Voice Recognition
Speaking of Siri, the voice search and command itself is a component of Zero UI.
Cortana, Google Voice, Amazon Echo, and Siri are a few examples of voice recognition-based Zero UI applications.
This technology allows a device to identify, distinguish and authenticate the voice of an individual.
That’s why it has found applications in biometric security systems.
Glanceability
Face recognition is also turning into one of the most popular Zero UI technologies.
Most laptops and computers already use this technology to unlock screens.
However, Apple’s new Face ID feature takes it to a whole new level.
With this feature, you can unlock your iPhone with just a glance.
There is no need to hold your phone to your face.
It uses infrared and visible light scans to identify your face, and it can work in a variety of conditions.
Gestures
Gesture-based interfacing is available on a variety of smart devices.
For example, Moto Actions, a gesture-based interface on Motorola smartphones, allows you to carry out tasks such as turning on the camera or flashlight without unlocking the phone.
A bit more advanced example refers to Microsoft Kinect.
You can add it to any existing Xbox 360 unit.
It uses an RGB-color VGA video camera, a depth sensor, and a multi-array microphone to detect your motion.
Messaging Interfaces
Messaging interfaces such as Google Assistant are also a part of Zero UI.
If you already have a credit card or e-wallet added to your Google account, you can ask Google Assistant to order food with few text messages instead of wading through different food ordering apps.
Similarly, you can perform several other tasks such as calling, texting, and sending out emails using the Assistant.

Brands That Are Doing It
Several brands are trying to create a Zero UI experience for their customers.
While some companies are building intuitive apps, others are making their services available on Zero UI devices such as Amazon Echo and Google Home.
Uber
The Uber Alexa skill has been available on Amazon Alexa for some time now.
Once you have installed Uber Alexa skill on your Amazon Echo and set the location of your Amazon Echo in the Uber App settings, just say “Alexa, ask Uber to book a ride” to call a ride at your doorstep.
Starbucks
Just like Uber, you can also order your favourite Starbucks drink using Starbucks Reorder Skill on Alexa.
You can use the Google Assistant on your phone to order beverages and snacks from the Starbucks menu.
You will need to link your Starbucks account and select a payment method before making the first purchase though.

Apple’s HomeKit
Apple’s HomeKit allows you to control smart home appliances using your iPhone.
With a simple voice command, you can regulate the thermostat, or turn the lights on/off.
CapitalOne
The banking sector is also taking advantage of Zero UI.
CapitalOne, one of the leading banks in the US, also provides banking service through Amazon Alexa.
Registered users can check their bank balance or transfer money with voice commands.
CBS
CBS provides the latest news from all over the globe through Amazon Alexa.
You can also access CBS TV shows, movies, and other programs using this feature.
How Will Zero Ul Affect Web Design?
Although Zero UI may seem like the end of visual interfaces, that’s unlikely to happen.
Humans are visual beings.
We can retain visual information better and longer.
So, Zero UI is not going to eliminate screens.
However, it is going to change the present concept of web design forever.
Contactless management and predictive thinking are going to be the pillars of Zero UI design.
The present web design, being two dimensional, is mostly built on linear sequences.
For example, voice search is often carried out using simple voice commands such as “Call my father” or “Call an Uber” or “Tell me the score of yesterday’s Knicks game.”
However, putting them all together “Call my father then an Uber and then tell me the Knicks game score last night” will probably render your voice search query useless.
Web designing will have to evolve to handle such complex nature of human conversation.

What This Means for Designers — Understanding Data and AI
Zero UI brings the search and the purchase history (or behavioural data), two critical components of digital marketing, closer to each other.
In other words, designers will need to design systems that are intelligent enough to contemplate and create the content you need.
This, in turn, means, as a designer, you will need a thorough understanding of data analytics and Artificial Intelligence (AI).
You will need to design the UI for interaction with the device, not just the screen as it will not be limited to the smartphones or computers.
For example, let’s say you have to build a thermostat that can sense gestures.
However, there are dozens of ways a user will tell it to increase the temperature.
That’s where data analytics and AI comes in.
The more you know about your consumers’ psychology and behavioural patterns, the easier it becomes to design a Zero UI device.
What Zero Ul Will Mean for Businesses
In a world of Zero UI, your business will rely on providing the right recommendations at the right time.
You can’t afford to wait for the customer to initiate searches themselves.
Your business needs proactive inspiration.
Data
You are probably already collecting tons of data for your business.
You will need to continue gathering this information for Zero UI development as well.
However, the new extended data structure will require various technologies including machine learning, AI, IoT, data analytics, and the Blockchain to work together.
As a result, it will mostly be collected and controlled by tech giants such as Amazon and Google.
So, small and medium businesses will have to form strategic alliances or register themselves with these brands to get this data.
Context
You will need to understand the meaning of what users are attempting to find.
Most people start their search with a simple term, but they want more information about it.
For example, if a user frequently orders Chinese takeout, his query for “restaurants near me” should list Chinese restaurants at the top.
This is automatically inferred context.
So, whenever someone uses a phrase related to your product or service, your system should automatically initiate the next steps to guide them to your brand.
Design
As mentioned earlier, your business will have to move away from the linear web designing process.
Your prospects can take any of the channels in the extended data structure to reach your brand.
Your system must be ready to gauge most of this incoming traffic.
For example, a query for “nearest pizza shop” can come from a smart vehicle or a smartphone or even a smart home.
Can your web design handle it?
Content
The content creation process will also become more dynamic than ever.
You will need to produce content in conjunction with the changes in the consumer data.
Local SEO will play a crucial role in Zero UI as most people will be searching for places and services in a given area.
For example, you can think of promoting location-based deals to attract more foot traffic to your store.
With voice search taking over text, natural language search will also take precedence in your SEO.

How to Optimise Your Site for Natural Language Search?
When it comes to natural language search, voice search is the most prevalent way of communication.
Nearly 70% of requests on Google Assistant are natural language searches, and not the typical keywords people type in a web search.
Optimising your website for this type of search is the first step towards creating a Zero UI experience for your prospects.
Structure Your Content
Usually, natural league queries are in the form of questions instead of phrases.
If you are looking for a burger shop, you will ask “Where is the best burger shop in Chicago?”
According to SEO Clarity, “how,” “what,” and “best” are the top three keywords used in these search terms.
So, you need to optimise your website content for these keywords.
Try to include them in your blogs, articles, and Q&A pages naturally.

Use Long-Tail Keywords
Conversational phrases also comprise long-tail keywords as people often use more words when they are talking.
So, you will also need to optimise your website content for longer and more conversational phrases and keywords.
While you are at it, try to maintain natural language throughout your content marketing efforts.
Add your Website/Store to Google My Business
Adding your website to Google My Business (GMB) helps attract more foot traffic through mobile searches, especially the “near me” searches.
Add your phone number, physical address, business hours, user reviews, and store or office location to GMB.
Zero UI and the Problem of Privacy
In its attempt to provide users with a seamless experience, Zero UI may also lead to serious privacy and security issues.
The ability to order an Uber or a pizza from your Amazon Alexa may seem like a magical experience, but that requires sharing your personal information such as credit card details with not only Amazon but also third-party sites.
Meanwhile, IoT devices are providing cybercriminals with new ways to steal personal data.
On October 12, 2016, Mirai, a self-propagating botnet virus brought down the internet service on the US east coast.
The virus infected poorly protected internet devices. Similar attacks are taking place as we speak.
However, using smart home appliances can also lead to the invasion of your privacy.
Recently, Amazon Echo recorded a family’s private conversation and sent it to a random individual in their contact list.
The victim, a Portland, Oregon, resident felt so insecure that she unplugged all the devices in her house.
Though rare, incidents such as this raise some serious privacy and security concerns.

Future of Zero UI
At the moment, the future of Zero UI looks bright despite the security and privacy concerns.
In the coming years, it will take the smart home and smart city concept to the next level.
For example, planning a trip to a shopping mall in 2030 will require you say the magic words “Plan a trip to the XYZ mall tomorrow at 7 PM” to Google Home or Amazon Alexa.
The system will take care of everything from planning a travel route to paying for the car parking in advance.
Coupled with AI and advanced data analytics, Zero UI devices will be able to form an empathetic and a personalised relationship with your consumers.
As human-machine interactions become more natural and human-like, it will open new opportunities for digital marketers.
In Conclusion
The Zero UI concept aims to make every community, marketplace, on-demand service, e-commerce site, and mobile application more interactive.
However, this is going to open new doors for marketers and designers alike.
Hopefully, this comprehensive coverage of the said topic will prove helpful in understanding its magnitude, consequences, and the opportunities it will create.
If you still have doubts, let us know in the comment section. We will get back to you as soon as possible.
Author Bio: I am the President and Founder of E2M Solutions Inc, a San Diego Based Digital Agency that specialises in White Label Services for Website Design & Development and eCommerce SEO. With over ten years of experience in the Technology and Digital Marketing industry, I am passionate about helping online businesses to take their branding to the next level.
If you wish to discuss how we can develop your brand or provide graphic design for your product or business, email us: hello@inkbotdesign.com
Inkbot Design is a Creative Branding Agency that is passionate about effective Graphic Design, Brand Identity, Logos and Web Design.
T: @inkbotdesign F: /inkbotdesign
Originally published at https://inkbotdesign.com/zero-ui/ on August 9, 2018.
