Five tech trends that shaped 2017
“The historian is a prophet looking backwards.” ― Friedrich Schlegel

This post was originally published on VC Cafe. As we approach the last stretch of 2017, I wanted to take stock of the tech trends that shaped our year. In the next post, I’ll cover my predictions for 2018.
1. Decentralisation
Perhaps the most impactful trend this year is the proliferation of Blockchain technologies and cryptocurrencies into the mainstream.
On the Blockchain front we’ve seen a wide array of potential applications from real estate to art dealing and diamond trade.
On Crypto, we moved from Voice Over IP to Money Over IP, and saw Bitcoin cross the $10,000 line. ICOs (initial coin offerings) became a ‘thing’ — tokenise everything, with people spending over $1M to buy virtual cats with Ethereum on CryptoKitties (it’s been acquired since).
For a second it looked like White Papers were replacing the fundraising deck, with startups that struggled raising traditional funding completing multi millions ICOs seemingly overnight. Unfortunately, a large percent of ICOs feel like a potential scam, money gets taken off the table quickly and almost with no supervision, and with the only collateral at risk being reputation (in some cases, not even that).

This is just the beginning in my opinion, but regulation is likely to step in here very soon.

2. AI is the new UI
The hype around AI reached new heights in 2017. Using a decision tree to apply a set of rules, or operating a chatbot don’t necessarily qualify as using AI, but it is almost inevitable to avoid having some form of machine learning, deep leaning, NLP etc today’s tech startups.
As a field, AI made major breakthroughs this year, namely DeepMind’s AlphaGo decisive victory over the Go world champion, and then the improved version AlphaGo Zero, which was self taught and even better.

There’s no doubt that AI will continue to penetrate entire industries, in particular Automotive (self driving vehicles), robotics, drones, healthcare and marketing tech — from advertising to customer service.
Another aspect of the rise of AI is the infrastructure side: new chips from Nvidia, Google and Graphcore to fuel our growing need for fast data processing.
The Artificial Intelligence Index 2017, a ‪Stanford report by AI Index (pdf) has some fantastic nuggets on the number of AI academic papers published, the number of enrolled students into AI courses, the growth rate of AI startups etc.

3. Data is the new oil
There’s one big problem with the perception of data being the new oil, the CEO of a successful AI startup told me. Large corporates are sure they are sitting on an oil field, and so spend millions to pour their data over to expensive data lakes, only to find that’s it’s hard to refine that crude oil (took the analogy all the way, I guess). Organisations are simply ‘sitting’ on their data, or paying for unproven expensive solutions. We are producing more data than ever in human history, and are getting better at understanding the patterns and the meaning of that data, but there’s still a lot of friction in getting that data and using it wisely.
For example, researchers can now predict the face of person based on a tiny sample of DNA. We are able to predict what customers will churn or upgrade simply by watching a small sample of their behaviour, and soon, we should be able to predict where/when a crime is about to happen, by applying models to surveillance data and past crime statistics.
Where does the line cross? Ethical considerations are becoming a major part of big data and machine learning startups, with several companies and industry bodies formed to tackle these questions.
4. Cyber is here to stay
Almost no weeks go by without the headline of a major hack. It seems like the Cyber security industry will only get bigger with more and more devices getting online, from our cars to our appliances.
We saw the rise of ‘Dark Marketing’, where advertisers are able to target individuals based on increasingly granular attributes (including race, religion, beliefs) and as Prof Scott Galloway said, “weaponise Facebook” as a platform to change public opinion.
Israeli startups attracted about 20% of the global funding for the security sector and saw the IPO of ForScout, reaching an $897M market cap.
5. GAFAM
5 companies now dominate tech (Google, Apple, Facebook, Amazon and Microsoft), or 7 if you add Alibaba and Tencent. Their power in the market is almost absolute for example, 99% of digital advertising growth is going to Facebook and Google. Just look at the size of Amazon compared to ALL OF RETAIL.

Their power is creating a public backlash — calling for tighter regulation on these companies dealings with privacy, data transparency and competition scrutiny.
It’s also getting increasingly hard to find a niche to compete with these giants, as they expand into to every major area from Cloud, messaging, hardware, enterprise, etc, adopting an AI first strategy. As an example, take a look at everything that Amazon announced at AWS re: Invent 2017.

In my next posts I will cover additional trends that dominated 2017, including Fake News, the Seed Slump, digital health, etc as well as some predictions for 2018. In the meanwhile, take a moment to sign up to my newsletter.
