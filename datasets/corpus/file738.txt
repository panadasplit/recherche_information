
We’re looking for Growth Heroes 🚀
Want to work with a fast growing enterprise SaaS company and help grow the next tech unicorn? Keep reading to join Morph.ai growth team!
About You
You love talking. Talking to people will be the essence of what you do here. On calls, through mail, in-person meeting, events, articles, advertisements whatever you do, you’ll be in conversation. An indeed that’s what Morph.ai is all about, conversations through chatbots!
You have been talking all your life. You have experience with public speaking at school or college. You have been in a role that involves extensive customer interaction in your previous jobs, if any. You know the art of impressing others, by your personality and by your wit. Experience in digital marketing is a big plus to have. You’ll be closely working the rest of the business and product team, so being a team player is a must.
You are a hustler. You are faster than Flash when it comes to getting things done. You don’t mind moving around for events and meetings across cities and countries. You are tech savvy and smart to answer impromptu questions thrown at you in a meeting. Being a deep-tech company you need to thoroughly understand what you are selling. You can get into the shoes of your users and ease out their problems and help them when they need.
And most importantly You love a challenge. We are a small team doing big things and working with large clients. So challenges are a routine excitement at Morph.ai.
About Morph.ai
Founded in 2016, Morph.ai is a chatbot based marketing platform, which helps businesses have personalised conversations with their potential customers to convert them.
Using Morph.ai, businesses can have one-on-one conversations with their audience to increase awareness, improve lead quality and drive more sales. We have set out to revolutionize the marketing and sales process by leveraging messaging as the most active channel of engagement. We strongly believe that chat is the perfect channel of interaction between businesses and customers and it would soon emerge as the strongest medium for marketing and selling products and services.
Today, Morph.ai is one of the fastest growing AI companies of the country, working with brands like Manchester City F.C., Yes Bank, Estée Lauder and many more.
Morph.ai has received a number of recognitions in a short time
Things You’ll Do
Morph.ai is a startup, so you might need to wear many hats when required. That said, here are some things you’ll probably get your hands on:
Help sell and market Morph.ai’s awesome chatbot marketing platform.
Manage interactions with Enterprise accounts to be their single point of contact.
Analyse the market opportunities and build business strategies for partnerships and expansion.
Help customers via support to ensure they have the best experience possible.
Help product team test and ensure that everything is working as desired and the customers love what they see.





Apply
Ready to apply? Just fill this form and we will get in touch with you ASAP.
