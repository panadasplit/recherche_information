Artificial Intelligence APAC — Issue #11
If you like this newsletter, please send it to a friend so they can subscribe too.

Come 26–27 June 2018, the upper echelons of the tech world will gather in Hong Kong for the 3rd annual edition of this now staple of MIT Technology Review in Asia — EmTech Hong Kong 2018. This year, the conference program will focus on 4 key topics: Future Cities, Rewriting Life, FinTech and Cybersecurity and Intelligent Machines. If these topics are of your interest, this is an event you shouldn’t miss!
Enter Artificial Intelligence HK Discount Code: ETHK8001 at Check-out to Enjoy 10% off
Industry News

24 EU countries sign AI pact in bid to compete with US and China — www.euractiv.com 
 Twenty-four EU countries pledged to band together to form a “European approach” to artificial intelligence in a bid to compete with American and Asian tech giants.

Facebook Is Forming a Team to Design Its Own Chips — www.bloomberg.com 
 Facebook Inc. is building a team to design its own semiconductors, adding to a trend among technology companies to supply themselves and lower their dependence on chipmakers such as Intel Corp. and Qualcomm Inc., according to job listings and people familiar with the matter.

Alibaba is developing its Own AI chips, too — www.technologyreview.com 
 Alibaba announced that it’s building a chip called Ali-NPU — for “neural processing unit” — designed to handle AI tasks like image and video analysis. The firm says its performance will be 10 times that of a CPU or GPU performing the same task. The project is led by Alibaba’s R&D arm, DAMO Academy. The firm also announced that it has acquired a Hangzhou-based CPU designer called C-SKY.

Microsoft to improve shipping operations with AI — techwireasia.com 
 MICROSOFT Research Asia (MSRA), is partnering with Orient Overseas Container Line Limited (OOCL) to use artificial intelligence (AI) within the shipping industry. OOCL aims to optimize its shipping network operations, with the help of deep learning research. The partnership will see both companies jointly conducting
Financial Services

AI will wipe out half the Banking jobs in a decade, experts say — www.mercurynews.com 
 Artificial intelligence will wipe out half the banking jobs in a decade, experts say
For AI Engineers

Training Drone Image Models with Grand Theft Auto — www.slideshare.net 
 CCRi Data Scientist Monica Rajendiran’s presentation from Charlottesville’s 2018 #tomtomfest Machine Learning conference.
Evolved Policy Gradients
blog.openai.com
Around Asia-Pacific Region

China’s AI dream is well on its way to becoming a reality — www.scmp.com 
 Andy Chun says China seems to have all the pieces in place to achieve the goals of its artificial intelligence strategic road map — from a vibrant start-up culture to government support and a population enthusiastic about technology

Huawei working on an Emotional AI — www.cnbc.com 
 Huawei is working on an emotion AI that would make conversations between users and virtual assistants more meaningful, the company’s executives told CNBC.

Japan: Robots are going to redefine Japan’s skylines — www.technologyreview.com 
 Japanese companies are turning to robots to help build their skyscrapers.

India: Twitter Co-Founder Invests In Indian AI-Based Health Startup Visit — analyticsindiamag.com 
 “In India, for every doctor there are 2,000 patients lined up in-clinics… This is where the technology approach by Visit comes in”

Thailand commits to ‘long-term partnership’ with Alibaba — www.nationmultimedia.com 
 Thailand has entered into a strategic partnership with Alibaba to drive the development of Thailand’s digital economy and the Eastern Economic Corridor under the Thailand 4.0 policy.
UK Plans $1.3 Billion Artificial Intelligence Push
fortune.com 
 The United Kingdom is planning a $1.3 billion investment into artificial intelligence technologies, with companies like Microsoft helping.

Vietnam: Robotics In Vietnam — jumpstartmag.com 
 By Keina Chiu | Fluctuating Variances in production volume and tight deadlines are issues that SMEs often face. While robots could be the ultimate solution,

These 3 countries are more prepared for automation than anyone else, here’s why — www.techrepublic.com 
 A study found that every country is grappling with how to respond to AI and automation, writing that “societies are…in for a long period of trial and error.”
Join AI Meetup Events in Hong Kong!

Join Hong Kong AI Meetup:
https://www.meetup.com/Artificial-Intelligence-HK
Brought to you by:
Artificial Intelligence & Deep Learning Ltd
https://www.linkedin.com/in/EugeniaWan/
Follow us on Facebook!
https://www.facebook.com/ArtificialIntelligenceAPAC
[ YOUR LOGO HERE ] — email enquiry@DeepLearning.ML

The above articles represent the authors’ own opinions or the respective news organizations’ and do not reflect the views of Eugenia Wan, Artificial Intelligence & Deep Learning Ltd nor eFusion Capital Ltd etc.

