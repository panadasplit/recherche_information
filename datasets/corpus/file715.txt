Image — Ramdas Ware
Why The Next AI Winter Will Not Happen
Since the inception of Artificial Intelligence (AI) as a computer science in 1956, there have been a number of serious AI winters — periods in which AI funding, hence research and development stalled, based on disappointing results and lack of progress in the field.
Some people warn that a next AI winter is imminent. I don’t believe so.
Why a next AI winter
Almost all of the widely publicized impressive breakthroughs in Artificial Intelligence of the last few years — roughly since 2012 with application of GPUs and large data sets — are based on one specific type of AI algorithm: Deep Learning, an application of Machine Learning.
Not the least of AI scientists, Geoff Hinton — sometimes dubbed the “father of Deep Learning” — has openly cast doubt on the broad applicability of Deep Learning. Hinton has doubts about one specific technique mostly used in Convolutional Neural Networks, called ‘back propagation’. In Hinton’s words: “I don’t think [back propagation] is how the brain works.”
Besides, even with the fastest hardware configurations and largest data sets in the world Machine Learning has not overcome some fundamental mathematical flaws, such as its sensitivity for adversarial examples and eg. the risk of overfitting or being stuck in a local minimum.
There still are obstacles to overcome for Machine Learning to become mainstream.
There are many well documented (eg. by Gary Marcus of NYU or Ion Stoica et al. of UC Berkeley) obstacles to overcome for Machine Learning, before it will mature into a technology that will be applicable in business as easily as electricity.
Roughly summarized, specific AI algorithms are a ‘one trick pony’, hardly transferable into other application fields (‘transfer learning’). That is why it is called Artificial Narrow Intelligence, after all. [The reason for this is simple: the structure — the ‘weights’ and ‘biases’ in the mathematics to be precise — is tailored to the specific dataset used for training the neural network. Different dataset, different structure.]

And there still are a lot of technical — and organizational — obstacles to overcome when you actually build a working, useful AI solution, eg. getting your hand on huge amounts of suitable data (cleansed and feature engineered). Not to speak of the ethical discussions esp. in consumer applications on eg. explainable AI, use of biased datasets and privacy.
The AI hype currently is probably at the peak of inflated expectations
Last but not least, AI — like every overhyped new technology wave — will follow the Gartner hype cycle. AI now is probably at the ‘peak of inflated expectations’. So the next few years, it will inevitably go through the ‘trough of disillusionment’.
And yet …
Why not a next AI winter
Simply said, the floodgates are open … for good.
First of all, many of the doomsday stories for AI refer to Artificial General Intelligence, systems as smart as humans. These predictions are right, in my opinion. No Terminator, no Ex Machina. And forget about Sophia. There will be no Artificial General Intelligence any time soon, despite existing singularity theories.
The floodgates are open … for good!
And some say, Hinton’s rejection of back propagation was merely a publicity stunt to promote another technique, the so called capsule networks. Maybe, maybe not. Besides, Hinton speaks of simulating the brain. That is not necessarily the purpose of Machine Learning.
But we must not throw the baby out with the bathwater. Artificial Narrow Intelligence is here, it works, and it is here to stay. And it will improve … dramatically.
Why? Well. There is one all-determining difference between the past AI winters and the situation today.
Investments
Too much money has already been invested by big corporates, big venture capitalists and big governments to simply accept depreciation of those investments. That is just not going to happen. We’re over the threshold. There is and will be tremendous pressure for results. It’s just how it works.
Too much money has been poored into AI to accept depreciation of such investments.
And it is widely expected, that investments in AI will dramatically increase in the coming years, from the 2017 estimated US$ 5 Billion (startups) up to some say US$ 37 Billion or more in 2025. And don’t forget, the main reason for the AI winters was the lack of funding.
So who is funding AI today? Well, practically everybody! That’s another reason why it won’t stop any time soon.
The main reason for the AI winters was a lack of funding.
Of course there are the US tech giants, Google, Amazon, Facebook, Apple who claim to be an AI First company. Not to forget IBM, Microsoft and the likes. Billions of investments in AI. And companies like Intel and Nvidia, for example. But don’t forget Chinese big tech, eg. Alibaba, TenCent, Baidu, Huawei. Alibaba announced to invest US$ 15 Billion in AI and related technologies such as quantum computing. [Alibaba recently developed an AI that is better at reading a specific dataset than humans.]
But also, all vertical industries are investing heavily in AI, eg. the automotive industry, running for the gauntlet of the autonomous vehicle. Financial services invest heavily in applications like algorithmic trading and credit card fraud prevention. Even agrobusiness is experimenting largely with precision agriculture.
But not just companies. The Chinese government announced in the summer of 2017 their very serious ambition of world domination in AI in 2035. Recently plans were announced for a US$ 2 Billion AI research park in the Mentougou area, west of Beijing. China is already implementing a nation-wide system of AI-powered face recognition.
And so on, and so on. The list is endless.
No more one trick pony
All of today’s AI that really works is Narrow AI. Each individual AI solution (artificial neural network) can only be applied to solve one specific problem (dataset). For each new problem you have to build a new solution. Well, things are changing. A lot of research is done in the field of Transfer Learning. And the first significant results are emerging.
Researchers of Google’s Deepmind have developed AlphaZero — successor of AlphaGo Zero, which was the successor of AlphaGo, which defeated the world champion of Go. AlphaZero not only learned itself to play Go, but also the games of Chess and Shogi at superhuman level, in a matter of hours, without any prior knowledge of those games. And without significant changes to the algorithm.
In the scientific community there is broad consensus that this is a major step forward. At the same time, this is still far away from any form of General AI. Eg. the games of Go, Chess and Shogi are very bounded, regulated environments with a straightforward goal (how to win). So rather, it is a form of Narrow AI+, slightly less narrow than Narrow AI. But still, it is a fundamental step forward.

Other factors
Besides capital and transfer learning, there are five other important factors pushing AI development. These are: fast hardware, big data, smart algorithms, continued research and talented people.
Hardware — Even — as some expect — if Moore’s Law will no longer be tenable in the near future, developments in parallel computing (GPUs) and quantum computing look promising enough to assert that hardware — or computing power, for that matter — will not become the limiting factor. Also, new algorithms like One Shot Learning and many other are being experimented with, promising lower requirements of processing power and data quantities.
Data — We will drown in data — we already do. Techniques for data cleansing and feature engineering are improving rapidly. The trick here will be to collect enough — labeled — data for your specific AI algorithm from your specific application area.
Algorithms & Research — When doubting the broad applicability of back propagation, Geoff Hinton said: “To push materially ahead, entirely new methods [=algorithms] will probably have to be invented. (…) The future depends on some graduate student who is deeply suspicious of everything I have said.”
That suspicious graduate student is very likely working hard at this moment — eg. in the field of transfer learning — to setup her AI startup company. I know from my own experience with the scientific AI community in my home country, the Netherlands, that a lot of innovative AI algorithmic work is being done. New successful algorithms will be invented, for sure. And that graduate student may well be Chinese, by the way.
Besides, most of the successful AI algorithms today are based on Supervised Learning, but there are many other families of algorithms that can and will be explored. Think of combinations of algorithms, such as used by Google Deepmind in AlphaZero. And combinations with other technologies, for that matter. Just think of Decentralized AI, eg. combining Blockchain and AI as the foundation of DAOs, Decentralized Autonomous Organizations (eg. Terra0.org) or in decentralized learning with device-centric AI on users’ smartphones and other devices, eg. through Google’s Federated Learning.
Talent — Maybe the biggest roadblock the coming years will be the limited supply of talented people, both technical (AI engineers, data scientists) and non-technical (eg. product managers, change managers, legal & compliance experts, innovative business managers) to implement AI in companies and organizations.
Major limiting factor
Current estimates are that globally there are about 10,000 in-depth technical AI experts, 300,000 AI ‘practitioners and researchers’ and already millions of roles available for people with AI and AI related skills.
It may sound odd, but I do not worry so much about the supply of AI scientists. You do not need millions of scientists globally, you need, let’s say, 10,000 or so very, very good scientists globally to create real breakthroughs in research and development.
If there will be any slowdown of AI, it will be from slow adoption and lack of AI pioneers.
I have more worries about the supply of the practitioners, the (technical) engineers and non-technical AI related experts in companies and organizations, because implementing and working with AI solutions has a totally different dynamic than working with ‘traditional’ IT and digital solutions. And you will need millions of them, globally tens of millions.
If there will be any factor slowing down adoption of AI in organizations (and society for that matter), it will be resistance to change (fear of job loss, need for retraining), slow adoption and lack of technical and non-technical AI pioneers, who will show the way to their colleagues in their respective companies and organizations.
But that will not cause an AI winter, at worst it will cause a prolongued AI spring.
Case Greenfield
. . .
Thank you for reading my post. Here at Medium and at LinkedIn I regularly write about management, work and life with Artificial Intelligence. If you would like to read my future posts then simply join my network here or click ‘Follow’. Also feel free to connect on Twitter.
Follow, Clap or Share if you appreciate this topic.
Published earlier on LinkedIn: https://www.linkedin.com/pulse/why-next-ai-winter-happen-kees-groeneveld/
