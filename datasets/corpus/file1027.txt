2018 Goals Monthly Review: January ed.
A sedate start.
Well, a lot of promises made and goals set. Did I make progress? Let’s see.
Tl; dr -
A summarised version of this post.
Link: https://docs.google.com/spreadsheets/d/1UEYtJzx-SUdpHdxKeW2OZQYZLOQTCh4z6iZblB1eCg0/edit?usp=sharing
Although the screenshot says the gist, I’d like to expand on three aspects.
Movies
While I planned to skip watching movies until July, I broke this resolution a couple of days back when I watched the movie AlphaGo (The story about Google’s AI engine beating the world champion Lee Sedol in Go) on Netflix. I’ve seen people lauding this movie recently and I decided to watch it and boy it was epic. The narration and screenplay of the movie mirrored real-life events and it was very emotional to say the least. In the man vs the machine battle, although the supporters of ML/AI wanted the machine to win, most of us deep down craved for the man to come out on the top. The moment where Lee resigns in the second game, slips a white stone on the board, his fingers still shivering with a feeling of disbelief on his face was perfectly captured. I thoroughly enjoyed the movie, partly because I could really connect to the algorithms etc., but I highly recommend this documentary to everyone to know the enormity of AI’s advancements and human creativity involved in creating this AI.

The big moment in the second game.
Book of the month
A Thousand Splendid Suns
I started the book named “The power of moments” by Chip & Dan Heath after a lot of recommendations and its immediate usability in my life. While the key concepts, about creating memorable moments to elevate people, are quite well explained, I realised I was forcing myself to read through it and it wasn’t an enjoyable read. I decided to chuck the book and picked up The thousand Splendid Suns by the famous Khaled Hosseini.
This book spans a 40-year story about two Afghani women, the kind of lives they live, the grief and oppression they go through and the hope that gets them through the day. Their grit and determination put a lot of things in perspective for most of us, in this materialistic world. The things we take for granted such as having two meals in a day, a peaceful home to live in, access to study, freedom to express love – they had none of these and when this happens, humans go through an enormous amount of grief and they start valuing small things in life. Often these people are stronger than we think. Mariam and Laila have left a strong impact on me. There’s a passage in the book where Laila and Mariam’s eyes are transfixed at each other and all they could manage is a smile. It was such a satisfying moment to read through. Also, the letter. Oh, it was heartbreakingly emotional. Tears rolled over in an instant. With that said, I’d like to read more of Khaled sometime later this year.
The 1TB RAM server for Kaggle
Rohan and I teamed up for the grocery forecasting kaggle challenge. Though we started in the last 10 days of the competition, we made good progress in the last week and we pushed hard to build really good individual models. It was the first time I was using servers of size 1TB RAM and 180 cores of CPU power. We believed we had a pretty strong validation framework and we ended up at 90th /1400 participants on public leaderboard on the last day. I woke up at 5:30AM to see final standings and I was shocked to see us fall by 530 places on private leaderboard. Later when we checked, we realised some of our initial models would have done better but we missed a couple of tricks that put us behind. It was a sad ending to a weeklong mania at my place. Anyways, lessons learnt and we’ll surely come back stronger.
Our ensemble scored way lesser than single models. XGB really overfitted in comparison to LGB, inline with many other competitors.
Overall, I think it’s a slow start to an exciting 2018 and there’s a ton of good work to be done. See you next month with a bunch of updates. Stay tuned…
The original goals post is available here: https://medium.com/@phanisrikanth33/my-2018-goals-80a3d70fd213
February post: https://medium.com/@phanisrikanth33/2018-goals-monthly-review-february-ed-f3d68ee83039
