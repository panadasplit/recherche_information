Screenplay Analytics Using ParallelDots
When I started my part-time PhD in 2006 I was interested in how analytics could help writers of screenplays to ask more questions of their script.
While there were a growing number of online and offline tools to help you write a screenplay, like Celtx and Final Draft, there were very few applications that helped you to analyze them. One of the few applications that did, Sophocles, simply disappeared soon after I started (where are you now Tim Sheehan?).
I was not interested in the primary ‘use case’ for screenplay analytics: Predicting if a screenplay would be a blockbuster and other kinds of ‘slush-pile’ filtering. Stuff that companies like Vault and StoryFit are now doing commercially. Even though I understood then that this use case is where the real money is and what the few calls I got from LA were actually interested in.
No. I wanted to understand more about my own screenplays to help me make more informed decisions. The basic use case for all analytics. I wanted to be told stuff about my own screenplays, so that maybe I would learn something and this could help me to rewrite and improve my script.
The first challenge was relatively simple: to turn a screenplay (unstructured data) into a structured data set. This was not so hard because a properly formatted screenplay is already structured to some extent with scene headings, character names, action and dialog, transition instructions etc. And it became even easier when Final Draft introduced their .FDX file format that uses XML to tag a wide range of screenplay entities in the text.
So now I had a data source as a basis for my analytics: Final Draft FDX files. By importing and ‘shredding’ these files into a MySQL database I had the basis for a rudimentary screenplay analytics application that surfaced in various forms over the years including at Viziscript.com. There’s lots of charts and basic analysis of screenplay content as standard in Viziscript but here I wanted to test out some of the text analytics provided via the ParallelDots (PD) application programming interfaces (APIs).
Eventually I will simply build-in the API calls from Viziscript to something like PD but for the purposes of this article I just used PD’s online text analytics demo page. To test PD, I used content from one of my own scripts that is a 60 page WW2 drama similar to ‘The Triple Echo’ by H.E. Bates which was filmed and starred Glenda Jackson and Oliver Reed.
Sentiment Analysis
Viziscript already includes sentiment analysis of character dialog but I wanted to see what the PD API does. Luckily this is easy to do from Viziscript as you can download all the dialog for a specific character as text and then submit it to PD via their online demo page. I wanted to compare the sentiment analysis of the protagonist’s dialog vs. the antagonist’s dialog from my script. The result — for me at least as the scriptwriter — was interesting. Both came out with a negative sentiment result, which was not exactly what I had expected or hoped for.
My protagonist’s dialog text returned 51.8% negative.

My antagonist’s dialog text returned 45.2% negative.

So what do I get from that? Maybe, based on their dialog only (but not their actions in the script) my protagonist is not positive enough and my antagonist is not negative enough. Some food for thought for a revision.
Emotion Analysis
Emotion analysis is a little more granular than overall sentiment analysis so how did my protagonist and antagonist dialog rate here?
It appears that my protagonist is angry based on this analysis of her dialog. I can understand that analysis but it’s not perhaps what I would like a script reader to come away with.

Unfortunately, my antagonist appears to be largely happy, which again is not quite what I expected. More food for thought.

Keyword Extractor
I then submitted my protagonist dialog to the keyword extractor and got this result:

In this context, keyword analysis does not tell me a lot even though it indicates my script could be something about pilots and flying. Obviously this analysis API is more likely to be helpful with analyzing say a technical article rather than a screenplay. However, the analysis of phrases is probably more helpful here in terms of ‘situating’ the storyworld of my screenplay.

The next PD API — named entity analysis — is not especially relevant to screenplays or to my screenplay in particular, so I skipped this.
Text Classification (Taxonomy)
Again this API is not that helpful for screenplay analytics but could help you to understand better what genre your screenplay fits into.

Based on my antagonist’s dialog the top-rated society/dating ‘tag’ is relevant to my screenplay since this script is a ‘social’ drama with two ‘romantic’ storylines.
I also skipped the semantic analysis API that is used to compare two texts and to help to cluster texts by relatedness. However, this API could be very useful for the following kinds of screenplay analysis:
Comparing sequels (e.g. The Godfather to The Godfather II)
Clustering screenplays from a corpus as a technique within genre analysis

The intent analysis API is also not that useful for screenplay analytics given that the range of ‘intents’ that the engine has been trained to identify: News, feedback, query, marketing and spam. But it’s easy to imagine that if the engine had been trained to identify other types of intent possibly ‘hidden’ in dialog text then this could be an interesting approach for role analytics.
Abusive Content Analyzer
This API will help you to confirm if your screenplay is likely to be rated ‘R’ or needing ‘parental guidance’. In reality, the likelihood is that if you are the writer then you are probably clear that your screenplay does or does not include ‘abusive’ content.
In this case, the PD engine came up with the correct answer from my antagonist’s dialog. Overall, my screenplay has no profanity in it and relatively mild sex and violence and this is reflected even in the dialog of my antagonist. So I’m happy with this non-abusive analysis.

Syntax Analysis
This API splits text submitted into the relevant parts of speech (POS) as the (partial) screenshot below indicates based on my antagonist’s dialog:

In screenplay analytics, POS analysis is interesting for at least two use cases:
Analyzing action content by extracting the most commonly used verbs within the script as a whole
2. Analyzing action content for nouns
Why?
Because action verbs give some indication of how ‘actiony’ the script is. The nouns often reflect the need for a production entity — namely ‘props’ — which helps with production script breakdown. Viziscript already does this noun and verb analysis of your script and can also produce word clouds from both.
The language detection API was also not used as most scripts are not multi lingual so this analysis would have minimal use.
Here, I’ve tried to show how some text analysis APIs, in this case from ParallelDots, may help you to ask more questions of your screenplay. I don’t have any commercial relationship with ParallelDots and there are many other APIs out there that you can try for textual analysis of your screenplay content. You should also bear in mind that the PD engines and APIs are not trained specifically for screenplay analytics but for use cases you might expect — the analysis of tweets, emails, chat messages and other typical online content data streams that are textual in nature.
For many of you reading this, including screenwriters, screenplay analytics may simply elicit a ‘so what’ yawn. But as the relentless march of analytics generally and machine learning specifically moves on, even screenwriters may not be immune from the application of programmatic analysis to their works as part of the commercial pipeline or supply chain for screenplays. It may be some time before this kind of quantitative analytics is enhanced with more useful qualitative analytics but that’s only a matter of time.
