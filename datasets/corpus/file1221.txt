Data Science Foundations: Introduction

As I am preparing for my upcoming MSc in Data Science at The University of Edinburgh, I am browsing the web for useful resources regarding Data Science. Until this point, I have successfully participated in more than 50 MOOCs (online courses) and read hundreds of articles. The fact that so many people share their knowledge online is astonishing.
Unfortunately, due to the enormous mass of information, many people who want to get into Data Science are becoming disoriented and or confused — Let’s say I’ve been there before 😅
I made the decision to begin these series with the purpose of compiling a well-curated list of resources, examples, cheatsheets and code snippets that will allow anybody start applying Data Science to solve everyday problems. The resources that I am going to propose have been either tried by me or have heavily been recommended by people in my environment.
The series “Data Science Foundations”, will consist of the topics below:
Introduction
Software Tools Installation
Command line
Version Control Systems (VCS)
Mathematics and Statistics
Python Basics
Relational Databases (SQL)
Non Relational Databases (MongoDB)
Scientific Computing (NumPy)
Data Manipulation and Analysis (Pandas)
Data Visualisation (Matplotlib and Bokeh)
Machine Learning with Python (scikit-learn)
Deep Learning with Python (keras)
Natural Language Processing with Python
Online Competitions
This list of topics will be updated continuously as new stories are being added.
