Why Big Data is pushing us towards Machine learning
As an Engineer Manager with more than 20 years of experience I have seen many changes that completely disrupted different areas: “Web 2.0”, “Cloud computing”, “Mobile-first”, “Big Data”, etc. The new kid on the block is “Machine learning” and it is definitely at its peak, one example is perfectly described by CB Insights on how startups coined the terms related to Machine learning to raise their valuation: “If you want to drive home that you’re all about that AI, use terms like machine learning, neural networks, image recognition, deep learning, and NLP. Then sit back and watch the funding roll in.”
In 2016 Gartner’s famous Hype Cycle of Emerging Technologies added the term “machine learning”, since it is being used everywhere.

But let’s put things in perspective and try to understand why is this happening now.
Huge amounts of data are being streamed from phones, computers, TVs and IoT devices. Every day the equivalent of 530,000,000 million digital songs or 250,000 Libraries of Congress worth of data are being created globally.
The data gathered grows exponentially, creating a paradigm shift on how we store and process large data sets. This affects the data infrastructure and long-term devops strategic decisions we need to make in order to support the increasing demand for scalability and concurrency.
But… It is not the quantity of data that is revolutionary but, that we can do something with the data.
For most organizations leveraging massive data sets is a problem since not everyone knows how to deal with terabytes of data. It takes highly specialized teams to analyze and process insights. When the data is huge, it is not humanly possible to understand which variables affect each other.
This is where Machine Learning fits in and why it will ultimately change the way we handle data. Regardless of the amount, researchers need to ask the right questions, design a test, and use the data to determine whether their hypothesis is right.
Let’s tidy things up
Here is an interesting Venn diagram on machine learning and statistical modeling in data science (Reference: SAS institute)

Artificial Intelligence — a term coined in 1956, refers to a line of research that seeks to recreate the characteristics possessed by human intelligence.
Data science — uses automated methods to analyze massive amounts of data to extract valuable knowledge and insight.
Machine Learning — is a recent development that started in the 1990s when the availability and pricing of computers enabled data scientists to stop building finished models and train computers instead.
Building data science teams
Most organizations create heterogeneous teams that include three primary data-focused roles: data scientists, data analysts and data engineers.
Here are the key differentiators between the data-focused roles:
Data Engineer — basically these are developers that know how to handle big data. In general they would have majors in: Computer science and engineering.
Data Analyst — they translate numbers into plain English. A data analyst’s job is to take data and use it to help companies make better business decisions. In general they would have majors in: Business, economics, statistics.
Data Scientist — they combine statistics, mathematics and programming. They have the ability to find patterns by cleansing, preparing, and aligning data. In general they would have majors in: Math, applied statistics, operations research, computer science, physics, aerospace engineering.
Wrapping up
Let me play with Dan Ariely’s quote on big data as others already did:
“Machine Learning is like teenage sex: everyone talks about it, nobody really knows how to do it, everyone thinks everyone else is doing it, so everyone claims they are doing it…”
So, can everybody use machine learning to increase ROI? probably not and I don’t think that in the near future all our jobs will be automated by bots. But, we definitely need to be able to massively process data at scale using machine learning in order to provide solutions whether it is to increase ROI in our organizations or to help solve global urgent problems like cancer research, natural resources scarcity and more.
Make sure you hire the right team and ask the right questions, hopefully your organization’s data + data science will provide you with the answers you are looking for.
Let me leave you with one last quote:
“We always overestimate the change that will occur in the next two years and underestimate the change that will occur in the next ten. Don’t let yourself be lulled into inaction.” — Bill Gates
So, start learning and researching so you can find the right answer for you and your data.
