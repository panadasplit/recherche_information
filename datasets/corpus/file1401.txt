We get on like a house on fire
Trust in AI may be one of the biggest challenges
We get on.
It may come that the biggest challenge in the adoption of AI is how humans are able to trust AI — and not in the way you think. Despite opinions that humans won’t ever trust robots, humans may be too trusting. Research suggests that once a human has placed their trust in a robot its hard to shake. That we are willing to blindly follow robots into the fire.
This may explain some of the accidents self driving cars have where there are humans behind the wheel supposedly supervising. Maybe it also explains some of the strange things we do when interacting with AI.
In the ‘assisted-self-driving’ car scenario I imagine people just switch off when told to hold the wheel and don’t actually pay attention and supervise. They trust the machine and why wouldn’t you - the car drives itself.
This mentality doesn’t help us through the next chapter of AI, where it augments our capabilities — but doesn’t replace us. If the AI is not to 100%, implicit and absolute trust will fail us both.
So how do we make it work while AI still has the training wheels on and is far from perfect? We learn to drive in a supervised environment, we should do the same with robots.
The challenge for AI/robot builders is how to work with people and keep them engaged. When do we let our users know “I’ve got this” and when do they need to intervene? I believe we need to encourage a healthy suspicion of the AI’s competence and gamify the act of supervising our creations .
We should stop worrying about building friendly faces into our robots to engender trust, and instead consider something more terrifying like the plague doctor mask to keep us on our toes.

