Pondering AI’s future with Azeem Azhar

This post is part of x.ai’s ‘Future of Jobs’ interview series. We’re talking to leaders and mavericks to find out how emerging technologies like AI are changing how they do their jobs. You can find all the posts in the series here.
Named a LinkedIn “Top Voice” for the past two years, Azeem Azhar has established himself as a vital voice at the intersection of technology and society. Founder of the venture-backed startup PeerIndex, the entrepreneur, investor, and former reporter currently publishes the essential weekly newsletter Exponential View.
We had a delightful exchange with Azhar about the current divide between human and AI problem solving, and his hopes for automation in the future.
Let’s talk about you first, Azeem, what aspects of your job would you like to see automated? What aspects of your job are already being automated or will be, in your view, over the next few years?

Improvements in prioritisation would be helpful, as there are too many opportunities and too much information. I’d love AI to be able to triage more efficiently to reduce what comes across my limited attention window. One part of my job, which is about exploring the space of the possible, has already benefited from automation. Web search engines and filtering tools have replaced much flicking through index cards at a library.
What parts of your job are you uniquely qualified to do (vs. AI, machine learning, etc.)?
I enjoy listening to people, asking them questions and learning from them, which will be hard for AI to learn to do genuinely. I also enjoy thinking about thinking and other aspects of metacognition. So I’m deliberate about trying other approaches to thinking through a problem, like analogic thinking, five WHYs, or applying a lens from different disciplines. I’m not sure AI will do that yet.
If artificially intelligent assistants could take over some of the tasks you’re used to doing, what would your day look like? What would you spend that extra time doing?
My problem solving falls into a number of categories: go deeper (to the most granular I can manage), go broader (to emphasis analogical problem solving), collaborate on a tricky issue, talk to someone (to socialise an idea or drive action), talk to an expert (to learn from them) or do something different (to let an issue foment in the back of my mind). If I’m not problem solving, then I need time to build the tree of actions and follow-up with whoever is doing It. In other words, any time bonus will be put to good use!
What’s the main thing you wish you had more time for? It doesn’t have to be related to work.
I would want to travel with my family and explore more of the world with them, especially while my kids are young.
If AI gave you time back, would you spend it pursuing recreational interests or would you spend it doing more work?
It is hard to know whether that would be the effect. It might be that I’d just to do more work. I’d like to think that I’d become like the fisherman in this Paulo Coelho parable but I’m not sure if many of us have the discipline to do that.
If you accept the premise that AI will fundamentally change your job in some way over the next decade, what will your job look like then, and what qualities will drive your success?
I’ve never done a job that existed the decade before I did it. My first job — as tech correspondent of the Guardian in 1994 — involved [coding] a Web publishing system and covering the Netscape IPO. I don’t look at my work over a ten-year time horizon. Too much changes too quickly.
Want to hire Amy + Andrew? Start your free trial HERE
Lead image via widewalls
Originally published at x.ai on February 4, 2018.
