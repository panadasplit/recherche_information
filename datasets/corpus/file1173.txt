3 — OPIRIA PDATA: TOKEN DAĞILIMI VE FON KULLANIMI
Önceki yazımda Opirianın token yapısı ve satış periyotlarının nasıl ilerleyeceğini açıklamıştım. Bu yazımda sizlere Opiria’nın token dağılımının nasıl olacağını, bu dağılımdan arta kalan miktarın nasıl kullanılacağını ve elde edilen fonun nereye aktarılacağını açıklayacağım.

Token Dağılımı
- Satış (%60): Üretilen tokenlerin %60’lık bir kısmı satışa çıkarılacaktır. Bu da 450 Milyon tokene eşittir.
- Geliştime (%13): Üretilen tokenlerin %13’lük kısmı data alımı ve iyileştirmeler için ayrılacaktır. 97.5 Milyon token değerindedir.
- Ekip (%20): Tokenlerin %20’si ekibe ayrılacaktır. 150 Milyon token değerindedir. Yazımın devamında ekibe ayrılan kısmın nasıl kullanılacağı ile ilgili bilgiler de vereceğim.
- Danışmanlar (%5): Yüzde 5’lik kısım danışmanlara aktarılacaktır. Bu bölüm 37.5 Milyon token değerindedir.
- Ödül (%2): Ödül için ayrılan kısım %2’dir. Bu da 15 Milyon token değerindedir.
PData token değerinin 0.10 dolar yani 10 cent olduğunu söylemiştim. Bu değerleri 0.10 ile çarparak dolar bazındaki karşılıklarını hesaplayabilirsiniz.

Ekip %20’lik kısmı nasıl kullanacak?
Ekibe ayrılmış olan kısmın %25’i ana satış sürecinden 3 ay sonra kullanıma hazır olacaktır. Geri kalan %75’lik kısım %25–25–25 olarak 6–12–24 aylık süreçlerde kilitli kalacaktır. Buradan projenin uzun soluklu planlandığını anlayabiliriz.
Fon Kullanımı
Şirket toplanan fonun sistemin gelişimi ve dünya çapında yayılması için kullanılmasını planlamaktadır. Burada planlanan giderleri şu şekilde sıralayabiliriz.
- Opiria platform iyileştirmesi: Fonun %30’luk kısmı platformun teknik altyapısının iyileştirilmesi için kullanılacaktır.
- Küresel yayılma: Fonun %45’lik kısmı yeni pazarlara girilip yeni müşteri bağlantılarına ulaşılması için kullanılacaktır.
- Veri tabanının genişletilmesi: Fonun %25’lik kısmı veri tabanının genişletilmesi, yeni panel müşterileri kazanılması ve pazarlama faaliyetleri için kullanılacaktır.
Bu makalemde sizlere PData tokenlerinin satış dağılımı, bu dağılımdan ekibin payına düşen kısmın nasıl kullanılacağı ve toplanan fonların nasıl harcanacağını açıkladım. Opiria PData platformu ile ilgili bilgiler vermeye devam edeceğim. Proje hakkında daha fazla bilgi edinmek isteyenler aşağıdaki linklerden çeşitli kaynaklara ulaşabilirler.
Web sitesi: https://opiria.io/
Teknik İnceleme: https://opiria.io/static/docs/Opiria-PDATA-Whitepaper.pdf
Telegram: https://t.me/PDATAtoken
BTC ANN: https://bitcointalk.org/index.php?topic=3076122.new#new
BTC Bounty: https://bitcointalk.org/index.php?topic=3081090
Medium: https://medium.com/pdata-token
Twitter: https://twitter.com/PDATA_Token
Facebook: https://www.facebook.com/pdatatoken/
Reddit: https://www.reddit.com/r/PDATA/
My BitcoinTalk Profile: https://bitcointalk.org/index.php?action=profile;u=1780407
