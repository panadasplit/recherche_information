Part 1: Machine Learning — Introduction

History
Arthur Lee Samuel, an American pioneer in the field of computer gaming and artificial intelligence coined the term “machine learning” in 1959. He built a Samuel Checkers-playing Program that appears to be the world’s first self-learning program, and as such a very early demonstration of the fundamental concept of artificial intelligence (AI).
What is Machine Learning ?
Arthur Lee Samuel’s definition of Machine Learning (1959)
Field of study that gives computers the ability to learn without being explicitly programmed.
Tom Mitchell ‘s definition of Machine Learning (1998)
Well-posed Learning Problem: A computer program is said to learn from experience E with respect to some task T and some performance measure P, if its performance on T, as measured by P, improves with experience E.
Combining both the definitions we get
Machine learning provide systems the ability to automatically learn and improve from experience without being explicitly programmed.
Applications in Real World
There are infinite number of real world applications but here are some latest ones.
Gmail Smart Reply
Apple Face ID in iPhone
Amazon Product Recommendations
Types of Machine Learning Algorithms
Supervised learning: learning model built using labeled training data that allows us to make predictions about unseen or future data.
Unsupervised learning: using unlabeled training data find patterns in the data.
Reinforcement learning: In this learning, the machine is exposed to an environment where it trains itself continually using trial and error. This machine learns from past experience and tries to capture the best possible knowledge to make accurate business decisions.
Originally published at blog.manishbisht.me.
