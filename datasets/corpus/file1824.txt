My First Foray with learning Machine Learning (Part I)
I had to speak at my first conference not too long ago and was presented with the problem of developing a machine learning model on an edge device. Not just that- considering my target audience, it gets better- training and inference had to perform disconnected from the internet entirely.
This, I later found out, is not a hard or unique problem that has not been done before. However, if you have zero background and experience with machine learning, this can become a daunting task very quickly.
Disclaimer: this story does not end with a sexy model with over 70% accuracy (I know, right?!) What it does do is communicate the resources I began with, my cat fight with online tutorials, and the things I learned about machine learning development by DOING.
So, let’s get started…
I knew (for various reasons, to include the fact that my audience loves pictures) that I was going to shoot for an object classification and object recognition model. If you are lost at this point, have no fear. I have a separate article I will be publishing (see my channel) that breaks down the machine learning basics I knew prior to tackling this project Barney style.
So where to find data? I started looking and soon stumbled upon Kaggle. Kaggle is amazing- if you’re looking for a great place for free datasets and an awesome open source community around machine learning and big data science challenges, Kaggle is the place to go.
I ended up picking the OIRDS dataset- a dataset of overhead images of various vehicles, as seen below:


OIRDS dataset sample
For future laughs, please notice the first image. What is different about this one compared to the others? [You shall find out, don’t worry. It won’t take you weeks to do this either, like it did for me…]
So, awesome. There is the dataset. So what did I have to do to built an object recognition model?
Well, I Googled it. I also asked smarter people at Google to help throw resources my way too.
I ended up with two places to start: this O’Reilly article on using transfer learning with the Inception v3 model, this article on TensorFlow (plus a tutorial), and finally the Tensorflow for Poets Codelab. These were all great resources, however, they failed me on a few things that I shall guide you through momentarily.
I began with the TensorFlow article, and then moved onto the Codelab. I would recommend starting with those two resources- they are excellent in giving you a high level understanding of what transfer learning is, guiding you through training your first object recognition model, then giving you the tools and guidance you need to train your own dataset. Unless… you pick a dataset like mine.
Advice here: if you would like to train your own dataset, I would recommend starting with a simple dataset that resembles the flowers dataset- images with single objects in them [more on this later] that are clear, and most importantly… are all the same size (dimensions)!
So there I went, sifting and sorting through all 1000+ OIRDS images and throwing them into category folders as TensorFlow for Poets taught me. Except… now scroll up to the sample images I included earlier.
First note: the first image is different because… it contains two vehicles. Also notice the quality of the images… can you really tell the difference between the types of vehicles? If you have no FMV-PED experience or training (even if you do) I challenge you to download the OIRDs dataset. Notice the quality of the images.
Back to the quality of the images. It is very important (in real life) for your training dataset to accurately reflect the expected image quality and variations that you might see in production. This is 1. because it will make your life hell to actually train your model and 2. it will make your model better prepared for production, where not every image is perfect. Avoid the classic code-breaking-in-production phenomenon as much as you can.
So naturally, the mistake I made was that I was now sorting images in duplicates, with images containing cars and trucks in the CARS folder, and that same image copied into the TRUCKS folder. This seriously f-ed up my model training process, giving me a whopping 29% accuracy on the first try.
Have no fear… Part II will bring you to the second part of the journey.
