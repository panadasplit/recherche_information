My quest to become a Data Scientist! Phase: Am I good enough?
Photo by Lacie Slezak on Unsplash
Imagine a person who loves technology. A person who is willing to help others with computer related problems even though they want it or not. A person who is considered as the computer guy in college. A person who has wasted a lot of his time playing video games. A person who hated mathematics. A person who doesn’t like to follow the academic curriculum but rather is only interested in new trending subjects. A person who is not that good when it comes to speaking in English.

2017 is coming to an end. A new interest is forming inside of my brain. Data Science… hm… Machine Learning… hm… Artificial Intelligence!!! Wow, I can become the next Tony Stark and create Jarvis, How cool is that? Although Iron Man created a significant impact on selecting this field, That may not be the whole reason for pursuing Data Science. I have realised that the era is changing and the future is all about data. It would be better if I took that path right now than later else I may need to catch up with a lot of things. So that’s it, I am gonna become a Data Scientist! I have decided.
WHAT ARE THE TOPICS THAT I NEED TO LEARN?
PROGRAMMING
Well, I have completed Bachelor of Computer Applications and have experience in coding things out and I am comfortable with solving problems that I face while programming. So I am sure this topic will not worry me. Even though I never tried python, all I need to do was learn the syntax of python. Most of the programming concepts are same with other programming languages. So I don’t need to learn from scratch.
LINEAR ALGEBRA AND CALCULUS
Ah! Crap! All these years I hated mathematics, now I need to love them? How can you love a topic that you hate? I don’t even remember Algebra except, (a + b)². Now I need to learn it all from scratch.
READER: YOU GOTTA BE KIDDING ME!
PROBABILITY AND STATISTICS
Probability? well, I think it’s not that hard. It’s just finding out how likely is an event to occur right? I just hope there is nothing more to it. And I don’t hate statistics, Its just I never gave any importance to it.
READER: QUIT DATA SCIENCE RIGHT NOW!
OKAY TIME TO START!
I know I have lots of hurdles on my way, but if I overcome that I can create JARVIS. Yes, I can do it! I can do it! I can do it! I can do it! I can do it! I can do it! I can do it! I can do it! I can do it! I can do it! I can do it!
READER: STOP IT! NOW YOU ARE ANNOYING ME!
With that level of motivation, I jumped on to Data Scientist Track in dataquest.io. First of all this website is amazing for those who are new to the world of Data Science. Now I need to get a job as soon as possible because of financial problem in my family. So I checked out the curriculum and decided I will complete Data Scientist track in one month. I quit all forms of entertainment and started my journey on 21st December 2017. Well, things weren’t going smoothly, I haven’t completed even Python Programming: Beginner Course until 1st January 2018. How am I supposed to complete it in one month? But I took a deep breath and started saying Yes, I can do it! I can do it! I can do it! I can do it! I can do it! I can do it! I can do it! I can do it!
READER: NOT AGAIN!
So with the power of coffee, I started pursuing that track like a madman. And finally, I reached 31st January 2018. I haven’t even completed Data Analyst section. Damn. All right, I will complete this track by February. So I started taking 3–4 cups of coffee and started pursuing the track like an ultra madman. By God’s grace, I completed the Data Scientist Track on 1st March 2018. I know a day late. :(
TIME TO LOOK FOR A JOB
Whaaat??? They are expecting a beginner data scientist to have all those skills. Nooooooooo! How can I get that many skills in such a short time? It’s impossible. Due to the financial crisis at my home. I decided to switch to become a Data Analyst by taking 1 month free trial of LinkedIn Learning. Even though I am easily able to follow the courses my heart was fully focused on becoming a Data Scientist. So on 20th March, I decided to take on Machine Learning by requesting my parents to give me time until June. And I have started studying Machine Learning from LinkedIn Learning to make proper use of that 1-month free trial. But still, I am kind of frustrated right now. My heart is pondering the question.
AM I GOOD ENOUGH?
