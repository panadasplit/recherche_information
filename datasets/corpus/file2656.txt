Why to organize and be a Speaker at Coursera Mentor Meetup.
Coursera Mentor Meetup along with LNMIIT Students
This year I successfully organized Coursera Mentors Meetup, Jaipur on 10th October 2017 at The LNM Institute of Information Technology.
The work was initiated on 19th Sep, when I first contacted Claire Smith, Head of Coursera Community Manager. She is one of the most enthusiastic and positive women, I have ever met.
Her positivity and support was the major key initiative that pushed me to conduct such a successful meetup.
LNMIIT as always, embraced my initiative and with the support of MozLNMIIT, I knew this was going to be a huge success. MozLNMIIT helped me to conduct and organize the above event which required a lot of effort from both ends.
My valuable peers from MozLNMIIT supported me throughout this journey and many more to come. I’m pretty sure that without their support it would not have been possible. So I would like to thank:
Akshit Agarwal (Campus Outreach)
Mayankar Shukla & Kartikey Joshi (Secretary)
Anirudh Jakhar & Chirag Goyal (Event Organizer)
Manan Nahata (Marketing Coordinator)
Enthusiastic students of LNMIIT
Pratul Kumar and Ayush Pareek
What I learned or gained from the event?
This was my first event at such a huge scale. I personally contacted several mentors from Rajasthan and made some valuable connections with them. Many of them are now my good friends.
I gained knowledge about many things from my fellow mentors who possess immense knowledge in their domain. I also learned how and when one should approach a others(mentors) appropriately.
With the great support from MozLNMIIT, I experienced the value of team effort and realized - “Together we stand, alone we fall”.
Why should one organize a Coursera Meetup?
You get a chance to meet highly intellectual people.
You get to know about new technology and working framework which would be highly beneficial for you.
You would get to know how an event is organized and how important a healthy community interaction is.
Networking, you meet several people with different mindset and different skills.
Student Making Notes
What the students of LNMIIT Gained?
More than 120 students turned up for the meetup. Students of LNMIIT got insight of several domains within a small time interval. As all the speaker gave insight about the Coursera courses they are mentoring,and how these courses helped them to shape their future, students had great time with speaking and getting to know to the speakers.
Pratul Kumar
What I learned as a Speaker?
This was my first event as a keynote Speaker. It was a great learning experience for me. Addressing the mass and trying to spread the knowledge to each and every one is a hard task. I learned that you need to be very specific and clear about the speech.
Speakers:
Divyanshu Rawat(Event Organizer and Speaker) — Single Page Application offered by John Hopkins University
 Divyanshu shared How Bootstrap Front End UI Framework makes designing a lot simpler and you don’t have to worry too much about how your site looks like on different browsers because the framework already takes care of that. It saves you a lot of time. Divyanshu also shared How the concept of Single-Page Application (SPA) is a web application or web site that interacts with the user by dynamically rewriting the current page rather than loading entire new pages from a server.
Lokesh Todwal — Machine Learning Offered by Stanford University.
 Lokesh shared insight about Machine Learning & AI : What it is and Why it Matters Artificial Intelligence (AI) and Machine Learning (ML) are two very hot buzzwords right now, and often seem to be used interchangeably.They are not quite the same thing, but the perception that they are can sometimes lead to some confusion.
Pradyumn Agarwal — Data Structures and Algorithms offered by University of California San Diego
 Pradyumn shared Algorithms are at the heart of every nontrivial computer application, and algorithms is a modern and active area of computer science and How he learnt algorithmic techniques for solving various computational problems and ended up solving whole lot of algorihmic problems by leveraging knowledge acquired by the Coursera Course that he is currently Mentoring.
Ayush Pareek — Natural Language Processing offered by the University of Michigan
 Ayush shared how Text mining, also referred to as text data mining, roughly equivalent to text analytics, is the can be leveraged to derive high-quality information from text.
Pratul Kumar(Event Organizer and Speaker) — Front End UI Frameworks And Tools offered by HKUST
 I shared how we can leverage Bootstrap to design templates for typography, forms, buttons, tables, navigation, modals, image carousels and many other, as well as optional JavaScript plugins and whole lot of cool stuffs that we can do with Bootstrap.




All the Five Mentors giving talk to LNMIIT students.
Each Speaker gave a brief insight about the course they are mentoring and the basic in that domain.
At the end I would like to thank all the mentors for turning up together and supporting this initiative.
