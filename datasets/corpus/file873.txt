Is KNIME (A Machine Learning Platform With ZERO Coding Involved) Suitable For You/Your Business?
Overview
It is not easy to become a Machine Learning engineer/Data Scientist. One of the biggest challenges for newcomers to this field is that there are so much you need to learn at the same time — Statistics, Linear Algebra, Data Processing, Programming Databases, etc. Knowing mathematical concepts and statistical models are not enough, you also need to learn how to code them quickly. Modern ML Engineers/Data Scientists also need to have great soft skills such as the ability to engage with senior management, good business acumen, excellent visual design skills, … etc.
This can be overwhelming, especially if you or your team have no background in coding! In this article, I will introduce some of the benefits of using KNIME to kick-start machine learning for your team or your business. The tool will be particularly useful if you just want to do a quick demo or Proof-of-Concept on Machine Learning.
Why KNIME and What’s Good About It?
KNIME is a GUI-based workflow platform. This analytics platform provides an easy-to-use graphical interface that allows you to simply drag-and-drop various pre-built Data Processing/Machine Learning modules without writing a single line of code.
It is free. All you need to do is to download the KNIME Analytics Platform & SDK from the website.
Lots of pre-built functions and modules. The KNIME tool provides you with the basic modules such as I/O, data processing, data transformation, data visualization, machine learning models (from linear regression to advanced deep learning). You can simply link these modules together in the workflow and provide specific instructions to fine-tune your model.
KNIME supports Python and R via wrapper script. This means you can incorporate your own code into the workflow or customize some of the processes within the workflow if you want to do so (although it is not as flexible as writing your own code in purely Python/R environment).
The open-source KNIME platform is GUI-based. The graphical interface allows you to simply drag-and-drop various modules to the Machine Learning workflow.
1. Do I Need To Upload Data to 3rd Party Cloud for Training?
No. This is a good news if uploading data to 3rd party cloud is your concern. You can store your data as .csv files locally, or you can also connect KNIME to your company databases such as MS SQL Server. See #2.
2. How To Connect KNIME to Microsoft SQL Server?
This is a great question since a lot of companies still use Microsoft Databases today. KNIME can connect to almost all JDBC-compliant databases. KNIME describes how to connect to database in their detailed Database Documentation. The driver must support at least JDBC 4.0 and the Microsoft JDBC driver for SQL server can be downloaded here.
3. Can I Visualize My Data?
Of course. One of the key benefits of using KNIME is that the tool provides the most basic data visualization functions such as Pie Chart, Histogram, Scatter Plot, Box Plot, etc. without writing even a single line of code. You can simply drag-and-drop the Data Visualization module you want to the ML workflow.
Data Visualization modules available in KNIME.
You can simply drag-and-drop the Data Visualization module you want to the ML workflow.
Some sample outputs from the data visualization modules.
Data Summary
Scatter Plot
4. Can I Access or Visualize the Predictive Model?
Yes, for certain ML models such as Decision Trees. Unlike some other cloud-based Machine Learning platforms/tools, KNIME actually let you view your fully-trained ML models. It is not a black-box. If you/your company are in an industry where your predictive models need to be reviewed by auditors for regulatory purposes, this ability will be very practical for you. It enables you to explain how predictions are made by your ML model.
Example of a fully-trained Decision Tree. You can visualize and explain how predictions are made by your ML model.
You can also export your trained models and save them locally. This is a huge bonus for using KNIME as compared to some other similar ML solutions/technologies, which do not allow exporting your trained model.
KNIME allows you to export and save your trained model to PMML format as part of the workflow.
5. What Machine Learning Models Are Readily Available?
A lot of basic Machine Learning Models are available in KNIME, including most Supervised ML models (Decision Tree, Regression, Neural Network), Unsupervised ML (PCA, Clustering), and even some advanced Deep Learning Models.
Examples of Built-in ML Models within the KNIME Platform
