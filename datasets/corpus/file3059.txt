Ecommerce trends in 2018: 6 growing trends
Do you know the most important ecommerce trends for 2018?Why do you think these trends are winning the hearts of your customers? And what do these trends mean for your company?

In this blog post, we will hear about the most popular trends in the field of e-commerce from two experts, the first is Frederik Hyldig from s360, who is Head of the bureau’s Paid Search team. The second is Frederik Nielsen from avXperten, who has been working with online sales for more than 15 years. The two gentlemen give us their take on the top trends based on their expertise in the field of e-commerce and based on the the conversations they have had with their own customers.
Three reasons why you should spend 10 minutes reading this blog because it will:
This blog will inform you of
● The 6 most important ecommerce trends in 2018
● 6 examples on how others have implemented these trends
● Why these trends are important for you and your customers

#1 Strengthen your products’ ranking on Google
When you search for “football boots” on Google, two columns pop up. The column on the left shows the general search results for football boots, while the right column shows the product advertisements.We call the column on the right “Google Shopping.”
Example of what Google Shopping looks like for football boots
Google Shopping’s advertisements appeal to customers who need a quick overview of a product — here they are able to compare products and prices from different companies with a simple Google search.
Google Shopping is for those who want to show products in a clear and inviting manner. Here, your customers can see: a picture of the product from your webshop, the product’s name, the price of the product and the name of your webshop.
Why Google Shopping is important
Advertisements on Google Shopping raise awareness via the search engine, and these ads also get more attention from the customer than ordinary search results on Google.
The illustration shows that Google Shopping has overtaken the text adverts
“On the basis of data from our e-commerce customers, we can see that Google Shopping has recently overtaken text adverts and, on average, now account for more than half of the paid traffic from Google to Danish webshops. Just 4 years ago, Shopping was almost non-existent in Denmark. Therefore, if you operate a webshop, Shopping is crucial if you are to get the maximum return from your AdWords advertising.”
Frederik Hyldig, Head of Paid Search, s360

#2 Engage your customers
“What will the weather be like today?” This is an example of what someone may ask through a voice search. A voice search is when customers use their voices to search for information or to make a purchase.
The video here shows how little time it takes for a man to purchase a bar of soap via voice search.

Voice search appeals to busy people — or those who are generally on the move a lot. It could also be useful for people who like to multitask, so they can search for something while they are doing something else. Companies that focus on Voice search try to engage their customers by helping them in their busy, day-to-day lives.
Exemplary instances of companies that have voice searches are Amazon, Dominos, Whirlpool and Ocado. Alexa, developed by Amazon, could be your new digital and personal assistant. Alexa has more than 1,500 different skills with which you can, for example, get Alexa to set an alarm, send text messages, order food to be delivered, add milk to your shopping list, find the nearest Greek restaurant, play Happier by Ed Sheeran or get it to read a book aloud for you (to name a few…).
Domino’s has also made it possible for their customers to order pizza via voice search. Whirlpool lets you start your washing machine, pause it, or adjust the temperature of your oven with a simple voice command. With the Ocado app, you can add “lemons” or other groceries to your shopping list by just saying the missing items out loud.
How to ask Oacdo to add lemons to your trolley
Why voice searching is important
Data from Comscore shows that 50% of all searches in 2020 will come via voice searches. Some people also believe that voice searches are more relevant and purchase-inducing than a normal written inquiry. Adding a voice search component means that your customers will come closer to going through with their purchase.
With the traditional text search, there are endless searches on Google that do not lead to any noticeable results for you as a webshop owner. Despite the fact that Voice searches have not yet gained a foothold in Denmark, it is still important to be aware of this trend.
“Even though voice searches have not made a breakthrough in Denmark yet, it is already extremely widespread in the USA, and the day products like Amazon Echo and Google Home start to understand Danish, I am certain that it will also be huge in Denmark. When it happens, you, as a webshop owner, must ensure that you also have a presence in this new type of search result.”
Frederik Hyldig, Head of Paid Search, s360

#3 Be where your customers are
Smartphones have taken control. This does not mean that fewer people are using their computer, it just means that there are more who have started using their phones when they carry out a search online. Therefore, it is crucial that your website is mobile-friendly. When your website is mobile-friendly, it means that your customers can access your website on any size of device without having to zoom in to find the shopping basket or to read the product description.
Previously, mobile-friendly websites were created with a responsive design, but a better solution to creating a mobile-friendly website is with Mobile First. Mobile First enables you to design your website on the telephone rather than the computer.
Why Mobile First is important
Mobile First must be taken seriously by all companies with a website and a webshop who wish to be accessible to their customers on the first part of their shopping journey, where the customer finds inspiration. If you are not in the running when your customers take the first step on their shopping journey, you can’t expect them to choose you when they have selected a webshop they want to buy from.

“Our data shows that almost half of the traffic today comes from mobile devices, and this shift means that you have to offer users a positive experience on their mobile phone if you don’t want to see your conversion rate fall.”
Frederik Hyldig, Head of Paid Search, s360
The conversion rate will decrease when the traffic moves from the computer to the telephone. Therefore, you should break down any barriers that may be present on the telephone. This can be achieved through more modern payment solutions in your checkout flow, such as MobilePay, ViaBill, and Apple Pay.
If you would like to find out more about why Mobile First is important and how to get started with it, then read this earlier blog post about mobile first.

#4 Be accessible “here and now”
 Websites often have a live chat function, with the main purpose of giving customers an answer “here and now.” The chat box normally pops up as soon as the customer accesses the website. On some websites, the chat function is set so it appears when the customer has been active on the website for a certain amount of time.
The chat function is used by customers who want to find out the price of an item or are looking for a specific product. It is easier for the customer to send a quick message in the chat box than to call a representative or send an e-mail. This is an appealing function to customers because they receive an answer to their question instantaneously, when they are in the middle of a purchase on the website.
3 reasons why customers prefer chat boxes
If you, as companies, want to be accessible when your customers have questions, even at 10 o’ clock on a Friday evening, then the chat box is an option. Chat boxes are for those in particular who have many products or technical products on their website.
Why a chat box is important
When your customers need an answer in order to proceed on their shopping journey, a chat box can give them a swift answer. If they don’t get an answer or a swift answer when they are on your website, then you run the risk of losing them because they specifically need an answer in order to take the final step on their shopping journey.
“We are seeing a higher conversion rate when we have the chat function activated. And we are getting some pre-sale inquiries that we otherwise wouldn’t have gotten (we measure this by the fact that we get more inquiries when we have chat activated — if we measure the number of chats + telephone calls versus the number of telephone calls when chat is deactivated — then we get more inquiries. In other words, there are inquiries we don’t get when chat is deactivated because they don’t come via telephone calls.”
Frederik Nielsen, CEO, avXperten

#5 Customise your products
Machine learning is used by companies who have the confidence to be brave enough to give their systems access to their customers’ data, so that the systems themselves can use this data and learn from it. This means that the recommendations that appear on the screen are not programmed by humans, but by a black box with an algorithm. Thus, machine learning gets the computer to do something without being explicitly programmed to do it.
“In the last decade, machine learning has given us self-driving cars, speech recognition, effective web searching and much more. Machine learning is so widespread today that you probably use it several times every day without knowing it.”
Frederik Nielsen, CEO, avXperten
Examples of recognisable companies that have implemented machine learning are Amazon, Netflix and Spotify. Amazon has, for example, made it possible for their customers to be presented with new recommendations. These recommendations are found on the basis of the customer’s previous purchases or the books the customer currently has on their bookshelf, or the items in one’s shopping cart.
On Spotify, you have probable become acquainted with “Discover Weekly”. This is also an example of machine learning. Here, Spotify’s algorithms find a personal playlist composed of different songs you haven’t heard before.
How Discover Weekly looks like on Spotify
This is why machine learning is important
More than 80% of all the series and films shown on Netflix have been put together via Netflix’s own recommendation system. What does this tell us? It tells us that machine learning is essential if you want to find products that match your customers’ previous purchases.
The more the products match the customer’s preferences, the more inclined the customer will be to buy them. Machine learning can be used to give your customers a tailor-made experience when they shop in your webshop or website.

#6 Beware of Amazon
 Amazon is spearheading almost half of all online sales in the U.S., which is something all companies should be aware of. There are also several indications that Amazon is making advances towards the Nordic countries. Read more here.
Stats about Amazons online growth
How to handle Amazon
It may be the large generalist webshops that will feel the Amazon effect the hardest, as they are not able to provide detailed advice on their products but are more a sales platform for an extremely broad and deep range of goods.
Therefore, Danish companies should start doing what Amazon will never be able to — that is, give customers a knowledgeable customer service with regard to the products. It is the customer service, and the specialised advice about the company’s products that separates Amazon from the individual niche stores.
You have now been given an introduction to 6 of the most popular trends in 2018 according to Frederik Hyldig’s and Frederik Nielsen’s assessment. But, we are also keen to know which trends in the field of e-commerce you find current for this year, and why.
If you would like to give your customers a greater incentive to purchase on the telephone, get in touch with ViaBill at this e-mail address: kbc@viabill.com
