Ubex Roadshow Continues in Seoul

The Ubex advertising exchange project has launched its roadshow and presentations at the d10e Conference in Seoul on the 6th of March, making important announcements during its debut in the Asian theater of the industry.

The d10e Conference held in Seoul is one of the foremost platforms for the development and presentation of blockchain technologies and projects, attracting specialists from around the world.
On the second day of the conference, Ubex presented the project and officially announced the start of White List registration. The results of the conference were extremely positive as a number of meetings were held with local partners. The overwhelming success of the presentation and heightened interest in the project from local enthusiasts forced the Ubex team to redouble their efforts so as to have the opportunity to establish working relations with the enormous number of willing Korean project participants and partners.

After Seoul, the Ubex team will launch its full scale roadshow with its next destination being the Money 2020 Conference in Singapore, which will be held on 13th of March. After Singapore, events in Vietnam, Shanghai, Hong Kong, Tokyo and finally Switzerland are to follow as Ubex will present the project at various world-class venues.

Stay tuned to the latest news and updates from Ubex by subscribing to its official Telegram, YouTube and KakaoTalk channels. The first 100 subscribers of the KakaoTalk Ubex channel will be entitled to rewards.
