EdTech Podcast: “What is AI & what has it got to do with me and my students?”
The EdTech Podcast has released the second episode in the FutureTech for Education series. Host Sophie Bailey* takes a deeper diver into artificial intelligence, following up on the inaugural episode, “What does future tech for education look like?”
AI pioneer Stephen Wolfram talks about how students can use Wolfram Alpha as a tool to learn computational thinking. Like seemingly every article published today, this piece in Wired is poorly titled but summarizes it fairly well: “Wolfram Alpha Is Making It Extremely Easy for Students to Cheat.” I do suggest you listen to the podcast to hear it directly from Wolfram himself.
- Stephen Wolfram
Murray Shanahan reads an excerpt from his book, The Technological Singularity, which imagines how a team of humans and a team of AI each build an ideal motorbike.
The Technological Singularity
The idea of technological singularity, and what it would mean if ordinary human intelligence were enhanced or overtaken…mitpress.mit.edu
The third guest is my colleague Peter Foltz, perhaps the foremost expert on use of AI for adaptive learning platforms. Foltz explains that while bias in AI is a problem we need to be continuously aware of, AI can also identify biases. As with all educational technologies, it’s a tool for educators to use — a compliment to the human skills necessary for successful learner outcomes.
Finally, Bailey gives an overview of the 2016 paper Intelligence Unleashed: An Argument for AI in Education, by Rose Luckin and Wayne Holmes, UCL Knowledge Lab, and Pearson’s Mark Griffiths and Laurie Forcier.
Once again, I’m honored to have been a small part of this episode. In just 25 minutes, Bailey, primarily supported by Wolfram, Shanahan, and Foltz, presents a clear overview of how AI is an increasingly valuable tool for learners and educators alike.
* Bailey was recently name one of the 3 voices that build a bridge between edtech startups and teachers
