Cortex新一轮空投正式启动 价值百万CTXC等你来拿！
Cortex自上线以来，获得了Ai领域、区块链领域从业者、爱好者、投资者的广泛关注，中英文社群也在快速成长中。
为了进一步促进社区良性成长，Cortex项目官方发布新一轮大规模空投活动。
规则如下：
中文社区投稿奖励
Cortex项目团队联合链向财经，推出专栏征集Cortex主题相关文章的活动。欢迎大家到链向财经发布Cortex项目动态、对Cortex项目的看法、token的行情分析、Cortex项目的技术分析或未来发展等内容，链向财经内容团队会进行审核，一旦审核通过会发布到Cortex项目库专栏下。投稿者将会获得100人民币对应的等值CTXC（按照发币当天市价兑换）。

活动链接、PC端：http://t.cn/R3wsaJn
移动端
Twitter、Telegram群组
reddit的社区奖励
2018年5月9日，Cortex正式上线了Twitter、Telegram群组、reddit相关的社区bounty奖励系统。
规则如下：
每天奖金池包括750 CTXC。针对不同的任务（如关注/点赞/转发/回复Twitter、telegram邀请好友/积极发言、reddit订阅/参加投票），会有不同的积分奖励。当日完成任务获得的积分，按照任务积分占当天总积分的比例瓜分奖金池的CTXC。
具体规则可参考空投页面详情介绍。

活动链接、PC端：http://t.cn/R3AvZ9V
移动端
真诚的欢迎大家通过以上任何一种方式参与到Cortex社区的建设中来，让Cortex社区健康、有序、良性的快速成长。
联系我们
网站：http：//www.cortexlabs.ai/
Twitter：https：//twitter.com/CTXCBlockchain
Facebook：https：//www.facebook.com/cortexlabs/
Reddit：http：//www.reddit.com/r/Cortex_Official/
Medium：http：//medium.com/cortexlabs/
电报：https：//t.me/CortexBlockchain
中国电报：https：//t.me/CortexLabsZh
