FAST.AI Lesson 4 Notes
Note: More mediocre notes but I expect them to get better on the next iteration.
Many awesome links on learning rate, Stochastic gradient descent. Differential learning rate. Transfer learning.
Structured learning.
Dropout: randomly pick X number of cells (e.g., 1% of all cells) and delete them; output doesn’t change much; avoids overfit (helps with generalization); each mini batch throws away different cells; forces algo to continue finding suitable representation. Start experimenting with different dropout rate at different layers when overfitting happens. (train vs. validation loss)
Linear layer: matrix multiply (e.g., 1024 -> 512 activations).
Also try dropping only on last (linear) layer. (Typically .25, 0.5)
Why loss rather than accuracy? Because we can see it in both train/validation. And it’s the actual item that’s being optimized.
Python for Data Analysis is a good resource.
The notebook losson3-rossman.ipynb uses 3rd place winner’s approach. Different types of column data: categorical, continuous. Year/Month/Day: treated like categorial.
Different embedding dimension for different variables. Take the cardinality of the variables, divided by 2, but no greater than 50. For example, the cardinality of days of the week is 7.
Embedding: Distributed representation that allows NN to learn interesting relationships. Good for any categorical variables. Might not be useful for high cardinality variables.
Creating time series columns (not traditional time series technique) and let NN learn from it:
DL allows for simpler model with less feature engineering while retaining state-of-the-art results.
See blog post by Rachel about How (and why) to create a good validation set.
Language modeling
Given a few words of a sentence, predict the next sentence.
Notebook: lang_model-arxiv.ipynb
Much less mature than computer vision. But the big ideas from CV are starting to spread into NLP.
This class focuses on word-level prediction (as opposed to Karpathy’s character-level prediction). Also focuses on text classification rather than generation.
Torch’s NLP library:
Spacy tokenizer is one of the best around. Replace each word with a integer that uniquely identifies the vocabulary.
Ok so I got a bit tired of typing and started using my iPad. Here’s the remainder of the notes:



