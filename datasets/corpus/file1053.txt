Loading Data to AWS from Kaggle
Loading data into AWS linux EC2 Linux instance is a time consuming job . There are tools like Kaggle CLI which can help is downloading it but it comes with less error handling which makes it difficult understand the error .
As a student of FastAi I have been introduced to a chrome extension tool called CurlWget . This makes the life really easy .
CurlWget listens to any download and it produces the Wget command out of it .

Well with this you just need to copy and paste to aws EC2 command line and you can download the dataset .
