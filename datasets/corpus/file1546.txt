
How to Spot The Real Data Scientist?
Some four years back, when data stream was picking the pace, many professionals began changing their titles to Data scientist; though the exact qualifications of a ‘Data Scientist’ was slackly defined. This resulted in perplexity in the hiring market due to an overstatement of skills in resumes.
A couple of years later, ‘Data Scientist’ became ‘The dream profile’ for ambitious job-seekers who wanted a high paying and an alluring job.
But there is a mounting apprehension among business leaders and employers that applicants are just getting tempted by the ‘title’ of a Data scientist without actually possessing a proper skill set and mindset required to succeed the job.

So, genuine as well as fake candidates appear with tremendous enthusiasm and optimism at the job interviews.
Structuring an interview for a Data Scientist position and knowing if the candidate is really suitable for the job profile is pretty hard. Usually in data science interviews, candidates are asked to solve brainteasers (related to probability) which are easily solvable if they have memorized the formulas. But it doesn’t signify if they will be able to work on, visualize and give meaningful insights from raw, unstructured and messy data.
So how to be sure of a worthy Data Scientist-
A real data scientist has:
Good experience with unstructured data and statistical analysis: If the resume reads experience in tools such as Google Analytics, SPSS, Hadoop, AWS, Omniture, and Python, ensure that they are accompanied by projects that showcase the skills put to use. If there is a vague mention of the experience or the projects, then they are probably not a data scientist.
Business Acumen: It’s not that a person with research background will not back a good data scientist, but the key responsibility of a corporate data scientist is to understand ‘how data affects the business goals’ and ‘to give critical insights in risk mitigation and overall solution.’ Exceptional data skills combined with business savoir-faire are the real determinants of corporate data scientists.
An eye for logical and visual interpretations: With strong analytical skills and ability to spot patterns, a good data scientist is one who understands visual representation of data and is able to interpret and represent a story around it that adds value to any organization.
Multidisciplinary: Many organizations narrow down their recruiting on candidates with Ph.D. in statistics or mathematics. But the truth is, a good data scientist can come from many backgrounds with no advanced degree in any of them.
‘Person who is better at statistics than any software engineer and better at software engineering than any statistician’ -Josh Wills
Curious and Creative: A data scientist should be able to look beyond numbers, even beyond an organization’s data sets to find answers to problems or may be pose new dimensions and insights to an existing scenario.
Able to handle noise in data: Any data model draws a fine line between the sample set being too simple to be meaningful and too complex to be trusted. No matter how ‘big the data is, its finite sample is pierced with potential bias. A promising data scientist actually develops a healthy skepticism of their data, techniques, and conclusions.
A data scientist not only helps in providing data-driven insights to propel faster decision making but are recognized as strategic assets which drive business growth and profitability.
If one can find a candidate with most of these traits accompanied with the ability and desire to grow, then you have found someone who can deliver incredible value to your system, your business and your organization at large.
“A data aspirant leverages data that everybody sees; a data scientist leverages data that nobody sees.”

