Is R really that simple? An Intro
Hey Guys! 😃 Many of you have been mad on learning R / Python for Big data / Data science related projects. Congratulations for those who have already landed in their dream jobs as a Data Scientist
Few students who are aspiring to be a Data scientist may have already begun to make your hands dirty by practicing R exercises. Good luck on that!

R is a simple programming that was discovered in 1976 mainly used for statistical computing.
What? Then why are we just discovering now to use R?
I got your question on why R is prevalent now but not before? Businesses before did not have large data sets (I mean the billions of data) in their analyzes and therefore there was no need to process them. As everyone knows, we lived in the world of Excel and thought that it was almost impossible to process large records in seconds.
R is the IRON MAN for Data science 💪 💪 💪

Instructions to Download and Install the Softwares
STEP 1 👉 Download ‘R’
Please click on the link https://cran.r-project.org/ to download ‘R’. In case you directly install ‘R Studio’, you would get the below error.

STEP 2 👉 Download the RStudio Software
You go to this link to download the Open Source edition — https://www.rstudio.com/products/rstudio/download/#download
If you’re Super Rich, you could also get the other versions

STEP 3 (ONLY FOR MAC USERS) 👉Adjust system settings

After you start ‘R’ and ‘R Studio’, if you get errors like the above, please execute the following command in mac terminal and then restart R
STEP 4 👉 Sorry, there is not any! 😝
Good Job! you have installed ‘R’

Is r simple
Programming languages ​​/ frameworks such as PHP, Javascript, PL / SQL are easy to learn. Similar to that, R is also a very simple programming language that you can learn quickly. The depth of knowledge lies in the amount of exercises that you work. The MORE the BETTER! ✗
Comment out a line -
Each programming language has its own way of commenting out the code, In ‘R’, the special character ‘#’ is used as a single line comment. In the below articles, I would be using ‘#’ to denote the commented part if the code and comments are mentioned on the same line. Below is a simple commented line,
Declaring a variable -
<variable_name> = c (<value>)
Examples -
Identify the datatype of a particular variable

Although you find can find it in “Environment” tab of R Studio, you could also see the datatype using the below command,
Data types
Below are the various types of variables that are used in R Programming
Vector, Matrix, Array, List, Data Frame
VECTORS
Types : Logical, Numeric, Integer, Character and Complex.
Below are the examples of each of the different types of vectors.
MATRIX
Matrix is an two dimensional rectangular layout
Parameters
data — Input vector
nrow — No.of rows
ncol — No.of Columns
byrow — If TRUE, then elements are arranged by Row
dimname — names assigned to Rows and Columns
Note: All the parameters The Now lets look at an example —
ARRAYS
Arrays is used to store data in more than 2 dimensions
Parameters
data — the vector data
array dimensions — first two values to describe size of each array. The multiplied value of 3rd and 4th values describes the occurrence of each array
If Number of values (vector) is lesser than the Matrix size, the values will restart in the matrix which is unfilled and will start to resume from the number where it is left off in the previous matrix. Below is an example -
LISTS
Lists can hold different datatypes
Syntax
example
Data Frame
Two dimensional table or array like structure. Its mostly used when working with data imported from excel or any data source
syntax
Example —
Other common pre loaded data sets can be accessed by the same command,
example —
Logical Operators
AND, OR, NOT
Using the AND and OR operator two times will only compare the first element
Examples -
Conditional Statements
IF Statement
Syntax
Example
Switch Case Statement
Syntax
Example
Repeat Statement
Syntax
Example
While Loop
Syntax
Example
For Loop
Syntax
Example
SEQUENCE Operators
Since the syntax is fairly simple, I’ll directly explain you with the examples
Functions
Similar to other programming languages, a function in R is a simple reusable code that can be called any number of times with the required parameter
Syntax
Example
FACTORS
Factors print the unique occurrences of each value present in the vector. This is very important as it will be used in determining probability in statistical problems
Now you have to learn the basics of R and how to use simple commands and their purpose.

I’ve posted various topics / issues related to R, Data Science and Data Warehousing. Please read them as well if you are interested. Feel free to contact me in case of any doubts / issues that you have face during R coding and I would be glad to help!
Thanks for reading.
