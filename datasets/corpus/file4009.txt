Duke University — Java Programming: Solving Problems with Software — Week 4
Image credit to Coursera, Duke University and JAVA
This course provided a fantastic finale to an already great course. This week delves deeper into the subject of CSV (Comma Separated Value) Files and how to use code to manipulate them. For the project this week, the students were given the US data on baby names according to gender by year. This data went as far back as 1880 all the way up until 2014! We wrote functions that would analyze the data set and rank names according to popularity. After that was completed, we would write a second function that would find what the name would be in a different year with the same rank. We would then print “(Name) born in (year) would be (Name2) if (he/she (based on gender)) was born in (year2). This has provided me with plenty of enjoyment as I have been checking the names of family and friends if they were born in different years. For example, my name would be “Alejandro” if I was born in 1996.
As always, to accomplish this task I had to use the Seven Step Method.
) Work Example By Hand
) Write Down What You Did
) Find Patterns
) Check By Hand
) Translate To Code
) Run Test Cases
) Debug Failed Test Cases
There is an optional honors week that follows, which I will be getting to next time!
Thank you for reading!
If you would like to read more of my blogs, I have a list below.
My Experience With Learning Java
My method of learning to code in JAVA begins with me seeing two course specializations on Coursera. The first…medium.com
Duke University — Java Programming: Solving Problems with Software — Week 2
This week of the course took a lot longer than I had originally anticipated. It provided quite a bit of challenge as…medium.com
Duke University — Programming Foundations with JavaScript, HTML and CSS — The Second Week
I have just finished the second week of Duke University’s Programming Foundations with JavaScript, HTML and CSS…medium.com
Duke University — Java Programming: Solving Problems with Software — Week 3
The third week of this course has been quite enjoyable. I, personally, have an interest in data science, and this week…medium.com
