Installing JupyterLab
I encountered JupyterLab recently on one of my Twitter journeys by seeing this tweet:

Why is this such an interesting feature? Well, typing . followed by TAB completion normally lists all methods and attributes that are associated with that object. If we were to perform a . followed by TAB on a common object like an integer, we would see a number of methods that can be called on that integer.

This is an extremely useful feature that is part of most IDEs (Interactive Development Environments).
This becomes more difficult when you have a situation where the object you are interested in is enclosed inside a set of brackets, for example: (3+1). What would happen if we attempt to perform the . TAB completion on this object?

Oops. This has now been autocompleted with .ipynb_checkpoints/, which is not a useful outcome at all. The reason why this occurs is that the . TAB completely ignores the object in brackets, and autocompletes as if it wasn’t there. (To be convinced of this, notice that this is exactly what happens if we . TAB in an empty cell)
If this feature were to work, then somehow the application would have to know that the result of (3+1) is an integer, and that it should treat the brackets in exactly the same way that it treats the integer. This has really bugged me in the past, but I could not think of a clear and robust way to frame the problem in order to contribute a solution.
I have been a long time user of Jupyter Notebooks for my work, and I regularly encourage others to use it who would like to work on Data Science projects. These recent updates to JupyterLab looked really cool.
Having already installed Jupyter Notebook within my Anaconda environment, I proceeded to install JupyterLab.
conda install -c conda-forge jupyterlab
This results in a successful installation without any pain at all. I launch the notebook with:
jupyter lab, and navigate tohttp://localhost/8888 in my browser.
So far, so good. Now to replicate this great Tab completion feature that attracted me to download this project in the first place:
Experimenting with TAB completion.
Huh? This is not the moment of satisfaction that I was expecting at all. Now, I have been working with data science software for two years now, I am sure that new users of Jupyter and JupyterLab would probably just give up when something like this happens. This is why I want to document my entire thought process in how I debugged this issue.
What software versions am I using?
This is a very common way to identify the source of a problem. After navigating to the JupyterLab website, I opened up the Binder link.

This opens up a version of JupyterLab on a webserver in the cloud, where you can immediately start hacking. (In another post, I would like to explain why I think Binder is such an awesome service to people working in data science.)
After opening up a new notebook, and investigate the . TAB completion as before, we can see that it works!

So what is going on here?
Well, clearly there is something different between my installation of JupyterLab and the version on JupyterLab that is launched through Binder.
Another benefit of JupyterLab is that it has a terminal console that is available within the environment. Using this, I can find out that the version of JupyterLab on the Binder instance is 0.31.8 — the same as I have on my machine!
Hmm — so if it is not a problem with the version of JupyterLab, what could it be a problem with? Within the JupyterLab documentation, I start to the get the impression that JupyterLab and Jupyter Notebook are actually two seperate projects. Therefore even if I have the state of the art version of JupyterLab on my system, the notebook need not be.
This turned out to be the cause of the issue. On the Binder version, jupyter notebook was version 5.4.0, and on my own computer the version was 5.0.0.
Sometimes, you just need to check your versions.
