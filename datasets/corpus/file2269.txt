Hands on with the AIY Projects Vision Kit
A Do-It-Yourself Intelligent Camera for the Raspberry Pi
In the early part of last year Google and Raspberry Pi did something rather unusual. Together they packaged machine learning, and the ability for a machine to think and reason, free on the cover of a magazine. The kit allowed you to add voice interaction to your Raspberry Pi project, and the magazine sold out in hours.
“The positive reception to Voice Kit has encouraged us to keep the momentum going with more AIY Projects. We’ll soon bring makers the ‘eyes,’ ‘ears,’ ‘voice’ and sense of “balance” to allow simple, powerful device interfaces.” — Google.
Announced at the tail end of last year, the second AIY Projects kit, the Vision Kit is a do-it-yourself intelligent camera based around the Raspberry Pi Zero and a custom-built Vision bonnet designed by Google.
The completed AIY Projects Vision Kit.
Based around the Movidius MA2450 chip, a vision processing unit designed by Intel and intended for machine vision in low-power environments, the Vision bonnet allows the kit to run real-time deep neural networks directly on the device, rather than in the cloud.
The initial rollout of the kit a couple of week ago had some teething troubles, and it was taken off the shelves at Micro Center. However after updating the documentation and software the kit is now back on the shelves.
Opening the Box
The AIY Projects Vision Kit.
Opening the box the contents will look familiar to anyone that’s played with the original Voice Kit, as it shares a lot of stylistic similarities to the original.
Opening the box.
The kit consists of the Vision Bonnet, which connects directly to the GPIO header of your Raspberry Pi Zero, two camera cables — one to connect to the Raspberry Pi Camera, the other to connect the Vision Bonnet to the Raspberry Pi Zero — a big arcade button, an LED, a piezo buzzer, some spacers, a tripod mounting nut, and a lens assembly which gets fitted in front of the Raspberry Pi Camera module.
The AIY Projects Vision Kit.
Finally there’s also the cardboard case which, after Google Cardboard, has become somewhat synonymous with Google’s own prototyping efforts.
The Vision Bonnet.
The final production version of the Vision bonnet looks almost identical to the pre-production version I’d seen before the kit arrived on the shelves, just missing some headers used during debugging to flash the MCU firmware onto the bonnet.
You’ll Also Need
In addition to the contents of the AIY Projects Vision Kit box you’ll also need a Raspberry Pi Zero, or more usefully Raspberry Pi Zero W, a 40-pin header block, a Raspberry Pi camera board, a 2 Amp USB power supply, and a micro-SD card which is at least 8GB. The camera board needs to be the latest revision, marked “V2.1,” which replaced the original camera board back in 2016.
40-pin header (top), Zero W (middle), micro-SD card (bottom left), and camera board (bottom right).
Despite the added cost it’s a good idea to get a named brand high speed SD Card. Because like power supplies, there really are big differences between the seemingly identical cards. Choosing the wrong one can mean long access times or even a card that just doesn’t work or fails quickly under normal use.
The Raspberry Pi Zero, and Zero W, don’t typically arrive with the 40-pin header block pre-soldered—although the Foundation has just released a new Raspberry Pi Zero WH that does, so if you’re feeling a bit unsure about your soldering you should probably spend a few extra dollars and pick up the pre-soldered board.
Gathering your Tools
If you picked up a Raspberry Pi Zero without headers, you’re going to need a soldering iron, and some solder, or alternatively you could use solderless “hammer” headers which require a jig and few taps with a hammer to fit. However other than that you can probably get away without any tools, although some scotch tape (aka sellotape), and possibly a craft knife or scissors, might come in handy.
Other Places to Get Help
If you find anything here confusing the kit does comes with a really good assembly guide, while issue 65 of the MagPi also has a solid walkthrough of how to put the kit together
Soldering Headers to your Raspberry Pi
If you’ve chosen to solder the headers onto your Raspberry Pi yourself, you should pick up a 40-pin male header block. While there are other options, if we’re going to attach the bonnet that’s what you’ll need.
An easy way to solder the headers is to insert them into a breadboard, and then insert the Pi on top. The breadboard will help keep the board steady and the pins straight and aligned as you initially tack the pins—another alternative is to use a blob of Blu Tack to fix the board down onto a flat surface.
Using a breadboard to steady the headers and Raspberry Pi.
Then use the flat of the soldering iron to heat the pin for one to two seconds before bringing in the solder to touch the other side of the pin. The solder should flow smoothly down the pin and form a good joint.
Making perfect solder joints. (Video credit: Pimoroni)
You should go ahead and tack the upper left and lower right header pins first. Once you’ve tacked the first two pins you can lift the board up and see if the Raspberry Pi is level and flush on the headers. If not, you can reheat the solder joints and use your fingers to level the headers off before proceeding to solder the rest of the pins.
It’s best to solder one long row at a time to help keep your iron well angled to the board, skipping between pins tends to mean you’re constantly changing the angle of the iron and your soldering can suffer as a result.
After soldering all your pins remove the board form the breadboard and take a careful look at all of your joints to check for messy joints or solder bridges. Most mistakes are easy to fix, so long as you don’t overheat the iron or leave it in contact with the board so long that you melt the silk screen. Although, even then, wonky pins can be fixed a good deal of the time.
If you’re unsure whether you’ve soldered your 40-pin connector to your Raspberry Pi Zero correctly, then a good first test is to use the Pin Test utility that ships with Gordon Henderson’s WiringPi library. You can download the latest version of the library from his website.
Getting the Software
Go ahead and download the latest SD Card image for the Vision Kit.
These days I’d generally recommend Etcher, made by the folks at Resin.io, for burning card images. It’s cross platform — it works on Windows, Linux and mac OS — and lets you burn an image in four clicks.
Burning the aiyprojects-2018-01-03.img.xz card image using Etcher.
However, if you’re a command line person like me, you can either download and install the experimental Etcher command line tools, or you can still go ahead and do it the old way.
The instructions here are for the Mac, because that’s what I have on my desk, but instructions for Linux are similar.
Go ahead and insert the micro SD card into the adaptor, and then the card and the adaptor into your Macbook. Then open up a Terminal window and type df -h, and check the device name for your SD Card. In my case it’s /dev/disk1, and I’ll need to use the corresponding raw device, /dev/rdisk1, when writing to the card.
Go ahead and unmount the card from the command line,
rather than ejecting it by dragging it to the trash. Then from there we can go ahead and write the image to our SD card.
Unfortunately the card image came as a .xz file and there isn’t a command line tool to uncompress these sorts of file available by default on macOS. Fortunately, if you have Homebrew installed, you can brew install the xz command line tool. In the Terminal window change to the directory with your downloaded disk image and type,
to uncompress the disk image, and then write it to your card as follows,
If the above command reports an error dd: bs: illegal numeric value, change bs=1m to bs=1M.
The image’s boot partition should be automatically remounted after dd is done writing the image.
Enabling Wireless Networking
If you’ve used Etcher, or if the card’s boot partition hasn’t automatically been remounted, you’ll need to open Disk Utility and remount the boot partition. Alternatively you can just pull the card out, and reinsert it, which is probably easier, which should also mount the boot partition automatically.
Make sure the partition is mounted and navigate to the boot partition, and create a new file named wpa_supplicant.conf using your favourite editor,
and add the following lines,
Where COUNTRY should be set the two letter ISO/IEC alpha2 code for the country in which you are using your Pi, e.g.
GB (United Kingdom)
FR (France)
DE (Germany)
US (United States)
SE (Sweden)
and SSID is the ESSID of your home network, with PASSWORD being the WPA2 password for that network.
It’s important to enter the correct country code in the file as this will determine which regulatory domain your Raspberry Pi thinks its operating in, and therefore which wireless channels it enables on your adaptor.
Enabling SSH
Recent releases of the Raspbian operating system have the SSH server disabled on boot, and since we’re intending to run the board without a monitor or keyboard, we need to renable it if we want to be able to SSH into our Raspberry Pi. You can do this by making sure there is a file called ssh present in the boot volume. Go ahead and enter,
at the command line. When the Pi first boots, it looks for this file; if it finds it, it will enable SSH and then delete the file. The contents of the ssh file don’t matter.
Enabling OTG
One disadvantage of Raspberry Pi Zero compared to a normal Raspberry Pi is the lack of Ethernet port. That means we’re relying on wireless networking to give us remote access to our Pi, unfortunately we need to configure the board for every wireless network we want to use. So it’s handy to have another route to log into the Pi.
Fortunately we can access our Raspberry Pi fairly easily using something called USB OTG which will allow us to set up a virtual network connection between your Raspberry Pi Zero and our laptop. This will allow you to SSH over the USB cable powering the Pi Zero, allowing us to configure wireless networking without need of a keyboard, mouse, or screen.
Make sure the boot partition is still mounted, and go ahead and open the config.txt file in an editor of your choice, e.g.
and make sure it contains the following entry,
which may well be already appended near the bottom of the file. However if not, add it. Next, go ahead and edit the cmdline.txt file.
You need to be careful here, as the formatting of this file is pretty important. Each parameter should be separated by a single space, not a newline or a tab. Go ahead and insert modules-load=dwc2,g_ether after rootwait.
Initially it should look like this,
and afterwards it should look like this,
Once you’re done, eject the card with the command,
and you should have a working card image with all three of wireless networking, SSH, and OTG now enabled.
Testing our OTG connection to the Raspberry Pi
Go ahead and insert your micro SD card into your Raspberry Pi Zero and connect it via USB to your computer. It is important to connect your laptop to the Raspberry Pi Zero using the USB, rather than the PWR, micro-USB port.
Connect your Raspberry Pi to your laptop using the USB, rather than the PWR micro USB port.
Using the USB port will power the board, but more importantly it will also allow us to make a data connection. After connecting the Pi Zero to your laptop the green ACT LED should start flashing. It could take as long as 90 s to boot up the first time, although it should be shorter on subsequent boots.
The Raspberry Pi will be listed as ‘RNDIS/Ethernet Gadget’ in Settings.
After it has finished booting the connection to the Raspberry Pi should appear as a USB Ethernet device called RNDIS/Ethernet Gadget in the Settings app. It’s there that we can see the IP range being used by the USB connection.
Typically the IP address range on your home network will use the 192.168.1.* IP block, but our laptop USB Ethernet connection has been given an address in the 192.168.11.* block.
While we know the IP address of our laptop, in this case 192.168.11.54, we don’t yet know the IP address of the Pi. The easiest way to find this out is to use nmap, an open source utility for network discovery. This doesn’t come installed by default on macOS, however you can easily download a disk image file containing the installer and follow the prompts to get up and running.
Once installed you should run it as follows,
Here we can see there are two hosts in the 192.168.11.* range. Our laptop, and an unidentified host that has to be our Raspberry Pi. We can check the connection with a quick ping request,
and once we’ve found the Pi, you can go ahead and login with ssh—the default username and password are “pi” and “raspberry” respectively.
After logging in we can also check that our Raspberry Pi has successfully logged on to our local wireless network using ifconfig.
With wireless networking enabled and working you won’t need to connect it to your laptop again unless something goes wrong.
The Raspberry Pi connected to a USB power supply.
Instead, you can plug the Pi directly into a power supply and after it finishes booting the Raspberry Pi should advertise itself using mDNS with a default name of raspberrypi.local, allowing you to find it easily on the network.
At this stage you should shut down your Raspberry Pi, detach it from your laptop, plug it into your power supply in the normal way, and wait for it to boot.
Installing a VNC Sever
We can now go ahead and install a VNC server. This is an optional step, but is quite useful if you want to be able to get to your Raspberry Pi desktop over the network.
Go ahead and SSH back into your Pi, this time over your wireless network,
Then type,
to install the server. Once it’s installed we can enable it using the Raspbian configuration utility. Type,
at the prompt to open the configuration manager. Using the Up/Down cursor keys navigate to Interfacing Options and the Enter key to select it. Then scroll down and select VNC, and then answer Yes when prompted. This will turn on the server, and return you to the main menu.
Now navigate to Advanced Options, then select Resolutions, and pick a workable resolution, I generally go with 1600×1200 as it fits nicely on my Mac’s desktop.
We need to do this because — as we’re connecting to a headless Pi — the VNC server will default to the smallest safe resolution, typically the same as a standard definition TV, which isn’t going to be particularly usable.
Then use the Left/Right cursor keys to navigate to Finish and hit the Enter key. You’ll be asked whether you want to reboot now, answer Yes.
Connecting to the Raspberry Pi using the Real VNC Viewer application.
Once the Pi has rebooted you should log back in as before using ssh to make sure everything is working correctly.
Unfortunately the version of VNC that we’re now running on the Pi isn’t compatible with the built in screen sharing on macOS. However RealVNC offers a VNC Viewer application for Windows, Linux, and macOS — as well as a number of other platforms.
So go ahead and download the application and install it on your laptop.
The Real VNC Viewer running under mac OS showing the Raspberry Pi desktop.
Once installed you should now just be able to again connect directly to raspberrypi.local with the default username and password. If everything has worked, you should see the Raspberry Pi desktop in a window.
Now we’ve got our Raspberry Pi configured and working, it’s time to shut it down again and assemble the Vision Kit. Leave the micro SD card inserted in the card slot during assembly.
Assembling the Vision Kit
The first thing you need to do is grab the two plastic spacers that come with the kit. They’re not strictly necessary, but they’re going to add a lot of stability. The Raspberry Pi has four mounting holes, the two spacers go into the holes furthest from the header block.
Once both spacers are in place push the Vision Bonnet down onto the Raspberry Pi headers, making sure there isn’t a gap and it sits firmly on the Pi. You should then be able to snap the two spacers into the Bonnet. This may take more force than you were expecting, but afterwards the entire thing should be pretty solid without any flex.
The Vision Bonnet on top of a Raspberry Pi Zero W, with the plastic spaces is visible (front).
Next we need to find the Vision Bonnet cable connector, the short ribbon cable. This is prominently marked with a white label to indicate which direction it should be inserted. Unfortunately you’re probably going to have to ignore the sticker as in a lot of cases these labels were incorrectly applied at the factory.
You’re probably going to have to ignore the sticker.
Pull the black release lever up, and orientating the cable with the serial number facing towards you and the exposed pins facing down, towards the Vision bonnet board, insert it into the into the connector until it hits the back. There really isn’t a lot of play here, so don’t expect it to go in all that far. Secure the cable by flicking the black release lever back down and give it a quick tug to ensure it is secure.
If you have inserted it the correct way around, the most densely populated section of wiring should be furthest away from the 40-pin header block.
This way up?
In my case the white label was incorrectly applied, and the correct way to insert it was to to point the directional arrow towards the Vision bonnet, not the Raspberry Pi.
Now pull the black release lever on your Pi’s camera connector outwards—rather than upwards as in the case of the Vision bonnet—and slide the other end of the cable into the connector underneath the black release lever, between the lever and the board itself. If the lever is pull all the way outwards the ribbon cable should slide smoothly beneath it, and you shouldn’t have to force it.
The short ribbon cable correctly seated.
Once it is fully inserted, slide the black release lever back into place to secure the cable. Like the Vision bonnet the exposed pins on the cable should be facing towards the Raspberry Pi board, not upwards towards the Vision bonnet. The cable will insert a little further than it did on the bonnet, but probably still won’t go in as far as you think it should.
Push the black release lever back into place, and give the cable a tug make sure it’s securely in place. Then get the loop of cable and gently push the loop so that it tucks between the two boards.
Tucking the ribbon cable between the boards.
Next grab the grey ribbon cable and push one end into the button connector on top of the Vision bonnet. The connector might be covered by a black plastic dust cap for protection, if so, remove the cap first. There is a small notch on the connector corresponding to a ridge on the cable plug, and it should slot into the connector easily. If you’re having trouble check to see if it’s going in the wrong way around, putting it in the right way should meant that the cable runs down the length of the board.
Attaching the button cable.
Now it’s time to start assembling the frame. Grab the camera module and the longer ribbon cable. Pull the black release lever of the camera module outwards—it works the same as the smaller levers on the Raspberry Pi—and insert the wider end of the long ribbon cable between the black lever and the board, with the exposed pins of the cable downwards, towards the board.
Inserting the ribbon cable into the camera module.
Once the cable is pushed back into the connector as it you can, push the black release lever back into place. Gently tug on the cable to make sure it’s secure.
Pick up the smaller of the two cardboard pieces, this is the inner frame and holds the hardware components in place inside the larger cardboard box. In the middle is a small U-shaped cut out. Push it out, the tab should naturally fold downwards.
Grab the Pi camera board and slot it in to the rectangular cut out as below, it should fit fairly tightly in into the hole.
First piece of hardware attached to our inner frame.
Then flip the cardboard over and fold down the tab over the back of the camera. The connector should fit through the cut out in the back of the tab.
Folding down the tab.
Go ahead and fold the left and right hand ‘elephant ear’ tabs up to form a box around the camera and then flip the cardboard around again. Find your piezo buzzer and thread the red-black wires through the hole to the left and below the camera lens. Then remove the adhesive cover from the back of the piezo buzzer and stick it into the U-shaped depression on the front of the camera.
Adding the piezo-buzzer.
Lay the cardboard face-down on your table top—it doesn’t matter if it springs slightly apart at this point—and grab your Raspberry Pi and Vision bonnet. Take the smaller (loose) end of the camera ribbon cable and insert it into the camera connector on top of the Vision bonnet. As before, flip the black release lever upwards, push the ribbon cable in with the exposed pins facing down towards the board, and once it is seated push the black release lever down.
Connecting the Vision Bonnet to the Raspberry Pi camera board.
Give the camera cable a gentle tug to make sure it it’s seated and then go ahead and place the boards on the bottom tab of the cardboard frame.
The completed inner frame.
Fold the bottom tab up and thread the ribbon cable through the side slot and gently flex it down towards the board—otherwise you may have problems getting everything in the box afterwards.
Thread the button cable, and the piezo buzzer wires upwards through the top of the frame, and grab the other cardboard piece—the outer box.
Inserting the inner frame into the box.
Pop the outer box open, keeping both the bottom and top flaps open, and go ahead and thread the button cable and piezo wires through from bottom to top—the bottom of the box has the smaller of the two holes in the flap.
Then push down the two side flaps at the bottom of the frame, and gently push the entire inner frame into the box, trying to make sure not to snag the camera cable on the way.
The inner frame fully inserted into the box.
Once the frame is fully inserted fold down the two side flaps of the box, there should be a nut shaped hole. Grab the 1/4/20 nut and place it in this gap—this is fairly fiddly, but despite appearances it’ll be fine once the flaps are closed as the inner frame will push it down against the outside flap of the box.
Inserting the nut.
Then close the outer flap. The screw thread of the nut should still be visible, this is a standard tripod mount screw. If you have any camera gear the Vision Kit, when completed, should jut fit on top.
The standard tripod mount.
Looking down from the top of the box push down slightly to seat the but properly against the bottom of the box. both the button connector cable, and the piezo wire should be sticking up out of the box at this point.
Looking down into the guts of the Vision Kit.
Flip the box around and make sure that the board connectors are aligned with the box cut outs. You should be able to see both USB sockets through the lower right hand cut out. If you don’t, reach into the box and try adjusting the height of the inner frame again.
The board connectors on the back.
Then, flipping it again, check that the camera lens and piezo buzzer are aligned with the cut outs on the front. You might have to push down to engage the camera lens through the cut out on the front of the box. Doing so will lock the entire assembly inside more or less in place.
Then go ahead and grab the small black plastic LED bezel and insert through the hole up, and to the right, of the camera lens.
Inserting the LED bezel.
Now flip the box around and grab the privacy LED and cable and push the LED head into the bezel from behind. It should insert into the bezel with a little gentle pressure, and be visible from the front if you flip the box around to check.
Inserting the privacy LED.
Grab the arcade button and unscrew the plastic washer. Then insert the button into the large hole on the top of the box, and screw back onto the button to fix it in place.
Attaching the arcade button.
Once the arcade button is attached, grab the piezo buzzer wires and insert the white connector into the black socket labelled ‘Piezo’ on the bottom of the button. The socket is handed with the flatter side of the connector going towards the top.
Attaching the piezo buzzer.
Now grab the privacy LED wires and do the same with the socket labelled ‘LED’. Take note that this socket is turned around from the piezo socket, so the wire will be going in upside with respect to the first.
Attaching the privacy LED.
Now grab the final wire, the grey ribbon cable and plug it into the final socket on the button of the arcade button. Just like the socket on the Vision bonnet, the socket here has a slot corresponding to a ridge on the connector on the end of the cable. You shouldn’t need to use much force to slot them together.
Attaching the Vision bonnet to the arcade button.
Once that’s done, carefully fold down the side flaps and wrangle the cables down into the box. Despite impressions to the contrary, the button of the arcade button will clear the side of the box, but there isn’t much clearance so you need to get the cables safely out of the way so you can close the box up.
Closing the box.
Once the box is closed up turn it over and check that everything—like the camera lens and theUSB sockets—are still aligned with the cut outs.
Everything is still in place.
Find the camera lens washer and using your fingernail, or a knife, peel the white backing off it to reveal the adhesive on the back. Flip it around and carefully center it over the camera lens and the push down, gluing it in place.
The camera lens washer in place on the front of the kit.
Now you can attach the lens assembly—which is magnetic—to the front of the camera. Try to get everything as concentric, and lined up, as possible.
Attaching the lens assembly.
That’s it, we’re done. The Vision Kit is completely assembled.
The finished kit.
Powering on the Vision Kit
Grab your power supply and plug it into your Raspberry Pi. If you look through the side slot you should see the green ACT LED flashing as it boots.
The finished Vision Kit mounted on a small tripod.
If you don’t see the green ACT LED flashing inside the box it’s likely that the Raspberry Pi isn’t booting. Make sure your micro SD card is firmly seated and turn it off and on again. If that doesn’t help, it’s possible that you’ve got the short ribbon cable between the Vision bonnet and the Pi the wrong way around. If so it’ll be shorting the 3.3V and GND pins on the camera connector, preventing the Pi from booting. Unfortunately, you may need to take things apart again to flip the cable around.
However if all goes well, after some time has passed—at least a minute or two, possibly a bit longer—the green privacy LED on the front of the kit will light up, as the Joy Detector demo automatically starts up.
The Joy Detector
Go ahead and point the the camera at your face. If you frown, or look sad, the arcade button should turn blue. While if you smile, or laugh, it will turn yellow and red.

The colour of the arcade button’s LED is the sum of the joy scores across all detected faces currently in the camera frame: sad faces are blue, joyful faces are red. When the joy score exceeds 85% in either direction—either sadness or joyful—an 8-bit sound will play on the piezo buzzer.
Trouble Shooting
If the Raspberry Pi boots, but the Joy Detector demo doesn’t start after a few minutes, you should still be able to access the it either via SSH or VNC to do some trouble shooting.
Go ahead and SSH into the Raspberry Pi. From there you can check dmesg for errors, and compare it to output from a ‘good’ boot.
You can also go ahead and stop AIY services, and unload all the AIY related drivers as follows,
After you’ve done this you can use i2cdetect command to check that the Myriad MCU is flashed and working correctly.
If you see ‘51’ then this indicates that your MCU is flashed and working. If you can’t see the Vision bonnet listed, then neither can your Raspberry Pi and it’s possible that you might have a problem with the soldering of your headers.
You can reach Google’s Support at support-aiyprojects@google.com if you run into any issues. But it’ll probably be faster, and more helpful for everyone else, look to see if anyone else is having a problem with the same issue on the project’s Github repo. If not you can go ahead and open an issue.
But before doing so you should check the troubleshooting section of the kit’s assembly guide, and check that you’re not suffering from one of the common teething troubles people have been having with the kit.
Other Examples
To run any of the other example code you’ll need to stop the Joy Detection service which starts automatically when the kit is booted,
and the set up the development environment,
From here we can run the simple face_detection_camera.py example which runs continuous face detection using the VisionBonnet and prints the number of detected faces in the camera image.

Starting it from the command line we can run it over 50 frames,
should give a count of the number of faces in each frame. It may take some time to initialise the script before it starts, so patience is needed.
More information on the demo software and Vision Kit SDK are available on the Maker’s Guide, and you can learn a good deal about how to interact with the kit from that, and from the SDK in the Github repo.
Where Now?
My first project with the new Vision Kit will be to go back and modify the magic mirror build I put together with the Voice Kit a few months ago. I’m going to use the new Vision Kit to replace the awkward custom hotword support with something a bit more seamless — having the mirror just ‘wake up’ when someone stands in front of it.
After that? Well, I’ve got an idea for a project around citizen journalism that might just be the good fit for the kit. Anyway, watch this space over the next month or so for more. Or go follow me on Twitter, where I’ll no doubt post some teaser pictures on how the builds are progressing.
Where Can I Buy It?
The first batch of Vision Kits—a limited run of just 2,000 units—is currently on the shelves at Micro Center in the US, and is priced at $44.99. Although you’ll need to add a few more things to your basket to get going if you don’t have them on hand already — a Raspberry Pi Zero W, a Raspberry Pi Camera module, an appropriately sized SD Card, and of course, a power supply. World wide availability for the kit is expected in the early Spring.
This post was sponsored by Google.
