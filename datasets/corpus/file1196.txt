Harrisburg University Backed THOUGHT ICO Launches Whitelist
Thought, a blockchain start-up based in Harrisburg, Pennsylvania, knows the importance of a great team. They have put together a skillful development team, and an accomplished board of advisors. And they are certainly not trying to hide themselves.

Thought is fundamentally changing the way data is being processed by embedding every piece of data with artificial intelligence. This means that otherwise ‘dumb data’ that needs an application to become useful, becomes valuable and ‘smart’, and is able to act on its own — understanding where it came from, where it’s supposed to go, and what it has to do. This reduces the need for third-party applications, making data processing a lot cheaper and faster.
“The amount of data being created by humans is increasing exponentially with things like social media, and innovations like IoT (Internet of Things) devices.” says the Founder and CEO of Thought, Professor Andrew Hacker. “Traditionally, data just sits around until it is sent to applications that are able to process that information, but Thought makes data agile and able to act on its own without any external help.”
Integrating blockchain technology with this concept creates an extra layer of security on the smallest level. Every piece of data is secured with cryptography, allowing the owners of the data to dictate exactly who has access to it.
Imagine a hospital room where all of the equipment communicates with each other. For example, the thermostat can communicate with the patient’s health record and his/her current health readings from the equipment and find the most optimal room temperature and humidity for this patient. And that’s only the thermostat! You don’t need any third-party applications to make it happen; the information is able to communicate with other pieces of data to make this happen.
Thought’s development is led by Professor Andrew Hacker, who, in addition to being the Founder and CEO of Thought, is a Cybersecurity Expert in Residence at Harrisburg University. He has conceived and developed the concept of hybrid and smart data for the last six years. With extensive cybersecurity experience, Andrew has appeared in many publications talking about the ever-increasing importance of security in the age of the Internet.
“Harrisburg University provides world-class science, technology and analytics education. But we increasingly also support business start-ups and entrepreneurship. Several years ago, we had the opportunity to have Mr. Andrew Hacker, a known expert in cyber security, join HU.” Says Dr. Eric Darr, President of the Harrisburg University. “Andrew’s technical expertise added greatly to our educational offerings, while his start-up company provided terrific experiential opportunities for our students. We came to believe that Mr. Hacker’s technology had significant promise. Therefore, HU has been pleased to financially support the involvement of many computer science students in the further development of the latest Thought technology. “
“The upcoming release of this new technology, Thought Blockchain, and its applications for businesses and consumers in artificial intelligence and analytics represents the bleeding edge of innovation. Harrisburg University looks forward to continuing its work with Mr. Hacker and Thought Technologies as they continue developing new and valuable technology.” Explains Dr. Darr.
The whitelist is open from today, with Pre-ICO starting on March 1, 2018 and the main ICO starting on March 14, 2018. To participate in the Thought ICO, or join their whitelist, please visit: https://thought.live
Join our Telegram Community!
Thought Community
Thought is fundamentally changing the way information is processed by embedding AI into every small piece of data…t.me
Follow our Twitter!
Thought (@Thought_THT) | Twitter
The latest Tweets from Thought (@Thought_THT). Telegran channel: https://t.co/ifkqGaAH9ktwitter.com
Like us on Facebook
Thought
Thought. 3.1K likes. Thought is fundamentally changing how information is being processed by embedding AI into every…www.facebook.com
or LinkedIn
Thought Network | LinkedIn
https://thought.live Data is inherently inanimate and only becomes valuable within the context of an application. As…www.linkedin.com
