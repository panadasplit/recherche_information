import unittest
from src.indexation import get_doc_tf


class TestIndexation(unittest.TestCase):
    def tearDown(self):
        print('Nettoyage !')

    def test_get_doc_tf(self):

        doc = "hello welcome in my startup which name is cool startup"
        res_doc_tf = {
            "hello": 1,
            "welcome": 1,
            "in": 1,
            "my": 1,
            "startup": 2,
            "which": 1,
            "name": 1,
            "is": 1,
            "cool": 1
        }

        doc_tf = get_doc_tf(doc)

        for word in doc.split():
            self.assertEqual(doc_tf[word], res_doc_tf[word])
