import unittest
from src.clean import lower_case, filter_urls, filter_mails, remove_accents, filter_stop_list


class TestClean(unittest.TestCase):
    def tearDown(self):
        print('Nettoyage !')

    def test_lower_case(self):

        self.assertEqual("hello world", lower_case("HellO World"))

    def test_remove_accents(self):

        self.assertEqual("eeeeaaiiuuo", remove_accents("éèêëàâîïùûô"))

    def test_filter_urls(self):

        urls = """http:www.8.comcareers bonjour.Super module www.62degres.com http:52.eu https://coucou.com"""

        self.assertEqual("bonjour.Super module", filter_urls(urls).strip())

    def test_filter_mails(self):

        mails = """kok@gmail.com envoie un message a caro_07@live.fr"""

        self.assertEqual("envoie un message a", filter_mails(mails).strip())

    def test_filter_stop_list(self):

        stop_list = ["coco", "toto"]
        txt = "toto adore les noix de coco le matin"
        filtered_txt = filter_stop_list(txt, stop_list)
        for word in stop_list:
            self.assertNotIn(word, filtered_txt)