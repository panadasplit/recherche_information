import unittest
from src.translate import is_english, get_en_2_fr_dict, translate_en_to_fr


class TestTranslate(unittest.TestCase):
    def tearDown(self):
        print('Nettoyage !')

    def test_is_english(self):
        samples = {
            """la cuisine faite maison de particulier à particulier, gastronomie collaborative""":
            False,
            """l'efficacité de son service.et un marché qui grossit vite ! aujourd'hui""":
            False,
            """mission is to develop dramatically better juvenile products, but we know we can’t accomplish that without a great team""":
            True,
            """12 helps millions of people around the world live a healthier life""":
            True
        }

        for txt, res in samples.items():
            self.assertEqual(is_english(txt), res)

    def test_get_en_2_fr_dict(self):

        en_2_fr_dictionnary = get_en_2_fr_dict()
        test_dict = {
            "Ormiston": ["Ormiston"],
            "jack of spades": ["valet de pique"],
            "mite": ["acarien", "mite", "un tout petit peu"],
            "coralblow": ["russélie"],
            "virility": ["virilité"],
            "melton": ["molleton"],
            "Bine": ["bine"],
            "garden walk": ["allée"],
            "Tasmanian giant freshwater crayfish":
            ["écrevisse géante de Tasmanie"],
            "Kearby with Netherby": ["Kearby with Netherby"],
            "bishopric": ["évêché"],
            "bathe": ["baigner", "se baigner"]
        }

        for en_word in test_dict:
            self.assertIn(en_word, en_2_fr_dictionnary)
            self.assertListEqual(test_dict[en_word],
                                 en_2_fr_dictionnary[en_word])

    def test_translate_en_to_fr(self):
        dictionnary = {
            "helps": ["aide"],
            "people": ["personne", "gens"],
            "live": ["vivre"]
        }
        txt = "helps millions of people around the world live a healthier life"
        res = "aide millions of personne around the world vivre a healthier life"

        self.assertEqual(translate_en_to_fr(txt, dictionnary), res)