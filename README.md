# Document Mining

Use of the [EMC algorithm](https://en.wikipedia.org/wiki/Expectation%E2%80%93maximization_algorithm) to cluster text documents.

## Install

`pip install -r requirements.txt`

## Launch

Generates the index of documents term frequency

`make index`

Observe the [law of Zipf](https://en.wikipedia.org/wiki/Zipf%27s_law)
    
`make zipf`

Generate k cluster from the index
    
`make cem k=<nb_clusters>`

## DataSets

* Articles from Medium [+2Go] : https://www.kaggle.com/aiswaryaramachandran/medium-articles-with-content
* Common words: https://www.kaggle.com/rtatman/english-word-frequency/download

You can use `extracts` fonction in extractor folder if you want to use the same giants csv datasets