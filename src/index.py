import os
import sys

import json

from clean import normalize, get_stop_list
from indexation import get_index_doc_tf

ROOT = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
PATH_CORPUS = ROOT + "/datasets/corpus"
PATH_INDEX_TF_JSON = "/datasets/index_doc_tf.json"


def get_docs(files_path):
    for file_path in files_path:
        with open(file_path, 'r') as doc:
            yield doc.read()


def main(files_path):

    print("Loading stop list dictionary...")
    stop_list = get_stop_list()

    print("Get docs...")
    corpus = (normalize(doc.strip(), stop_list)
              for doc in get_docs(files_path))

    print("Generating the tf index of the corpus...")
    index_doc_tf = get_index_doc_tf(corpus)

    print("Save result...")
    with open(ROOT + PATH_INDEX_TF_JSON, 'w', encoding='utf-8') as f:
        json.dump(index_doc_tf, f, indent=4)
    print(f"Saved in {PATH_INDEX_TF_JSON}")


if __name__ == "__main__":
    files_path = [
        os.path.join(PATH_CORPUS, f) for f in os.listdir(PATH_CORPUS)
        if (os.path.isfile(os.path.join(PATH_CORPUS, f)) and f.endswith(".txt")
            )
    ]
    main(files_path)