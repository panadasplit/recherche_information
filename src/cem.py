import os
import sys
import math
import json
from random import randint

from clean import normalize, get_stop_list
from indexation import get_index_doc_tf, get_clusters_tf, get_set_words, get_clusters_word_count
from computation import get_teta, likelihood, log_teta, logify_pi, sum_tf_log_teta

ROOT = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
PATH_INDEX_DOC_TF = "/result/index_doc_tf.json"

NB_WORDS_DISPLAYED = 10


def main(k, epsilon=0.1):

    print("Loading tf index...")
    with open(ROOT + PATH_INDEX_DOC_TF, 'r') as f:
        index_doc_tf = json.load(f)

    clusters = cem(index_doc_tf, k, epsilon)
    printify_clusters(clusters, index_doc_tf)


def cem(index_doc_tf, k, epsilon):

    # init
    nb_doc = len(index_doc_tf)
    corpus_words = get_set_words(index_doc_tf)

    # init clusters
    clusters = init_clusters(k, nb_doc)

    # init teta
    pi = step_m(clusters, nb_doc)
    log_pi = logify_pi(pi)
    clusters_tf = get_clusters_tf(clusters, index_doc_tf)
    nb_words_clusters = get_clusters_word_count(clusters_tf)
    teta = get_teta(corpus_words, clusters_tf, nb_words_clusters)

    # init Lc
    previous_l_c = -1 - epsilon
    l_c = 0
    t = 0

    # boucle
    while abs(l_c - previous_l_c) > epsilon:

        # step e
        list_estimations_doc = step_e(clusters_tf, corpus_words, log_pi,
                                      index_doc_tf, teta, nb_doc)

        # step c
        clusters = step_c(list_estimations_doc, k)

        # step m
        pi = step_m(clusters, nb_doc)
        log_pi = logify_pi(pi)
        clusters_tf = get_clusters_tf(clusters, index_doc_tf)
        nb_words_clusters = get_clusters_word_count(clusters_tf)
        teta = get_teta(corpus_words, clusters_tf, nb_words_clusters)

        # calcul vraissemblance
        previous_l_c = l_c
        l_c = likelihood(clusters, log_pi, list_estimations_doc, nb_doc)

        t += 1
        print(abs(l_c - previous_l_c))

    return clusters


def step_e(clusters_tf, corpus_words, log_pi, index_doc_tf, teta, nb_doc):
    """ step E of CEM algo """

    list_estimations_doc = [log_pi[:] for _ in range(nb_doc)]

    for id_doc in range(nb_doc):
        for id_cluster in range(len(clusters_tf)):
            list_estimations_doc[id_doc][id_cluster] += sum_tf_log_teta(
                teta, corpus_words, index_doc_tf[id_doc], id_cluster)

    return list_estimations_doc


def step_c(list_estimations_doc, k):
    """ step C of CEM algo """

    new_clusters = [[] for _ in range(k)]

    for id_doc, estimations_doc in enumerate(list_estimations_doc):
        new_clusters[estimations_doc.index(
            max(estimations_doc))].append(id_doc)

    return new_clusters


def step_m(clusters, nb_doc):
    """ step M of CEM algo """

    return [len(cluster) / nb_doc for cluster in clusters]


def init_clusters(k, nb_doc):
    """ return a random list of k cluster [id_doc1, id_doc2, ...] """

    clusters = [[] for _ in range(k)]
    for id_doc in range(nb_doc):
        clusters[randint(0, k - 1)].append(id_doc)
    return clusters


def printify_clusters(clusters, index_doc_tf):
    """ for each clusters, print the words with the most occurences"""
    clusters_tf = get_clusters_tf(clusters, index_doc_tf)
    for k, cluster_tf in enumerate(clusters_tf):
        print(f"\nCluster {k}:")
        for i, item in enumerate(
                sorted(cluster_tf.items(), key=lambda x: x[1], reverse=True)):
            if i == NB_WORDS_DISPLAYED:
                break
            print(f"#{item[0]} : {item[1]}")


if __name__ == "__main__":
    try:
        k = int(sys.argv[1])
    except IndexError:
        print("Missing argument k: make cem k=<nb_cluster>")
    else:
        main(k)