"""Tools to clean a text"""

import os
import re
import unicodedata

from translate import is_english, translate_en_to_fr

# PATH
PATH_ROOT = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
PATH_STOP_LIST = PATH_ROOT + "/datasets/stop_list_en.txt"

# REGEX
REGEX_MAIL = re.compile(r"""\S*@\S*\s?""", re.I)
REGEX_URL = re.compile(
    r"""((http[s]?://)|(www.)|(http[s]?:))(?:[a-zA-Z]|[0-9]|[$-_@.&+]|[!*\(\),]|(?:%[0-9a-fA-F][0-9a-fA-F]))+""",
    re.I)
REGEX_WORD = re.compile(r"""[a-z]{2,}""", re.I)


def normalize(txt, stop_list, en_2_fr_dict=None, translate_en_2_fr=False):
    """ normalize a text, just words will remain """
    tmp = only_words(filter_mails(filter_urls(lower_case(txt))))
    if translate_en_2_fr and is_english(tmp):
        tmp = translate_en_to_fr(tmp, en_2_fr_dict)
    return remove_accents(filter_stop_list(tmp, stop_list))


def get_stop_list():
    """ return the list of most common (and insignificant) words """
    return set((word.rstrip() for word in open(PATH_STOP_LIST, 'r')))


def filter_stop_list(txt, stop_list):
    """ filters a text according to a word list"""

    return " ".join(filter(lambda word: word not in stop_list, txt.split()))


def lower_case(txt):
    """ return text in lowercase """
    return txt.lower()


def filter_urls(txt):
    return REGEX_URL.sub("", txt)


def filter_mails(txt):
    return REGEX_MAIL.sub("", txt)


def only_words(txt):
    return (' '.join(REGEX_WORD.findall(txt)))


def remove_accents(txt):
    return ''.join((c for c in unicodedata.normalize('NFD', txt)
                    if unicodedata.category(c) != 'Mn'))
