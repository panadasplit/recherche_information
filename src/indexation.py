import math
import sys


def get_doc_tf(doc):
    """ return dictionary { word : tf } for a doc """

    doc_tf = {}
    for word in doc.split():
        doc_tf[word] = doc_tf.get(word, 0) + 1
    return doc_tf


def get_corpus_tf(index_doc_tf):
    """ return dictionary { word : tf } for a corpus """
    corpus_tf = {}
    for doc in index_doc_tf:
        for word in doc:
            corpus_tf[word] = corpus_tf.get(word, 0) + 1
    return corpus_tf


def get_doc_tf_idf(doc_tf, corpus_df, nb_doc):
    """ return dictionary { word : tf*idf } for a doc """

    doc_tf_idf = {}
    for word, tf in doc_tf.items():
        doc_tf_idf[word] = tf * math.log10(nb_doc / corpus_df[word])
    return doc_tf_idf


def get_index_doc_tf(corpus):
    """ return a list of dictionary { word : tf }
    for each doc in a corpus """

    return [get_doc_tf(doc) for doc in corpus]


def get_clusters_tf(clusters, index_doc_tf):
    """ return the list of dictionaries {word: tf} for each cluster """

    return [
        get_corpus_tf((index_doc_tf[id_doc] for id_doc in cluster))
        for cluster in clusters
    ]


def get_clusters_word_count(clusters_tf):
    """ return a list containing the number of word for each cluster """

    return [sum(cluster_tf.values()) for cluster_tf in clusters_tf]


def get_set_words(index_doc_tf):
    """ return a set containing all word """

    words = set()
    for doc in index_doc_tf:
        words.update(doc.keys())
    return words