import os
import matplotlib
matplotlib.use('TkAgg')
from matplotlib import pyplot as plt
import tkinter as tk
import json

from clean import normalize, get_stop_list
from translate import get_en_2_fr_dict

from indexation import get_corpus_tf

ROOT = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))

PATH_INDEX_DOC_TF = "/datasets/index_doc_tf.json"


def main():
    print("Loading tf index...")
    with open(ROOT + PATH_INDEX_DOC_TF, 'r') as f:
        index_doc_tf = json.load(f)

    print("Generating corpus tf ...")
    corpus_tf = get_corpus_tf(index_doc_tf)

    print("Displaying result ...")
    plt.plot(range(1,
                   len(corpus_tf) + 1), sorted(corpus_tf.values(),
                                               reverse=True))
    plt.xlabel('rank')
    plt.ylabel('term frequency')
    plt.savefig('result/zipf.png')
    plt.show()


if __name__ == "__main__":
    main()