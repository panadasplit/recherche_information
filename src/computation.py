"""Compute on index"""

import math


def logify_pi(pi):
    """ return pi logify """

    return [math.log10(pi_k) if pi_k > 0 else -math.inf for pi_k in pi]


def get_teta(corpus_words, clusters_tf, nb_words_clusters):
    teta = [[0 for _ in range(len(clusters_tf))]
            for _ in range(len(corpus_words))]
    for j, word in enumerate(corpus_words):
        for k, cluster_tf in enumerate(clusters_tf):
            if word in cluster_tf:
                teta[j][k] = cluster_tf[word] / nb_words_clusters[k]
    return teta


def log_teta(n):
    "customized log teta adapted to 0 value"
    return math.log10(n) if n else -math.inf


def sum_tf_log_teta(teta, corpus_words, doc_tf, k):
    """ return Produit_de_j=1_a_v( teta_j|k * tf_j,di ) """

    res = 0
    for j, word in enumerate(corpus_words):
        if word in doc_tf:
            res += doc_tf[word] * log_teta(teta[j][k])
    return res


def likelihood(clusters, log_pi, list_estimations_doc, nb_doc):
    """ retourne la vraissemblance """
    res = 0
    for id_doc in range(nb_doc):
        for k, cluster in enumerate(clusters):
            if id_doc in cluster:
                res += log_pi[k] * list_estimations_doc[id_doc][k]

    return res