"""
    tools to translate from english to french
"""

import os
import json
from langdetect import detect

ROOT = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
PATH_EN_2_FR = ROOT + "/data/en2fr.txt"  #TO SETUP


def is_english(txt):
    """ test if the text is in english """

    try:
        return detect(txt) == "en"
    except:
        return False


def get_en_2_fr_dict():
    """ return the dict (en->fr) """

    with open(PATH_EN_2_FR, 'r') as en_2_fr_file:
        return json.loads(en_2_fr_file.read())


def translate_en_to_fr(txt, en_2_fr_dict):
    """Translate an english test in french, with the help of a dictionnary"""

    translator = lambda word: en_2_fr_dict[word][
        0] if word in en_2_fr_dict else word
    return " ".join(map(translator, txt.split()))
