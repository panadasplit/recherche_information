import os
import csv

ROOT = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))

PATH_COMMON_WORD = 'unigram_freq.csv'
PATH_STOP_LIST = "data/stop_list_en.txt"

MAX_WORDS = 100

with open(ROOT + PATH_COMMON_WORD, newline='') as File:
    reader = csv.reader(File, delimiter=',')
    with open(ROOT + PATH_STOP_LIST, "w") as f:
        for n, row in enumerate(reader):
            if n != 0:
                f.write(row[0] + "\n")
            if n == MAX_WORDS:
                break