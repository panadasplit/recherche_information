import os
import csv

ROOT = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))

PATH_ARTICLES_FILE = '/datasets/medium_articles.csv'
PATH_CORPUS_DIR = "/corpus/"

MAX_ARTICLES = 1000

unique = set()

with open(ROOT + PATH_ARTICLES_FILE, newline='') as File:
    reader = csv.reader(File, delimiter=',')
    for n, row in enumerate(reader):
        if n == 0:
            index_text, index_title, index_language = row.index(
                "text"), row.index("title"), row.index("language")
        else:
            if row[index_title] not in unique and row[index_language] == "en":
                with open(f"{ROOT+PATH_CORPUS_DIR}file{n}.txt", "w") as f:
                    unique.add(row[index_title])
                    if len(row[index_text].strip()) > 10:
                        f.write(row[index_text])
            if len(unique) == MAX_ARTICLES:
                break